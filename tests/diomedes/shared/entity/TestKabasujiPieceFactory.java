package diomedes.shared.entity;

import java.security.InvalidParameterException;

import junit.framework.TestCase;

public class TestKabasujiPieceFactory extends TestCase {
	
	//Test the numberedPiece method with expected input
	public void testNumberedPieceBuilder() {
		//Test that every piece can be constructed
		for(int i=1; i<=35; i++){
				//Pieces to check against
				Position[] actualSquares = KabasujiPieceFactory.shapes[i-1];
				//Construct the numbered piece
				Piece test = KabasujiPieceFactory.makeNumberedPiece(i);
				//Check that each square in the piece was properly constructed
				for(int j=0; j< test.getSquares().length; j++) {
					//Check that the values are equal
					assertTrue(test.getSquares()[j].equals(actualSquares[j]));
					//Check that the pointers are not
					assertFalse(test.getSquares()[j] == actualSquares[j]);
				}
			
		}
	}
	
	//Test the numberedPiece method with bad input
	public void testNumberedPieceBuilderBadInput() {
		try {
			KabasujiPieceFactory.makeNumberedPiece(0);
			//Should throw an exception...
			fail();
		} catch(InvalidParameterException e) {
			
		} catch(Exception e) {
			//Unexpected exception
			fail();
		}
		
		try {
			KabasujiPieceFactory.makeNumberedPiece(36);
			//Should throw an exception...
			fail();
		} catch(InvalidParameterException e) {
			
		} catch(Exception e) {
			//Unexpected exception
			fail();
		}
		
		
	}

}
