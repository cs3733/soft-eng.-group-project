package diomedes.shared.entity;

import java.security.InvalidParameterException;

import junit.framework.TestCase;

/**
 * Tests for the Piece entity class
 * @author Jordan Burklund
 *
 */
public class TestPiece extends TestCase {
	//Test that the Constructor throws an exception if a null list is added
	public void testNullConstructor() {
		try{
			Piece p = new Piece(null, -1);
			fail();
		} catch (InvalidParameterException e) {
		}
	}
	
	//Tests that the Constructor will only allow lists of length 6
	public void testConstructorLength() {
		//Test an empty list
		try {
			Position[] squares = {};
			Piece p = new Piece(squares, -1);
			fail();
		} catch (InvalidParameterException e) {
			//Threw the correct exception, pass!
		}
		//Test a list less than 6
		try {
			Position[] squares = {	new Position(0,0),
									new Position(1,1),
									new Position(2,2),
									new Position(3,3) };
			Piece p = new Piece(squares, -1);
			fail();
		} catch (InvalidParameterException e) {
			//Threw the correct exception, pass!
		}
		//Test a list greater than 6
		try {
			Position[] squares = {	new Position(0,0),
									new Position(1,1),
									new Position(2,2),
									new Position(3,3),
									new Position(4,4),
									new Position(5,5),
									new Position(6,6)};
			Piece p = new Piece(squares, -1);
			fail();
		} catch (InvalidParameterException e) {
			//Threw the correct exception, pass!
		}
		//Test a list equal to 6
		try {
			Position[] squares = {	new Position(0,0),
									new Position(1,1),
									new Position(2,2),
									new Position(3,3),
									new Position(4,4),
									new Position(5,5)};
			Piece p = new Piece(squares, -1);
		} catch (Exception e) {
			//If any exceptions were thrown, the test fails
			e.printStackTrace();
			fail();
		}
	}
	
	//Test to validate that a list cannot have duplicates
	public void testDuplicateConstructor() {
		//Test having duplicates
		try {
			Position[] squares = {	new Position(0,0),
									new Position(1,1),
									new Position(2,2),
									new Position(3,3),
									new Position(4,4),
									new Position(0,0)};
			Piece p = new Piece(squares, -1);
			fail();
		} catch (InvalidParameterException e) {
			if(!e.getMessage().equals("Duplicate points: <0,0>")) {
				fail();
			}
			//Threw the correct exception, pass!
		}
		//Test not having duplicates
		try {
			Position[] squares = {	new Position(0,0),
									new Position(1,1),
									new Position(2,2),
									new Position(3,3),
									new Position(4,4),
									new Position(5,5)};
			Piece p = new Piece(squares, -1);
		} catch (Exception e) {
			//If any exceptions were thrown, the test fails
			e.printStackTrace();
			fail();
		}
	}
	
	//Test that the internal values are copies of the input list, and that they are completely separate objects
	public void testConstructorCopy() {
		Position p1 = new Position(1,1);
		Position p2 = new Position(2,2);
		Position p3 = new Position(3,3);
		Position p4 = new Position(4,4);
		Position p5 = new Position(5,5);
		Position p6 = new Position(6,6);
		Position[] squares = {p1, p2, p3, p4, p5, p6};
		Piece p = new Piece(squares, -1);
		
		//Check that all positions are duplicates, and not the same object
		for(int i=0; i<p.getSquares().length; i++) {
			boolean areEqual = p.getSquares()[i].equals(squares[i]);
			boolean areSame = (p.getSquares()[i] == squares[i]);
			assertTrue(areEqual && !areSame);
		}
	}

	//Test flipping a piece along the vertical axis
	public void testVerticalFlip() {
		Position p1 = new Position(0,0);
		Position p2 = new Position(0,1);
		Position p3 = new Position(0,2);
		Position p4 = new Position(0,3);
		Position p5 = new Position(1,0);
		Position p6 = new Position(2,0);
		Position[] preFlip = {p1, p2, p3, p4, p5, p6};
		
		Position p1t = new Position(0,0);
		Position p2t = new Position(0,1);
		Position p3t = new Position(0,2);
		Position p4t = new Position(0,3);
		Position p5t = new Position(-1,0);
		Position p6t = new Position(-2,0);
		Position[] postFlip = {p1t, p2t, p3t, p4t, p5t, p6t};
		
		Piece p = new Piece(preFlip, -1);
		p.flipVertical();
		for(int i=0; i<p.getSquares().length; i++) {
			assertEquals(postFlip[i], p.getSquares()[i]);
		}
		//Also check that flipping again mutates it back to the orriginal
		p.flipVertical();
		for(int i=0; i<p.getSquares().length; i++) {
			assertEquals(preFlip[i], p.getSquares()[i]);
		}
	}
	
	//Test flipping a piece along the horizontal axis
		public void testHorizontalFlip() {
			Position p1 = new Position(0,0);
			Position p2 = new Position(0,1);
			Position p3 = new Position(0,2);
			Position p4 = new Position(0,3);
			Position p5 = new Position(1,0);
			Position p6 = new Position(2,0);
			Position[] preFlip = {p1, p2, p3, p4, p5, p6};
			
			Position p1t = new Position(0,0);
			Position p2t = new Position(0,-1);
			Position p3t = new Position(0,-2);
			Position p4t = new Position(0,-3);
			Position p5t = new Position(1,0);
			Position p6t = new Position(2,0);
			Position[] postFlip = {p1t, p2t, p3t, p4t, p5t, p6t};
			
			Piece p = new Piece(preFlip, -1);
			p.flipHorizontal();
			for(int i=0; i<p.getSquares().length; i++) {
				assertEquals(postFlip[i], p.getSquares()[i]);
			}
			//Also check that flipping again mutates it back to the orriginal
			p.flipHorizontal();
			for(int i=0; i<p.getSquares().length; i++) {
				assertEquals(preFlip[i], p.getSquares()[i]);
			}
		}
		
		//Test rotating the piece left, and all the way around
		public void testRotateLeft() {
			Position p1 = new Position(0,0);
			Position p2 = new Position(0,1);
			Position p3 = new Position(0,2);
			Position p4 = new Position(0,3);
			Position p5 = new Position(1,0);
			Position p6 = new Position(2,0);
			Position[] preRotate = {p1, p2, p3, p4, p5, p6};
			
			Position p1t = new Position(0,0);
			Position p2t = new Position(1,0);
			Position p3t = new Position(2,0);
			Position p4t = new Position(3,0);
			Position p5t = new Position(0,-1);
			Position p6t = new Position(0,-2);
			Position[] postRotate = {p1t, p2t, p3t, p4t, p5t, p6t};
			
			Piece p = new Piece(preRotate, -1);
			p.rotateLeft();
			for(int i=0; i<p.getSquares().length; i++) {
				assertEquals(postRotate[i], p.getSquares()[i]);
			}
			//Also check that rotating again 3x mutates it back to the orriginal
			p.rotateLeft();
			p.rotateLeft();
			p.rotateLeft();
			for(int i=0; i<p.getSquares().length; i++) {
				assertEquals(preRotate[i], p.getSquares()[i]);
			}
		}
		
		//Test rotating the piece right, and all the way around
		public void testRotateRight() {
			Position p1 = new Position(0,0);
			Position p2 = new Position(0,1);
			Position p3 = new Position(0,2);
			Position p4 = new Position(0,3);
			Position p5 = new Position(1,0);
			Position p6 = new Position(2,0);
			Position[] preRotate = {p1, p2, p3, p4, p5, p6};
			
			Position p1t = new Position(0,0);
			Position p2t = new Position(-1,0);
			Position p3t = new Position(-2,0);
			Position p4t = new Position(-3,0);
			Position p5t = new Position(0,1);
			Position p6t = new Position(0,2);
			Position[] postRotate = {p1t, p2t, p3t, p4t, p5t, p6t};
			
			Piece p = new Piece(preRotate, -1);
			p.rotateRight();
			for(int i=0; i<p.getSquares().length; i++) {
				assertEquals(postRotate[i], p.getSquares()[i]);
			}
			//Also check that rotating again 3x mutates it back to the orriginal
			p.rotateRight();
			p.rotateRight();
			p.rotateRight();
			for(int i=0; i<p.getSquares().length; i++) {
				assertEquals(preRotate[i], p.getSquares()[i]);
			}
		}
}
