package diomedes.shared.entity;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import junit.framework.TestCase;
/**
 * Test cases for all of the level classes.
 * @author Aura Velarde */
public class TestLevel extends TestCase {

	public void testPuzzle(){


		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		Position pos = new Position(0, 0);
		tiles.put(pos, new Tile(pos));
		Board board = new Board(tiles);

		Position[] squares = {	new Position(0,0),
				new Position(1,1),
				new Position(2,2),
				new Position(3,3),
				new Position(4,4),
				new Position(5,5)};


		ArrayList<Piece> pieces = new ArrayList<Piece>();
		pieces.add(piece);

		Bullpen bullpen = new Bullpen(pieces);


		Puzzle p = new Puzzle(board, bullpen, 1, 10);

		//Let's unlock it first
		p.unlockLevel();
		assertTrue(p.isUnlocked());

		//Check that the constructor worked properly

		assertEquals(1, p.getOrder());
		assertEquals(10, p.getNumMovesLeft());
		//Starting with 12 tiles uncovered
		p.setNumTilesLeft(12);
		assertEquals(12, p.getNumTilesLeft());

		//Let's "Add a piece"
		p.decreaseNumTilesLeft();
		assertEquals(6, p.getNumTilesLeft());

		//We should only have one piece left to complete the puzzle
		assertEquals(1, p.getNumPieceLeft());

		//We should have one less move available after adding the piece
		// but the number of MaxMoves should remain the same
		p.decreaseNumMovesLeft();
		assertEquals(9, p.getNumMovesLeft());
		assertEquals(10, p.getMaxMoves());

		assertFalse(p.hasWon());
		assertTrue(p.hasAchievement());
		assertEquals(2, p.getCurrentAchievement());
		assertFalse(p.hasWon());

		p.decreaseNumTilesLeft();
		assertEquals(0, p.getNumPieceLeft());
		assertTrue(p.hasAchievement());
		assertEquals(3, p.getCurrentAchievement());
		assertTrue(p.hasWon());

		assertEquals("Puzzle", p.getVariationName());

		System.out.println(p.toString());

		p.increaseNumTilesLeft();
		assertEquals(6, p.getNumTilesLeft());
		assertEquals(1, p.getNumPieceLeft());

		assertEquals(2, p.getCurrentAchievement());
		assertFalse(p.hasWon());


		p.increaseNumTilesLeft();
		assertEquals(12, p.getNumTilesLeft());
		assertEquals(2, p.getNumPieceLeft());

		assertEquals(1, p.getCurrentAchievement());
		assertFalse(p.hasWon());

		p.increaseNumTilesLeft();
		assertEquals(18, p.getNumTilesLeft());
		assertEquals(3, p.getNumPieceLeft());
		assertEquals(0, p.getCurrentAchievement());
		assertFalse(p.hasWon());


		p.reset();
		
		assertEquals(0, board.getPieces().size());



	}

	public void testLightning(){


		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		Position pos = new Position(0, 0);
		tiles.put(pos, new Tile(pos));
		Board board = new Board(tiles);

		Position[] squares = {	new Position(0,0),
				new Position(1,1),
				new Position(2,2),
				new Position(3,3),
				new Position(4,4),
				new Position(5,5)};


		ArrayList<Piece> pieces = new ArrayList<Piece>();
		pieces.add(piece);

		Bullpen bullpen = new Bullpen(pieces);

		//Initiate lightning level
		Lightning l = new Lightning(board, bullpen, 1);

		//Set time to 1 minute
		l.setMaxTime(60);
		assertEquals(60, l.getMaxTime());

		//Let's unlock it first
		l.unlockLevel();
		assertTrue(l.isUnlocked());

		//Check that the constructor worked properly

		assertEquals(1, l.getOrder());

		//Starting with 12 tiles uncovered
		l.setNumTilesLeft(15);
		assertEquals(15, l.getNumTilesLeft());
		assertEquals(0, l.getAchievement());

		l.decreaseNumTilesLeft(3);
		assertEquals(12, l.getNumTilesLeft());
		assertEquals(1, l.getCurrentAchievement());

		//Let's "Add a piece"
		l.decreaseNumTilesLeft(3);
		assertEquals(9, l.getNumTilesLeft());

		l.decreaseNumTilesLeft(5);
		assertEquals(4, l.getNumTilesLeft());
		assertEquals(2, l.getCurrentAchievement());

		l.decreaseNumTilesLeft(4);
		assertEquals(0, l.getNumTilesLeft());
		assertEquals(3, l.getCurrentAchievement());
		assertTrue(l.hasWon());



		assertEquals("Lightning", l.getVariationName());

		System.out.println(l.toString());

	}

	public void testRelease(){
		//Initialize some tiles, Bullpen, Board
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		HashMap<Position, NumberedTile> numberedTiles = new HashMap<Position, NumberedTile>();
		
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		
		Position pos = new Position(0, 0);
		Position pos2 = new Position(1, 2);
		
		tiles.put(pos, new Tile(pos));
		tiles.put(pos2, new Tile(pos2));
		
		ArrayList<NumberedTile> arr1 = new ArrayList<NumberedTile>();
		ArrayList<NumberedTile> arr2 = new ArrayList<NumberedTile>();
		ArrayList<NumberedTile> arr3 = new ArrayList<NumberedTile>();
		
		NumberedTileGroup g1 = new NumberedTileGroup(arr1);
		NumberedTileGroup g2 = new NumberedTileGroup(arr2);
		NumberedTileGroup g3 = new NumberedTileGroup(arr3);
		
		

		//Add a tile to the numbered tile groups
		g1.addNumberedTile(pos, new ReleaseData(2, Color.RED));
		g2.addNumberedTile(pos, new ReleaseData(2, Color.GREEN));
		g3.addNumberedTile(pos, new ReleaseData(2, Color.BLUE));

		

		
		Board board = new Board(tiles);

		Position[] squares = {	new Position(0,0),
				new Position(1,1),
				new Position(2,2),
				new Position(3,3),
				new Position(4,4),
				new Position(5,5)};


		ArrayList<Piece> pieces = new ArrayList<Piece>();
		pieces.add(piece);

		Bullpen bullpen = new Bullpen(pieces);

		//Finally create a release level!

		//Start the NumberedTileGroup array to initialize Release
		ArrayList<NumberedTileGroup> ntg = new ArrayList<NumberedTileGroup>();
		
		
		Position pos3 = new Position(1,3);
		Position pos4 =  new Position(1,4);
		Position pos5 =  new Position(1,5);
		Position pos6 = new Position (2,1);
		
		tiles.put(pos3, new Tile(pos3));
		tiles.put(pos4, new Tile(pos4));
		tiles.put(pos5, new Tile(pos5));
		tiles.put(pos6, new Tile(pos6));
		
		
		
		g1.addNumberedTile(pos2, new ReleaseData(1, Color.RED));
		g1.addNumberedTile(pos3, new ReleaseData(3, Color.RED));
		g1.addNumberedTile(pos4, new ReleaseData(4, Color.RED));
		g1.addNumberedTile(pos5, new ReleaseData(5, Color.RED));
		g1.addNumberedTile(pos6, new ReleaseData(6, Color.RED));
		
		g2.addNumberedTile(pos2, new ReleaseData(1, Color.GREEN));
		g2.addNumberedTile(pos3, new ReleaseData(3, Color.GREEN));
		g2.addNumberedTile(pos4, new ReleaseData(4, Color.GREEN));
		g2.addNumberedTile(pos5, new ReleaseData(5, Color.GREEN));
		g2.addNumberedTile(pos6, new ReleaseData(6, Color.GREEN));
		
		g3.addNumberedTile(pos2, new ReleaseData(1, Color.BLUE));
		g3.addNumberedTile(pos3, new ReleaseData(3, Color.BLUE));
		g3.addNumberedTile(pos4, new ReleaseData(4, Color.BLUE));
		g3.addNumberedTile(pos5, new ReleaseData(5, Color.BLUE));
		g3.addNumberedTile(pos6, new ReleaseData(6, Color.BLUE));
		
		

		ntg.add(g1);
		ntg.add(g2);
		ntg.add(g3);
		
		g1.setIsCovered(pos2);
		
		g1.setIsCovered(pos);
		/**TODO: Why is this not being sset correctly??+ */
		
		g1.setIsCovered(pos3);
		g1.setIsCovered(pos4);
		g1.setIsCovered(pos5);
		g1.setIsCovered(pos6);

		Release r = new Release(board, bullpen, 1, ntg);
		System.out.println(r.toString());

		assertEquals(1, r.getOrder());
		assertEquals(1, r.getNumTilesGroupCovered());
		assertTrue(r.hasAchievement());
		
		//Cover some of the other tiles, and check that it still has an achievement
		g2.setIsCovered(pos);
		g2.setIsCovered(pos2);
		g2.setIsCovered(pos3);
		g2.setIsCovered(pos4);
		g2.setIsCovered(pos5);
		g2.setIsCovered(pos6);
		assertEquals(2, r.getNumTilesGroupCovered());
		assertTrue(r.hasAchievement());
		
		g3.setIsCovered(pos);
		g3.setIsCovered(pos2);
		g3.setIsCovered(pos3);
		g3.setIsCovered(pos4);
		g3.setIsCovered(pos5);
		g3.setIsCovered(pos6);
		assertEquals(3, r.getNumTilesGroupCovered());
		assertTrue(r.hasAchievement());
		
//		//Becomes covered so update hashmap!
////		nt1.isCovered = true;
////		assertFalse(nt2.isCovered);
//
//		g1.setIsCovered(pos);
//		assertEquals(1, r.getNumTilesGroupCovered());
//
//		System.out.println(r.toString());
//
//		assertEquals(1, r.getAchievement());
//
//		//Becomes covered so update hashmap!
////		nt2.isCovered = true;
////		assertTrue(nt2.isCovered);
//		g2.setIsCovered(pos);
//		
//	
//		assertEquals(2, r.getNumTilesGroupCovered());
//		assertEquals(2, r.getAchievement());
//		
//		//One more covered to win!
////		nt3.isCovered = true;
////		assertTrue(nt3.isCovered);
//		g3.setIsCovered(pos2);
//		
//	
//		assertEquals(3, r.getNumTilesGroupCovered());
//		assertEquals(3, r.getAchievement());
		r.recalculate();
	}
	
	public void testReset()
	{
		Board b = new Board();
		b.addPiece(new PlacedPiece(new Position(0, 4), KabasujiPieceFactory.makeNumberedPiece(1)));
		ArrayList<Piece> pieces = new ArrayList<Piece>();
		Bullpen bp = new Bullpen(pieces);
		Level lvl = new Puzzle(b, bp, 10, 1);
		assertTrue(b.getPieces().size() == 1);
		assertTrue(b.isTileCovered(new Position(0, 4)));
		lvl.reset();
		assertTrue(b.getPieces().size() == 0);
		assertTrue(bp.getPieces().size() == 1);
		assertFalse(b.isTileCovered(new Position(0, 4)));
	}
	
	public void testDiscard()
	{
		Board b = new Board();
		b.addPiece(new PlacedPiece(new Position(0, 4), KabasujiPieceFactory.makeNumberedPiece(1)));
		ArrayList<Piece> pieces = new ArrayList<Piece>();
		Bullpen bp = new Bullpen(pieces);
		Level lvl = new Puzzle(b, bp, 10, 1);
		assertTrue(b.getPieces().size() == 1);
		assertTrue(b.isTileCovered(new Position(0, 4)));
		lvl.discard();
		assertTrue(b.getPieces().size() == 0);
		assertTrue(bp.getPieces().size() == 0);
		assertFalse(b.isTileCovered(new Position(0, 4)));
	}

}
