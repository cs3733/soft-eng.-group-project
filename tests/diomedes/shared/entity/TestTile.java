package diomedes.shared.entity;

import junit.framework.TestCase;

public class TestTile extends TestCase{

	public void testConstructor() {
		Position p = new Position(1,2);
		Tile t = new Tile(p);
		assertEquals(p, t.boardPosition);
		assertEquals(false, t.isCovered);
	}
	
	//Tests the getRow method
	public void testGetIsCovered() {
		Position p = new Position(3,4);
		Tile t = new Tile(p);
		assertEquals(false, t.getIsCovered());
	}
	
	public void testGetPosition(){
		Position p = new Position(2,1);
		Tile t = new Tile(p);
		assertEquals(p, t.getPosition());
	}
	
	public void testSetIsCovered(){
		Position p = new Position(3,4);
		Tile t = new Tile(p);
		t.setIsCovered(true);
		assertEquals(true, t.getIsCovered());
	}
	
	public void testEqualsWhenEqual(){
		Position p = new Position(1,2);
		Tile t = new Tile(p);
		Tile t2 = new Tile(p);
		assertEquals(true, t.equals(t2));
	}
	
	public void testEqualsWhenNotEqual(){
		Position p = new Position(1,2);
		Tile t = new Tile(p);
		Position p2 = new Position(4,5);
		Tile t2 = new Tile(p2);
		assertEquals(false, t.equals(t2));
	}
	
	public void testEqualsWhenColEqual(){
		Position p = new Position(1,2);
		Tile t = new Tile(p);
		Position p2 = new Position(2,2);
		Tile t2 = new Tile(p2);
		assertEquals(false, t.equals(t2));
	}
	
	public void testEqualsWhenRowEqual(){
		Position p = new Position(1,2);
		Tile t = new Tile(p);
		Position p2 = new Position(1,5);
		Tile t2 = new Tile(p2);
		assertEquals(false, t.equals(t2));
	}
	
	public void testEqualsWhenNotTile(){
		Position p = new Position(1,2);
		Tile t = new Tile(p);
		assertEquals(false, t.equals("hi"));
	}
	
	public void testHashCode(){
		Position p = new Position(1,2);
		Tile t = new Tile(p);
		int code = (t.boardPosition.col*3) + (t.boardPosition.row*5);
		assertEquals(code, t.hashCode());
	}
}
