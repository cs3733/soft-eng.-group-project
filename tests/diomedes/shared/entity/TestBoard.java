package diomedes.shared.entity;

import java.security.InvalidParameterException;
import java.util.HashMap;

import junit.framework.TestCase;

public class TestBoard extends TestCase
{
	public void testCorrectConstructor()
	{
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		Position pos = new Position(0, 0);
		tiles.put(pos, new Tile(pos));
		Board board = new Board(tiles);
		assertTrue(board.tiles.containsKey(pos));
	}
	
	public void testNullContructor()
	{
		try
		{
			Board board = new Board(null);
			fail();
		} catch(InvalidParameterException e) {
			
		}
	}
	
	public void testAddPiece()
	{
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		for(int i = 0; i < 12; i++)
		{
			for(int j = 0; j < 12; j++)
			{
				Position pos = new Position(i, j);
				tiles.put(pos, new Tile(pos));
			}
		}
		Board board = new Board(tiles);
		board.addPiece(placed);
		assertTrue(board.pieces.contains(placed));
	}
	
	public void testIsTileCovered()
	{
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		for(int i = 0; i < 12; i++)
		{
			for(int j = 0; j < 12; j++)
			{
				Position pos = new Position(i, j);
				tiles.put(pos, new Tile(pos));
			}
		}
		Board board = new Board(tiles);
		board.addPiece(placed);
		assertTrue(board.isTileCovered(new Position(6, 6)));
		assertFalse(board.isTileCovered(new Position(0, 0)));
	}
	
	public void testIsValid()
	{
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		for(int i = 0; i < 12; i++)
		{
			for(int j = 0; j < 12; j++)
			{
				Position pos = new Position(i, j);
				tiles.put(pos, new Tile(pos));
			}
		}
		Board board = new Board(tiles);
		board.addPiece(placed);
		assertFalse(board.isValid(placed));
		assertTrue(board.isValid(new PlacedPiece(new Position(5, 5), piece)));
	}
	
	public void testRemovePiece()
	{
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
		PlacedPiece placed2 = new PlacedPiece(new Position(5, 5), piece);
		for(int i = 0; i < 12; i++)
		{
			for(int j = 0; j < 12; j++)
			{
				Position pos = new Position(i, j);
				tiles.put(pos, new Tile(pos));
			}
		}
		Board board = new Board(tiles);
		board.addPiece(placed);
		board.addPiece(placed2);
		assertTrue(board.removePiece(placed));
		assertTrue(board.removePiece(placed2));
		
		
	}
}
