package diomedes.shared.entity;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import diomedes.levelbuilder.entity.Toolbar;
import junit.framework.TestCase;

/**
 * Tests the funtionality of the Toolbar Entity.
 * @author Kyle
 */
public class TestPieceToolbar extends TestCase {
	
	public void testGetPieces() {
		try{
			Toolbar t = new Toolbar();
			ArrayList<Piece> pieces = t.getPieces();
			
			for(int i = 1; i <= pieces.size(); i++) {
				if(!pieces.get(i - 1).equals(KabasujiPieceFactory.makeNumberedPiece(i))) {
					fail(); //if getPieces doesn't return same pieces, fail test
				}
			}
			
			if(!pieces.equals(t.getPieces())) {
				fail(); //if getPieces doesn't return same pieces, fail test
			}
		} catch (InvalidParameterException e) {
			e.printStackTrace();
			fail();
			//if exceptions thrown, test fails
		}
		
	}
	
}