package diomedes.shared.entity;

import junit.framework.TestCase;

/**
 * Tests for the Position class
 * @author Jordan Burklund
 *
 */
public class TestPosition extends TestCase {
	//Test to see that the constructor sets the x and y points correctly
	public void testConstructor() {
		Position p = new Position(1,2);
		assertEquals(1, p.col);
		assertEquals(2, p.row);
	}
	
	//Tests the getRow method
	public void testGetRow() {
		Position p = new Position(3,4);
		assertEquals(4, p.getRow());
	}
	
	//Tests the getCol method
	public void testGetCol() {
		Position p = new Position(3,4);
		assertEquals(3, p.getCol());
	}
	
	//Test that calling equals on a null object returns false
	public void testEqualsNull() {
		Position p = new Position(3,4);
		assertFalse(p.equals(null));
	}
	
	//Tests that calling equals on a different type of object returns false
	public void testEqualsDifferentObject() {
		Position p = new Position(3,4);
		assertFalse(p.equals(new Integer(5)));
	}
	
	//Tests that calling equals is reflexive (a position is equal to itself)
	public void testEqualsReflexive() {
		Position p = new Position(3,4);
		assertTrue(p.equals(p));
	}
	
	//Tests the equals method for several different Positions
	public void testEquals() {
		Position p = new Position(3,4);
		Position p1 = new Position(3,4);
		Position p2 = new Position(4,4);
		Position p3 = new Position(3,3);
		Position p5 = new Position(1,2);
		
		assertTrue(p.equals(p1));
		assertFalse(p.equals(p2));
		assertFalse(p.equals(p3));
	}
	
	//Tests that the equals method is symmetric (if x=y, then y=x)
	public void testEqualsSymmetric() {
		Position p = new Position(3,4);
		Position p1 = new Position(3,4);
		
		assertTrue(p.equals(p1) && p1.equals(p));
	}
	
	//Tests that the equals method is transitive (if x=y and y=z, x=z)
	public void testEqualsTransitive() {
		Position p = new Position(3,4);
		Position p1 = new Position(3,4);
		Position p2 = new Position(3,4);
		Position p3 = new Position(1,2);
		
		assertTrue( p.equals(p1) && p1.equals(p2) && p2.equals(p));
		assertFalse( p.equals(p1) && p1.equals(p3) && p3.equals(p));
	}
	
	//Tests that the copy function copies over all the correct values to a new object
	public void testCopy() {
		Position p = new Position(10, 20);
		Position p2 = p.copy();
		
		assertTrue(p2 != null);
		assertTrue(p2.getRow() == 20);
		assertTrue(p2.getCol() == 10);
		assertTrue(p.equals(p2));
		assertTrue(p2.equals(p));
		assertFalse(p == p2);
	}
	
	//Tests the toString method
	public void testToString() {
		Position p = new Position(1,2);
		assertEquals("<1,2>", p.toString());
	}
	
	//Tests the hashing function for Position
	public void testHashCode() {
		//Test that the hash value is the expected value
		Position p = new Position(1,2);
		assertEquals(102, p.hashCode());
		
		//Tests that two positions with the same values have the same hash
		Position p2 = new Position(1,2);
		assertEquals(p.hashCode(), p2.hashCode());
	}
}
