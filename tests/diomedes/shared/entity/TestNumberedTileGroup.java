package diomedes.shared.entity;

import java.awt.Color;
import java.util.ArrayList;

import junit.framework.TestCase;

public class TestNumberedTileGroup extends TestCase{

	public void testConstructor() {
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		assertEquals(numberedTiles, g.numberedTiles);
	}
	
	public void testAddNumberedTileCreatingNewTile(){
		Position p = new Position(1,2);
		int number = 2;
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		assertEquals(numberedTiles.get(0), numTile);
		
		assertFalse(numTile.equals(number));
		
	}
	
	public void testCopy(){
		Position p = new Position(1,2);
		int number = 2;
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		assertEquals(numberedTiles.get(0), numTile);
		
		NumberedTile numCopy = numTile.copy();
		assertEquals(p, numTile.getPosition());
		assertEquals(new ReleaseData(number, Color.RED), numTile.getReleaseData());
	}
	public void testAddNumberedTileAddTwo(){
		Position p = new Position(1,2);
		Position p2 = new Position(2,4);
		int number = 2;
		int number2 = 4;
		
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		assertEquals(numberedTiles.get(0), numTile);
		
		NumberedTile numTile2 = new NumberedTile(p2, g, new ReleaseData(number2, Color.RED));
		g.addNumberedTile(p2, new ReleaseData(number2, Color.RED));
		assertEquals(numberedTiles.get(1), numTile2);
	}
	
	public void testAddNumberedTileAlreadyExists(){
		Position p = new Position(1,2);
		int number = 2;
		
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		g.addNumberedTile(p, new ReleaseData(number, Color.RED)); 
		assertEquals(numberedTiles.get(0), numTile);
		
		g.addNumberedTile(p, new ReleaseData(3, Color.RED));
		assertEquals(3, numberedTiles.get(0).getReleaseData().getNumber()); 
	}
	
	public void testSetIsCovered(){
		Position p = new Position(1,2);
		int number = 2;
		
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		g.setIsCovered(p);
		assertEquals(true, numberedTiles.get(0).isCovered);
		
	}
	
	public void testRemoveNumberedTileRemoveLast(){
		Position p = new Position(1,2);
		Position p2 = new Position(2,4);
		int number = 2;
		int number2 = 4;
		
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		
		NumberedTile numTile2 = new NumberedTile(p2, g, new ReleaseData(number2, Color.RED));
		g.addNumberedTile(p2, new ReleaseData(number2, Color.RED));
		
		assertEquals(2, g.getSize());
		
		g.removeNumberedTile(p2);
		
		assertEquals(1, g.getSize());
	} 
	
	public void testRemoveNumberedTileRemoveFirst(){
		Position p = new Position(1,2);
		Position p2 = new Position(2,4);
		int number = 2;
		int number2 = 4;
		
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		
		NumberedTile numTile2 = new NumberedTile(p2, g, new ReleaseData(number2, Color.RED));
		g.addNumberedTile(p2, new ReleaseData(number2, Color.RED));
		
		assertEquals(2, g.getSize());
		
		g.removeNumberedTile(p);
		 
		assertEquals(1, g.getSize());
		assertEquals(numberedTiles.get(0), numTile2);
	} 
	
	public void testRemoveAll(){
		Position p = new Position(1,2);
		Position p2 = new Position(2,4);
		int number = 2;
		int number2 = 4;
		
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		
		NumberedTile numTile2 = new NumberedTile(p2, g, new ReleaseData(number2, Color.RED));
		g.addNumberedTile(p2, new ReleaseData(number2, Color.RED));
		
		assertEquals(2, g.getSize());
		
		g.removeAll();
		
		assertEquals(0, g.getSize());
	}
	
	public void testMakeCopy(){
		Position p = new Position(1,2);
		Position p2 = new Position(2,4);
		int number = 2;
		int number2 = 4;
		
		ArrayList<NumberedTile> numberedTiles= new ArrayList<NumberedTile>();
		NumberedTileGroup g = new NumberedTileGroup(numberedTiles);
		
		NumberedTile numTile = new NumberedTile(p, g, new ReleaseData(number, Color.RED));
		g.addNumberedTile(p, new ReleaseData(number, Color.RED));
		
		NumberedTile numTile2 = new NumberedTile(p2, g, new ReleaseData(number2, Color.RED));
		g.addNumberedTile(p2, new ReleaseData(number2, Color.RED));
		
		NumberedTileGroup groupCopy = g.makeCopy();
		
		assertEquals(2, groupCopy.getSize());
		
		assertEquals(g.numberedTiles.get(0), groupCopy.numberedTiles.get(0));
		assertEquals(g.numberedTiles.get(1), groupCopy.numberedTiles.get(1));
	}
	
}
