package diomedes.shared.entity;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import junit.framework.TestCase;

public class TestLevelDirectorySorter extends TestCase {
	Level lightning;
	Level puzzle;
	Level release;
	Level newType;
	
	//Class created to test throwing exceptions if the Level type cannot be handled
	public class NewLevelType extends Level {
		public NewLevelType(Board board, Bullpen bullpen, int order) {
			super(board, bullpen, order);
		}

		public LevelMemento generateMemento() {return null;}
		public boolean restoreFromMemento(LevelMemento memento) {return false;}
		public boolean hasAchievement() {return false;}
		public boolean hasWon() {return false;}
		public String getVariationName() {return null;}

		@Override
		public void recalculate()
		{
			// TODO Auto-generated method stub
			
		}
	}
	
	/**
	 * Setup a bunch of objects for testing
	 */
	@Override
	public void setUp() {
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		Board board = RectangleBoardFactory.makeRectangleBoard(3, 3);
		
		lightning = new Lightning(board, bullpen, 1);
		puzzle = new Puzzle(board, bullpen, 2, 42);
		ArrayList<NumberedTileGroup> tileGroups = new ArrayList<NumberedTileGroup>();
		tileGroups.add(new NumberedTileGroup());
		tileGroups.add(new NumberedTileGroup());
		tileGroups.add(new NumberedTileGroup());
		release = new Release(board, bullpen, 3, tileGroups);
		newType = new NewLevelType(board, bullpen, 2);
	}
	
	/**
	 * Test the getDirectoryForLevel method
	 */
	public void testGetDirectory() {
		assertEquals(Resources.lightningDir, LevelDirectorySorter.getDirectoryForLevel(lightning));
		assertEquals(Resources.puzzleDir, LevelDirectorySorter.getDirectoryForLevel(puzzle));
		assertEquals(Resources.releaseDir, LevelDirectorySorter.getDirectoryForLevel(release));
		try {
			String st = LevelDirectorySorter.getDirectoryForLevel(newType);
			fail();
		} catch (InvalidParameterException e) {
			//threw the correct exception
		} catch (Exception e) {
			//Threw an unexpected exception
			fail();
		}
	}
}
