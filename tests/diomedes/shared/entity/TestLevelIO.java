package diomedes.shared.entity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.security.InvalidParameterException;
import java.util.ArrayList;

import junit.framework.TestCase;

public class TestLevelIO extends TestCase {
	Bullpen bullpen;
	Board board;
	Level[] levels;
	String testDir = "levels/test/";
	
	@Override
	public void setUp() {
		bullpen = new Bullpen(new ArrayList<Piece>());
		board = RectangleBoardFactory.makeRectangleBoard(3, 3);
		levels = new Level[3];
		levels[0] = new Lightning(board, bullpen, 1);
		levels[0].achievement = 3;
		levels[1] = new Puzzle(board, bullpen, 2, 42);
		levels[1].achievement = 3;
		levels[2] = new Lightning(board, bullpen, 3);
		levels[2].achievement = 3;
	}
	
	//Verify that levels can be saved to memory, and have the proper file name
	public void testSaveLevels() {
		LevelSaver saver = new LevelSaver(testDir, levels);
		
		//Attempt to save the levels
		try {
			assertTrue(saver.saveLevels());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		//Generate a false file to ensure only level files are loaded
		try {
			PrintWriter writer = new PrintWriter(testDir+"notMe.txt");
			writer.println("Lorem Ipsum");
			writer.close();
		} catch (FileNotFoundException e) {
			System.err.println("Could not create dummy false file");
		}
		
		//Verify that the folder has the correct filenames
		File folder = new File(testDir);
		File[] fileList = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.contains("level");
			}
		});
		assertEquals(levels.length, fileList.length);
	}
	
	//Verify that levels can be loaded after they are saved to memory
	public void testLoadLevels() {
		LevelSaver saver = new LevelSaver(testDir, levels);
		
		//Attempt to save the levels
		try {
			assertTrue(saver.saveLevels());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		//Try to load the files
		LevelLoader loader = new LevelLoader(testDir);
		try {
			ArrayList<Level> levels = loader.loadLevels();
			assertTrue(levels != null);
			assertTrue(levels.size() == 3);
			//Check that each memento loaded the correct level variation and is not null
			int i = 0;
			for(Level level : levels) {
				assertTrue(level != null);
				assertEquals(3, level.getAchievement());
				if(i==0 || i==2) {
					assertEquals("Lightning", level.getVariationName());
				} else if(i==1) {
					assertEquals("Puzzle", level.getVariationName());
				}
				i++;
			}
		} catch (Exception e) {
			//Some exception occured while loading the files
			e.printStackTrace();
			fail();
		}
	}
	
	//Verify that the constructors throw exceptions for null parameters
	public void testNullConstructors() {
		try {
			LevelLoader test = new LevelLoader(null);
			fail();
		} catch(InvalidParameterException e) {
			//Threw the correct exception
		} catch(Exception e) {
			//Threw an unexpected exception
			fail();
		}
		
		try {
			Level[] nullArray = null;
			LevelSaver test = new LevelSaver(null, nullArray);
			fail();
		} catch(InvalidParameterException e) {
			//Threw the correct exception
		} catch(Exception e) {
			//Threw an unexpected exception
			fail();
		}
		try {
			Level[] nullArray = null;
			LevelSaver test = new LevelSaver("abc", nullArray);
			fail();
		} catch(InvalidParameterException e) {
			//Threw the correct exception
		} catch(Exception e) {
			//Threw an unexpected exception
			fail();
		}
	}
	
}
