package diomedes.shared.entity;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import junit.framework.TestCase;

public class TestLevelMementos extends TestCase {
	ArrayList<Piece> bullpenPieces;
	Board board;
	ArrayList<NumberedTileGroup> numTileGroups;
	boolean testUnlocked;
	int testOrder;
	int testAchievement;
	int testMaxTime;
	int testMaxMoves;
	int testNumTilesLeft;
	/**TODO: add test for number of tiles left */
	
	public void setUp() {
		bullpenPieces = new ArrayList<Piece>();
		bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(1));
		bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(2));
		
		board = RectangleBoardFactory.makeRectangleBoard(6, 6);
		PlacedPiece piece1 = new PlacedPiece(new Position(0,2), KabasujiPieceFactory.makeNumberedPiece(1));
		PlacedPiece piece2 = new PlacedPiece(new Position(1,2), KabasujiPieceFactory.makeNumberedPiece(1));
		PlacedPiece hint1 = new PlacedPiece(new Position(0,0), KabasujiPieceFactory.makeNumberedPiece(1));
		PlacedPiece hint2 = new PlacedPiece(new Position(0,1), KabasujiPieceFactory.makeNumberedPiece(2));
		board.addPiece(piece1);
		board.addPiece(piece2);
		board.hints.add(hint1);
		board.hints.add(hint2);
		
		numTileGroups = new ArrayList<NumberedTileGroup>();
		NumberedTileGroup numGroup1 = new NumberedTileGroup();
		numGroup1.addNumberedTile(new Position(1,1), new ReleaseData(1, Color.RED));
		numGroup1.addNumberedTile(new Position(2,2), new ReleaseData(2, Color.RED));
		NumberedTileGroup numGroup2 = new NumberedTileGroup();
		numGroup2.addNumberedTile(new Position(3,2), new ReleaseData(1, Color.GREEN));
		numGroup2.addNumberedTile(new Position(4,2), new ReleaseData(2, Color.GREEN));
		NumberedTileGroup numGroup3 = new NumberedTileGroup();
		numGroup3.addNumberedTile(new Position(5,2), new ReleaseData(1, Color.BLUE));
		numGroup3.addNumberedTile(new Position(6,2), new ReleaseData(2, Color.BLUE));
		numTileGroups.add(numGroup1);
		numTileGroups.add(numGroup2);
		numTileGroups.add(numGroup3);
		
		testUnlocked = true;
		testOrder = 2;
		testAchievement = 3;
		testMaxTime = 42;
		testMaxMoves = 42;
		testNumTilesLeft = 12;
	}
	
	//Check that the base level reading and writing of mementos works,
	//and that the read data matches the written data
	public void testSerializable() {
		LevelMemento memento = new LevelMemento(bullpenPieces, board.pieces, board.tiles,
				board.hints, board.width, board.height, testUnlocked, testOrder, testAchievement);
		LevelMemento lmemento = new LightningMemento(bullpenPieces, board.pieces, board.tiles,
				board.hints, board.width, board.height, testUnlocked, testOrder, testAchievement, testMaxTime);
		LevelMemento pmemento = new PuzzleMemento(bullpenPieces, board.pieces, board.tiles,
				board.hints, board.width, board.height, testUnlocked, testOrder, testAchievement, testMaxMoves, testNumTilesLeft);
		LevelMemento rmemento = new ReleaseMemento(bullpenPieces, board.pieces, board.tiles,
				board.hints, board.width, board.height, testUnlocked, testOrder, testAchievement, numTileGroups);
		try {
			FileOutputStream fileOutStream = new FileOutputStream("levels/test/test.jbin");
			ObjectOutputStream saveStream = new ObjectOutputStream(fileOutStream);
			saveStream.writeObject(memento);
			saveStream.writeObject(lmemento);
			saveStream.writeObject(pmemento);
			saveStream.writeObject(rmemento);
			saveStream.close();
			fileOutStream.close();
		} catch (Exception e) {
			//Could not write the data for some reason
			e.printStackTrace();
			fail();
		}
		try {
			FileInputStream fileInStream = new FileInputStream("levels/test/test.jbin");
			ObjectInputStream saveStream = new ObjectInputStream(fileInStream);
			
			LevelMemento readMemento = (LevelMemento) saveStream.readObject();
			LightningMemento lreadMemento = (LightningMemento) saveStream.readObject();
			PuzzleMemento preadMemento = (PuzzleMemento) saveStream.readObject();
			ReleaseMemento rreadMemento = (ReleaseMemento) saveStream.readObject();
			
			//Check that the generic level items were retrieved correctly
			assertEquals(memento.bullpenPieces, bullpenPieces);
			assertEquals(memento.boardPieces, board.pieces);
			assertEquals(memento.boardTiles, board.tiles);
			assertEquals(memento.boardHints, board.hints);
			assertEquals(memento.unlocked, true);
			assertEquals(memento.order, testOrder);
			assertEquals(memento.achievement, testAchievement);
			
			//Check that the special Level elements were retrieved correctly
			assertEquals(lreadMemento.maxTime, testMaxTime);
			assertEquals(preadMemento.maxMoves, testMaxMoves);
			assertEquals(rreadMemento.tileGroups, numTileGroups);
			saveStream.close();
		} catch (Exception e) {
			//Could not read in the data...
			e.printStackTrace();
		}
	}
	
	//Test saving and restoring a Lightning level
	public void testSaveRestoreLightning() {
		Lightning lLevel = new Lightning(board, new Bullpen(bullpenPieces), testOrder);
		lLevel.setMaxTime(testMaxTime);
		LevelMemento lMemento = lLevel.generateMemento();
		Lightning lLevelRestored = new Lightning(board, new Bullpen(new ArrayList<Piece>()), 0);
		//Check that the restore operation signals that it was successful
		assertTrue(lLevelRestored.restoreFromMemento(lMemento));
		//Check that the values restored are the same as the orriginal values
		assertEquals(lLevel.bullpen.getPieces(), lLevelRestored.bullpen.getPieces());
		assertEquals(lLevel.board.getPieces(), lLevelRestored.board.getPieces());
		assertEquals(lLevel.board.getTiles(), lLevelRestored.board.getTiles());
		assertEquals(lLevel.board.getHints(), lLevelRestored.board.getHints());
		assertEquals(lLevel.order, lLevelRestored.order);
		assertEquals(lLevel.maxTime, lLevelRestored.maxTime);
	}
	
	//Test saving and restoring a Puzzle level
	public void testSaveRestorePuzzle() {
		Puzzle pLevel = new Puzzle(board, new Bullpen(bullpenPieces), testOrder, testMaxMoves);
		LevelMemento pMemento = pLevel.generateMemento();
		Puzzle pRestored = new Puzzle(null, null, 0, 1);
		//Check that the restore operation succeeds
		assertTrue(pRestored.restoreFromMemento(pMemento));
		//Check that the values restored are the same as the orriginal values
		assertEquals(pLevel.bullpen.getPieces(), pRestored.bullpen.getPieces());
		assertEquals(pLevel.board.getPieces(), pRestored.board.getPieces());
		assertEquals(pLevel.board.getTiles(), pRestored.board.getTiles());
		assertEquals(pLevel.board.getHints(), pRestored.board.getHints());
		assertEquals(pLevel.order, pRestored.order);
		assertEquals(pLevel.maxMoves, pRestored.maxMoves);
	}
	
	//Test saving and restoring a Release level
	public void testSaveRestoreRelease() {
		Release rLevel = new Release(board, new Bullpen(bullpenPieces), testOrder, numTileGroups);
		LevelMemento rMemento = rLevel.generateMemento();
		ArrayList<NumberedTileGroup> tileGroups = new ArrayList<NumberedTileGroup>();
		tileGroups.add(new NumberedTileGroup());
		tileGroups.add(new NumberedTileGroup());
		tileGroups.add(new NumberedTileGroup());
		Release rRestored = new Release(null, null, 0, tileGroups);
		//Check that the restore operation succeeds
		assertTrue(rRestored.restoreFromMemento(rMemento));
		//Check that the values restored are the same as the original values
		assertEquals(rLevel.bullpen.getPieces(), rRestored.bullpen.getPieces());
		assertEquals(rLevel.board.getPieces(), rRestored.board.getPieces());
		assertEquals(rLevel.board.getTiles(), rRestored.board.getTiles());
		assertEquals(rLevel.board.getHints(), rRestored.board.getHints());
		assertEquals(rLevel.order, rRestored.order);
		assertEquals(rLevel.tileGroups, rRestored.tileGroups);
	}
	
	//Test that each level cannot load another's memento or a generic memento
	public void testRestoreBadInput() {
		Lightning lLevel = new Lightning(board, new Bullpen(bullpenPieces), testOrder);
		LevelMemento lMemento = lLevel.generateMemento();
		Puzzle pLevel = new Puzzle(board, new Bullpen(bullpenPieces), testOrder, testMaxMoves);
		LevelMemento pMemento = pLevel.generateMemento();
		Release rLevel = new Release(board, new Bullpen(bullpenPieces), testOrder, numTileGroups);
		LevelMemento rMemento = rLevel.generateMemento();
		LevelMemento memento = new LevelMemento(bullpenPieces, board.pieces, board.tiles,
				board.hints, board.width, board.height, testUnlocked, testOrder, testAchievement);
		
		//Test the lightning level
		assertFalse(lLevel.restoreFromMemento(null));
		assertFalse(lLevel.restoreFromMemento(pMemento));
		assertFalse(lLevel.restoreFromMemento(rMemento));
		assertFalse(lLevel.restoreFromMemento(memento));
		
		//Test the Puzzle level
		assertFalse(pLevel.restoreFromMemento(null));
		assertFalse(pLevel.restoreFromMemento(lMemento));
		assertFalse(pLevel.restoreFromMemento(rMemento));
		assertFalse(pLevel.restoreFromMemento(memento));
		
		//Test the release memento
		assertFalse(rLevel.restoreFromMemento(null));
		assertFalse(rLevel.restoreFromMemento(lMemento));
		assertFalse(rLevel.restoreFromMemento(pMemento));
		assertFalse(rLevel.restoreFromMemento(memento));
		
	}
}
