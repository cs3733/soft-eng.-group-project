package diomedes.shared.entity;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import junit.framework.TestCase;

/**
 * Tests the funtionality of the Bullpen Entity.
 * @author Kyle
 */
public class TestBullpen extends TestCase {

	public void testNullConstructor() {
		try{
			Bullpen b = new Bullpen(null);
			fail();
		} catch (InvalidParameterException e) {
		}
	}
	
	public void testGetPieces() {
		Position[] squares = {	new Position(0,0),
				new Position(1,1),
				new Position(2,2),
				new Position(3,3),
				new Position(4,4),
				new Position(5,5)};
		
		Piece piece = new Piece(squares, -1);
		ArrayList<Piece> pieces = new ArrayList<Piece>();
		pieces.add(piece);
		
		try{
			Bullpen b = new Bullpen(pieces);
			
			if(!pieces.equals(b.getPieces())) {
				fail(); //if getPieces doesn't return same pieces, fail test
			}
		} catch (InvalidParameterException e) {
			e.printStackTrace();
			fail();
			//if exceptions thrown, test fails
		}
		
	}
	
}
