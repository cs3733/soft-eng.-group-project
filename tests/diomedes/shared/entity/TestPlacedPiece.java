package diomedes.shared.entity;

import junit.framework.TestCase;

public class TestPlacedPiece extends TestCase {
	
	//Tests the constructor with valid input
	public void testValidConstructor() {
		Position pos = new Position(1,2);
		PlacedPiece p = new PlacedPiece(pos, KabasujiPieceFactory.makeNumberedPiece(1));
		
		//Check that the position values are the same
		assertTrue(pos.equals(p.boardPosition));
		//Check that positions have different pointers
		assertFalse(pos == p.boardPosition);
	}
	
	//Tests the constructor with invalid input
	public void testInvalidConstructor() {
		try{
			Position pos = new Position(1,2);
			PlacedPiece p = new PlacedPiece(pos, null);
			fail();
		} catch (NullPointerException e) {
			//Constructor output the correct exception
		} catch (Exception e) {
			//Unexpected exception
			fail();
		}

		try{
			PlacedPiece p = new PlacedPiece(null, KabasujiPieceFactory.makeNumberedPiece(1));
			fail();
		} catch (NullPointerException e) {
			//Constructor output the correct exception
		} catch (Exception e) {
			//Unexpected exception
			fail();
		}
	}
	
	//Tests the getPostion() method
	public void testGetPosition() {
		Position pos = new Position(1,2);
		PlacedPiece p = new PlacedPiece(pos, KabasujiPieceFactory.makeNumberedPiece(1));
		
		assertTrue(pos.equals(p.getPosition()));
	}
	
	//Tests the getPiece() method
	public void testGetPiece() {
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		PlacedPiece p = new PlacedPiece(new Position(1,2), piece);
		
		assertTrue(piece == p.getPiece());
	}
	
	//Tests the equals method
	public void testEquals() {
		PlacedPiece p1 = new PlacedPiece(new Position(1,2),
										KabasujiPieceFactory.makeNumberedPiece(1));
		PlacedPiece p2 = new PlacedPiece(new Position(1,2),
										KabasujiPieceFactory.makeNumberedPiece(1));
		PlacedPiece p3 = new PlacedPiece(new Position(1,3),
										KabasujiPieceFactory.makeNumberedPiece(1));
		PlacedPiece p4 = new PlacedPiece(new Position(1,2),
										KabasujiPieceFactory.makeNumberedPiece(2));
		
		assertTrue(p1.equals(p2));
		assertTrue(p2.equals(p1));
		assertFalse(p1.equals(p3));
		assertFalse(p1.equals(p4));
		assertFalse(p1.equals(null));
		assertFalse(p1.equals("Hello World"));
	}
	
	//Tests the hashcode method
	public void testHashCode() {
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		Position pos = new Position(1,1);
		PlacedPiece placedPiece = new PlacedPiece(pos, piece);
		
		assertEquals(piece.id*10000 + pos.hashCode(), placedPiece.hashCode());
	}
}
