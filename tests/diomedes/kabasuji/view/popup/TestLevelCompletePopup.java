package diomedes.kabasuji.view.popup;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Puzzle;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import junit.framework.TestCase;

public class TestLevelCompletePopup extends TestCase {

	public void test() {
		Puzzle plevel = new Puzzle(new Board(), new Bullpen(new ArrayList<Piece>()),1,1);
		KabasujiModel model = new KabasujiModel();
		model.addLevel(plevel);
		model.setCurrentLevel(plevel);
		Application app = new Application(model);
		//Test the constructor
		LevelCompletePopupView popup = new LevelCompletePopupView(app);
		assertEquals(app, popup.app);
		assertTrue(popup.replayButton != null);
		assertTrue(popup.returnButton != null);
		
		//Test the buttons close the popup
		popup.actionPerformed(new ActionEvent(popup.returnButton, 1, "Return"));
		assertFalse(popup.isShowing());
		popup = new LevelCompletePopupView(app);
		popup.actionPerformed(new ActionEvent(popup.replayButton, 1, "Replay"));
		assertFalse(popup.isShowing());
		
		app.dispose();
	}
}
