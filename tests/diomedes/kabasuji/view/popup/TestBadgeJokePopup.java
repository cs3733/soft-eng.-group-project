package diomedes.kabasuji.view.popup;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Puzzle;
import junit.framework.TestCase;

public class TestBadgeJokePopup extends TestCase {
	public void test() {
		Puzzle plevel = new Puzzle(new Board(), new Bullpen(new ArrayList<Piece>()),1,1);
		KabasujiModel model = new KabasujiModel();
		model.addLevel(plevel);
		model.setCurrentLevel(plevel);
		Application app = new Application(model);
		
		//Test the constructor
		BadgeJokePopupView popup = new BadgeJokePopupView(app);
		assertTrue(popup.okButton != null);
		
		//Test that the window does not close for another button
		popup.actionPerformed(new ActionEvent("Test", 2, "Nothing"));
		//Test that the window closes
		popup.actionPerformed(new ActionEvent(popup.okButton, 1, "Ok"));
		assertFalse(popup.isShowing());
		
		app.dispose();
	}
}
