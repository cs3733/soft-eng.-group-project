//package diomedes.kabasuji.control;
//
//import java.awt.Color;
//import java.awt.event.MouseEvent;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import diomedes.kabasuji.model.KabasujiModel;
//import diomedes.kabasuji.view.Application;
//import diomedes.shared.entity.Board;
//import diomedes.shared.entity.Bullpen;
//import diomedes.shared.entity.KabasujiPieceFactory;
//import diomedes.shared.entity.Level;
//import diomedes.shared.entity.NumberedTile;
//import diomedes.shared.entity.NumberedTileGroup;
//import diomedes.shared.entity.Piece;
//import diomedes.shared.entity.PlacedPiece;
//import diomedes.shared.entity.Position;
//import diomedes.shared.entity.Puzzle;
//import diomedes.shared.entity.RectangleBoardFactory;
//import diomedes.shared.entity.Release;
//import diomedes.shared.entity.ReleaseData;
//import diomedes.shared.entity.Tile;
//import junit.framework.TestCase;
//
//public class TestReleaseBoardController extends TestCase{
//
//	KabasujiModel kaModel;
//	Application app;
//
//	@Override
//	protected void setUp() {
//
//		kaModel = new KabasujiModel();
//		app = new Application(kaModel);
//
//
//		//Initialize some tiles, Bullpen, Board
//		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
//		HashMap<Position, NumberedTile> numberedTiles = new HashMap<Position, NumberedTile>();
//
//		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
//		PlacedPiece placed = new PlacedPiece(new Position(6, 6), piece);
//
//		Position pos = new Position(0, 0);
//		Position pos2 = new Position(1, 2);
//		Position pos3 = new Position(1,3);
//		Position pos4 =  new Position(1,4);
//		Position pos5 =  new Position(1,5);
//		Position pos6 = new Position (2,1);
//
//		tiles.put(pos, new Tile(pos));
//		tiles.put(pos2, new Tile(pos2));
//		tiles.put(pos3, new Tile(pos3));
//		tiles.put(pos4, new Tile(pos4));
//		tiles.put(pos5, new Tile(pos5));
//		tiles.put(pos6, new Tile(pos6));
//
//
//
//		ArrayList<NumberedTile> arr1 = new ArrayList<NumberedTile>();
//		ArrayList<NumberedTile> arr2 = new ArrayList<NumberedTile>();
//		ArrayList<NumberedTile> arr3 = new ArrayList<NumberedTile>();
//
//
//		//Start the NumberedTileGroup array to initialize Release
//		ArrayList<NumberedTileGroup> ntg = new ArrayList<NumberedTileGroup>();
//
//		NumberedTileGroup g1 = new NumberedTileGroup(arr1);
//		NumberedTileGroup g2 = new NumberedTileGroup(arr2);
//		NumberedTileGroup g3 = new NumberedTileGroup(arr3);
//
//
//		//Add a tile to the numbered tile groups
//		g1.addNumberedTile(pos, new ReleaseData(2, Color.RED));
//		g2.addNumberedTile(pos, new ReleaseData(2, Color.GREEN));
//		g3.addNumberedTile(pos2, new ReleaseData(2, Color.BLUE));
//
//
//		Board board = new Board(tiles);
//
//		Position[] squares = {	new Position(0,0),
//				new Position(1,1),
//				new Position(2,2),
//				new Position(3,3),
//				new Position(4,4),
//				new Position(5,5)};
//
//
//		ArrayList<Piece> pieces = new ArrayList<Piece>();
//		pieces.add(piece);
//
//		Bullpen bullpen = new Bullpen(pieces);
//
//
//
//
//		ntg.add(g1);
//		ntg.add(g2);
//		ntg.add(g3);
//
//		g1.setIsCovered(pos2);	
//		g1.setIsCovered(pos);		
//		g1.setIsCovered(pos3);
//		g1.setIsCovered(pos4);
//		g1.setIsCovered(pos5);
//		g1.setIsCovered(pos6);
//
//		Release testLevel = new Release(board, bullpen, 1, ntg);
//		kaModel.addLevel(testLevel);
//		kaModel.setCurrentLevel(testLevel);
//	}
//
//
//	public void testMouseClicked(){
//		BoardControllerRelease bcr = new BoardControllerRelease(app);
//		//		MouseEvent me = new MouseEvent(bcr, 0, 0, );
//		//		bcr.mouseClicked(me);
//	}
//
//}
