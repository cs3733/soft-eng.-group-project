package diomedes.kabasuji.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import diomedes.kabasuji.KabasujiTestCase;
import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.BoardView;
import diomedes.kabasuji.view.panel.BullpenView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Puzzle;

public class TestBullpenController extends KabasujiTestCase {
	
	public void testSelectPieceInBullpen() {
		KabasujiModel km = new KabasujiModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		km.setCurrentLevel(puzzle);
		Application app = new Application(km);
		app.getLevelView().setLevel(km.getCurrentLevel());
		BullpenView bv = app.getLevelView().getBullpenView();
		bv.update();
		MouseEvent mouseClicked = createClicked(app, bv.getPieceViews(), 40, 40);
		MouseListener[] ml = bv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null);
		app.dispose();
	}
	
	public void testMovePieceToBoardFromBullpen() {
		KabasujiModel km = new KabasujiModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		km.setCurrentLevel(puzzle);
		Application app = new Application(km);
		app.getLevelView().setLevel(km.getCurrentLevel());
		BullpenView bv = app.getLevelView().getBullpenView();
		bv.update();
		MouseEvent mouseClicked = createClicked(app, bv.getPieceViews(), 40, 40);
		MouseListener[] ml = bv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}

		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null);

		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}

		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() == null);

		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}

		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null);

		assertEquals(puzzle.getNumMovesLeft(), puzzle.getMaxMoves());

		BoardView boardView = app.getLevelView().getBoardView();
		MouseEvent mouseClickedOnBoard = createClicked(app, boardView, -51, -83);
		ml = boardView.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardControllerPuzzle) {
				BoardControllerPuzzle bc = (BoardControllerPuzzle) m;
				bc.mouseClicked(mouseClickedOnBoard);
			}
		}

		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() == null);
		assertEquals(puzzle.getNumMovesLeft(), puzzle.getMaxMoves() - 1);
		assertFalse(app.getModel().getCurrentLevel().getBoard().getPieces().isEmpty());
		assertTrue(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() == null);

		for(MouseListener m : ml) {
			if(m instanceof BoardControllerPuzzle) {
				BoardControllerPuzzle bc = (BoardControllerPuzzle) m;
				bc.mousePressed(mouseClickedOnBoard);
				boardView.repaint();
			}
		}

		assertTrue(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() != null);

		app.dispose();
	}

}
