package diomedes.kabasuji.control;

import java.util.ArrayList;

import javax.swing.JButton;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Puzzle;
import junit.framework.TestCase;

public class TestRotateFlip extends TestCase {

	public void testRotateAndFlip() {
		KabasujiModel km = new KabasujiModel();
		ArrayList<Piece> pieces = new ArrayList<Piece>();
		pieces.add(KabasujiPieceFactory.makeNumberedPiece(1));
		km.setCurrentLevel(new Puzzle(new Board(), new Bullpen(pieces), 1, 10));
		Application app = new Application(km);
		app.getLevelView().setLevel(km.getCurrentLevel());
		
		km.getCurrentLevel().getBullpen().setSelectedPiece(app.getModel().getCurrentLevel().getBullpen().getPieces().get(0));
		
		Piece piece = KabasujiPieceFactory.makeNumberedPiece(1);
		
		JButton rotateRight = new JButton();
		rotateRight.addActionListener(new RotatePieceRight(app, km));
		rotateRight.doClick();
		piece.rotateRight();
		
		assertEquals(piece, km.getCurrentLevel().getBullpen().getSelectedPiece());
		
		JButton rotateLeft = new JButton();
		rotateLeft.addActionListener(new RotatePieceLeft(app, km));
		rotateLeft.doClick();
		piece.rotateLeft();
		
		assertEquals(piece, km.getCurrentLevel().getBullpen().getSelectedPiece());
		
		JButton flipHorizontal = new JButton();
		flipHorizontal.addActionListener(new FlipPieceHorizontal(app, km));
		flipHorizontal.doClick();
		piece.flipHorizontal();
		
		assertEquals(piece, km.getCurrentLevel().getBullpen().getSelectedPiece());
		
		JButton flipVertical = new JButton();
		flipVertical.addActionListener(new FlipPieceVertical(app, km));
		flipVertical.doClick();
		piece.flipVertical();
		
		assertEquals(piece, km.getCurrentLevel().getBullpen().getSelectedPiece());
		app.dispose();
	}
	
}
