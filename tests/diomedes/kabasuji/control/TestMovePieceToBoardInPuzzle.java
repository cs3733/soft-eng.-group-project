package diomedes.kabasuji.control;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.RectangleBoardFactory;
import diomedes.shared.entity.Tile;
import junit.framework.TestCase;

/**
 * @author Aura Velarde
 * Reusing code from @Jordan Burklund
 */
public class TestMovePieceToBoardInPuzzle extends TestCase {
	KabasujiModel kaModel;
	MovePieceToBoard move;
	Puzzle testLevel;

	@Override
	protected void setUp() {
		kaModel = new KabasujiModel();
		HashMap<Position, Tile> testTiles = new HashMap<Position, Tile>();

		Board board = RectangleBoardFactory.makeRectangleBoard(6,6);
		ArrayList<Piece> bullpenPieces = new ArrayList<Piece>();
		bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(1));
		bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(2));
		Bullpen bullpen = new Bullpen(bullpenPieces);
		testLevel = new Puzzle(board, bullpen, 1, 3);
		kaModel.addLevel(testLevel);
		kaModel.setCurrentLevel(testLevel);
	}

	//Tests the constructor with null values
	public void testConstructorNull() {
		try{
			move = new MovePieceToBoardInPuzzle(null,null);
			fail();
		} catch(InvalidParameterException e) {
			//Test has passed
		} catch(Exception e) {
			//A different type of excpetion was thrown
			fail();
		}
		try{
			move = new MovePieceToBoardInPuzzle(new Position(0,1),null);
			fail();
		} catch(InvalidParameterException e) {
			//Test has passed
		} catch(Exception e) {
			//A different type of excpetion was thrown
			fail();
		}
	}

	//Tests the setPosition method
	public void testSetPosition() {

		move = new MovePieceToBoardInPuzzle(new Position(3,4), KabasujiPieceFactory.makeNumberedPiece(1));
		assertEquals(new Position(3,4), move.boardPosition);


	}

	//Tests the valid method for the move
	public void testValid() {
		//Try to add a piece to an empty board
		Piece piece1 = KabasujiPieceFactory.makeNumberedPiece(1);
		move = new MovePieceToBoardInPuzzle(new Position(0,2), piece1);
		assertTrue(move.isValid(kaModel));
		move.doMove(kaModel);

		//Try to add a piece that does not intersect the first one
		Piece piece2 = KabasujiPieceFactory.makeNumberedPiece(12);
		move = new MovePieceToBoardInPuzzle(new Position(1,1),piece2);
		assertTrue(move.isValid(kaModel));
		move.doMove(kaModel);

		//Try to add a piece that intersects the others;
		Piece piece3 = KabasujiPieceFactory.makeNumberedPiece(12);
		move = new MovePieceToBoardInPuzzle(new Position(1,1), piece3);
		assertFalse(move.isValid(kaModel));


		assertEquals(1, testLevel.getNumMovesLeft());

		/**
		 * TODO: Test adding another piece and then another to check numMovesLeft 0 causes an invalid move
		 */


		//Let's see what happens if the number of moves is 0

	}

	//Tests the doMove method
	public void testDoMove() {

		//Make sure the Num Moves Left is decreased


		//Test that a valid piece shows up in the board
		Piece piece1 = KabasujiPieceFactory.makeNumberedPiece(1);
		move = new MovePieceToBoardInPuzzle(new Position(0, 2), piece1);
		move.doMove(kaModel);
		Board board = kaModel.getCurrentLevel().getBoard();
		assertTrue(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece1)));

		assertEquals(2, testLevel.getNumMovesLeft());

		//Test that an invalid piece does not show up in the board
		Piece piece2 = KabasujiPieceFactory.makeNumberedPiece(12);
		move = new MovePieceToBoard(new Position(0,1), piece2);
		move.doMove(kaModel);

		assertFalse(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece2)));
	}

	//Tests the undoMove method
	public void testUndoMove() {
		//Test that a piece is added, and then removed from the board
		Piece piece1 = KabasujiPieceFactory.makeNumberedPiece(1);
		move = new MovePieceToBoard(new Position(0,2), piece1);
		move.doMove(kaModel);
		Board board = kaModel.getCurrentLevel().getBoard();
		assertTrue(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece1)));

		//Validate that the undo method returns false
		assertFalse(move.undoMove(kaModel));
	}

}
