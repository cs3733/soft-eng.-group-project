package diomedes.kabasuji.control;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.RectangleBoardFactory;
import diomedes.shared.entity.Tile;
import junit.framework.TestCase;

public class TestMovePieceToBullpen extends TestCase {
	/**
	 * @author Aura Velarde
	 * Reusing code from @Jordan Burklund
	 */
	
		KabasujiModel kaModel;
		MovePieceToBullpen move;
		Puzzle testLevel;
		MovePieceToBoardInPuzzle move2;
		Bullpen bullpen;


		@Override
		protected void setUp() {
			kaModel = new KabasujiModel();
			HashMap<Position, Tile> testTiles = new HashMap<Position, Tile>();

			Board board = RectangleBoardFactory.makeRectangleBoard(6,6);
			ArrayList<Piece> bullpenPieces = new ArrayList<Piece>();
			bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(1));
			bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(2));
			bullpen = new Bullpen(bullpenPieces);
			testLevel = new Puzzle(board, bullpen, 1, 3);
			kaModel.addLevel(testLevel);
			kaModel.setCurrentLevel(testLevel);
		}

		//Tests the constructor with null values
		public void testConstructorNull() {
			ArrayList<Piece> bullpenPieces = new ArrayList<Piece>();
			bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(1));
			bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(2));
			Bullpen bullpen = new Bullpen(bullpenPieces);

			try{
				move = new MovePieceToBullpen(null,null);
				fail();
			} catch(InvalidParameterException e) {
				//Test has passed
			} catch(Exception e) {
				//A different type of excpetion was thrown
				fail();
			}
			try{
				move = new MovePieceToBullpen(null, bullpen);
				fail();
			} catch(InvalidParameterException e) {
				//Test has passed
			} catch(Exception e) {
				//A different type of excpetion was thrown
				fail();
			}
		}

		//Tests the setPosition method
		public void testDoMove() {
			
			
			Board board = kaModel.getCurrentLevel().getBoard();
			int size = kaModel.getCurrentLevel().getBullpen().getPieces().size();
			
			Piece piece1 =  KabasujiPieceFactory.makeNumberedPiece(1);
			move2 = new MovePieceToBoardInPuzzle(new Position(0, 2), piece1);
			move2.doMove(kaModel);
			assertTrue(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece1)));
			
			PlacedPiece Placedpiece1 = new PlacedPiece(new Position(0,2), piece1);
			move = new MovePieceToBullpen(Placedpiece1, bullpen);
			board.selectPiece(Placedpiece1);

			assertTrue(move.isValid(kaModel));
			
			assertTrue(move.doMove(kaModel));
			
			assertEquals(size+1, kaModel.getCurrentLevel().getBullpen().getPieces().size());
			assertTrue(kaModel.getCurrentLevel().getBullpen().getPieces().contains(piece1));
			assertFalse(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece1)));




		}
		
		public void testUndoMove(){
			Board board = kaModel.getCurrentLevel().getBoard();
			testLevel = new Puzzle(board, bullpen, 1, 3);
			
			Piece piece1 =  KabasujiPieceFactory.makeNumberedPiece(1);
			move2 = new MovePieceToBoardInPuzzle(new Position(0, 2), piece1);
			move2.doMove(kaModel);
			assertTrue(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece1)));
			
			PlacedPiece Placedpiece1 = new PlacedPiece(new Position(0,2), piece1);
			move = new MovePieceToBullpen(Placedpiece1, bullpen);
			board.selectPiece(Placedpiece1);
			
			assertTrue(move.isValid(kaModel));
			
			assertTrue(move.doMove(kaModel));
			
			assertFalse(move.undoMove(kaModel));
		}
		
		public void testValid(){
			Board board = kaModel.getCurrentLevel().getBoard();
			Piece piece1 =  KabasujiPieceFactory.makeNumberedPiece(1);
			move2 = new MovePieceToBoardInPuzzle(new Position(0, 2), piece1);
			move2.doMove(kaModel);
			
			PlacedPiece Placedpiece1 = new PlacedPiece(new Position(0,2), piece1);
			move = new MovePieceToBullpen(Placedpiece1, bullpen);
			
			board.selectPiece(Placedpiece1);
			
			assertEquals(2, testLevel.getNumMovesLeft());
			assertTrue(move.isValid(kaModel));
			move.doMove(kaModel);
			assertFalse(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece1)));
			assertEquals(1, testLevel.getNumMovesLeft());
			
			
			move = new MovePieceToBullpen(Placedpiece1, bullpen);
//		    board.selectPiece(Placedpiece1);
			
			assertFalse(move.doMove(kaModel));
			
			move2 = new MovePieceToBoardInPuzzle(new Position(0, 2), piece1);
			assertTrue(move2.doMove(kaModel));
			assertEquals(0, testLevel.getNumMovesLeft());
			
			move = new MovePieceToBullpen(Placedpiece1, bullpen);
			assertFalse(move.doMove(kaModel));
		}
	}

//
//		//Tests the valid method for the move
//		public void testValid() {
//			//Try to add a piece to an empty board
//			Piece piece1 = KabasujiPieceFactory.makeNumberedPiece(1);
//			move2 = new MovePieceToBoardInPuzzle(new Position(0,2), piece1);
//			assertTrue(move2.isValid(kaModel));
//			move2.doMove(kaModel);
//
//			//Try to add a piece that does not intersect the first one
//			Piece piece2 = KabasujiPieceFactory.makeNumberedPiece(12);
//			move2 = new MovePieceToBoardInPuzzle(new Position(1,1),piece2);
//			assertTrue(move2.isValid(kaModel));
//			move2.doMove(kaModel);
//
//			//Try to add a piece that intersects the others;
//			Piece piece3 = KabasujiPieceFactory.makeNumberedPiece(12);
//			move2 = new MovePieceToBoardInPuzzle(new Position(1,1), piece3);
//			assertFalse(move.isValid(kaModel));
//
//
//			assertEquals(1, testLevel.getNumMovesLeft());

/**
 * TODO: Test adding another piece and then another to check numMovesLeft 0 causes an invalid move
 */


//Let's see what happens if the number of moves is 0
//
//		}

//		//Tests the doMove method
//		public void testDoMove() {
//
//			//Make sure the Num Moves Left is decreased
//
//
//			//Test that a valid piece shows up in the board
//			Piece piece1 = KabasujiPieceFactory.makeNumberedPiece(1);
//			move2 = new MovePieceToBoardInPuzzle(new Position(0, 2), piece1);
//			move2.doMove(kaModel);
//			Board board = kaModel.getCurrentLevel().getBoard();
//			assertTrue(board.getPieces().contains(new PlacedPiece(new Position(0,2), piece1)));
//
//			assertEquals(2, testLevel.getNumMovesLeft());
//
//			
//		}
//
//		//Tests the undoMove method
//		public void testUndoMove() {
//		
//		}
//
//	}

//
//}
