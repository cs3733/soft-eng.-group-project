package diomedes.levelbuilder;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import diomedes.levelbuilder.view.Application;
import junit.framework.TestCase;

/**
 * Defines useful helper methods for testing KombatSolitaire plugins.
 * <p>
 * If you would like to use this capability, have your JUnit test case
 * extend from this class instead of {@link TestCase}.
 * 
 * @author George Heineman
 */
public abstract class LevelBuilderTestCase extends TestCase {
	/** (dx,dy) are offsets into the widget space. Feel Free to Use as Is. */
	public MouseEvent createPressed (Application game, JPanel view, int dx, int dy) {
		MouseEvent me = new MouseEvent(game, MouseEvent.MOUSE_PRESSED, 
				System.currentTimeMillis(), InputEvent.BUTTON1_MASK, 
				view.getX()+dx, view.getY()+dy, 0, false);
		return me;
	}
	
	/** (dx,dy) are offsets into the widget space. Feel Free to Use as Is. */
	public MouseEvent createRightClick (Application game, JPanel view, int dx, int dy) {
		MouseEvent me = new MouseEvent(game, MouseEvent.MOUSE_PRESSED, 
				System.currentTimeMillis(), InputEvent.BUTTON3_MASK, 
				view.getX()+dx, view.getY()+dy, 0, true);
		return me;
	}
	
	/** (dx,dy) are offsets into the widget space. Feel Free to Use as Is. */
	public MouseEvent createReleased (Application game, JPanel view, int dx, int dy) {
		MouseEvent me = new MouseEvent(game, MouseEvent.MOUSE_RELEASED, 
				System.currentTimeMillis(), 0, 
				view.getX()+dx, view.getY()+dy, 0, false);
		return me;
	}
	
	/** (dx,dy) are offsets into the widget space. Feel Free to Use as Is. */
	public MouseEvent createClicked (Application game, JPanel view, int dx, int dy) {
		MouseEvent me = new MouseEvent(game, MouseEvent.MOUSE_CLICKED, 
				System.currentTimeMillis(), 0, 
				view.getX()+dx, view.getY()+dy, 1, false);
		return me;
	}
	
	/** (dx,dy) are offsets into the widget space. Feel Free to Use as Is. */
	public MouseEvent createDoubleClicked (Application game, JPanel view, int dx, int dy) {
		MouseEvent me = new MouseEvent(game, MouseEvent.MOUSE_CLICKED, 
				System.currentTimeMillis(), 0, 
				view.getX()+dx, view.getY()+dy, 2, false);
		return me;
	}
}