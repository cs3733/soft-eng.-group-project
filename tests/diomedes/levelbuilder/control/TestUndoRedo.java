package diomedes.levelbuilder.control;

import java.util.ArrayList;

import javax.swing.JButton;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Puzzle;
import junit.framework.TestCase;

/**
 * Tests the functionality of undoing and redoing a move.
 * @author Kyle Carrero
 *
 */
public class TestUndoRedo extends TestCase {

	public void testUndoAndRedo() {
		LevelBuilderModel lb = new LevelBuilderModel();
		lb.setCurrentLevel(new Puzzle(new Board(), new Bullpen(new ArrayList<Piece>()), 1, 10));
		Application app = new Application(lb);
		EditLevelView lv = app.getEditLevelView();
		lv.setLevelToEdit(lb.getCurrentLevel());
		
		String beforeMove = lb.getCurrentLevel().getBoard().toString();
		
		JButton b1 = new JButton();
		b1.addActionListener(new ResizeBoard(app, lv, true, false));
		b1.doClick(); //should push move onto undo stack
		
		String afterMove = lb.getCurrentLevel().getBoard().toString();
		assertFalse(beforeMove.equals(afterMove));
		
		JButton b2 = new JButton();
		b2.addActionListener(new UndoAction(app, lb));
		b2.doClick(); //should undo move and push move onto redo stack
		
		assertEquals(beforeMove, lb.getCurrentLevel().getBoard().toString());
		
		JButton b3 = new JButton();
		b3.addActionListener(new RedoAction(app, lb));
		b3.doClick(); //should redo move and push move onto undo stack
		
		assertEquals(afterMove, lb.getCurrentLevel().getBoard().toString());
		
		b3.doClick(); //test redo with empty redo stack
		
		assertEquals(afterMove, lb.getCurrentLevel().getBoard().toString());
		
		b2.doClick(); //should undo move and push move onto redo stack
		b2.doClick(); //test undo with empty undo stack
		
		assertEquals(beforeMove, lb.getCurrentLevel().getBoard().toString());
		
		app.dispose();
	}
	
}
