package diomedes.levelbuilder.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import diomedes.levelbuilder.LevelBuilderTestCase;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.BoardView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;

public class TestBoardController extends LevelBuilderTestCase
{
	public void testPieceToBoardFromToolbar()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getPieces().get(0);
		app.getEditLevelView().getPieceToolbarView().getToolbar().setSelectedPiece(p);
		
		
		BoardView bv = app.getEditLevelView().getBoardView();
		MouseEvent mousePressed = createPressed(app, bv, 60, 60);
		MouseListener[] ml = bv.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressed);
			}
		}
		
		assertEquals(1, app.getModel().getCurrentLevel().getBoard().getPieces().size());
		
		app.dispose();
	}
	
	public void testPieceToBoardFromBullpen()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		Piece p = app.getModel().getCurrentLevel().getBullpen().getPieces().get(0);
		app.getModel().getCurrentLevel().getBullpen().setSelectedPiece(p);
		
		
		BoardView bv = app.getEditLevelView().getBoardView();
		MouseEvent mousePressed = createPressed(app, bv, 60, 60);
		MouseListener[] ml = bv.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressed);
			}
		}
		
		assertEquals(1, app.getModel().getCurrentLevel().getBoard().getPieces().size());
		
		app.dispose();
	}
	
	public void testSelectingPieceInBoard()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getPieces().get(0);
		app.getEditLevelView().getPieceToolbarView().getToolbar().setSelectedPiece(p);
		
		
		BoardView bv = app.getEditLevelView().getBoardView();
		MouseEvent mousePressed = createPressed(app, bv, 60, 60);
		MouseListener[] ml = bv.getMouseListeners();
		BoardController bc = null;
		for(MouseListener m: ml) {
			if(m instanceof BoardController) {
				bc = (BoardController) m;
				bc.mousePressed(mousePressed);
			}
		}
		
		assertEquals(1, app.getModel().getCurrentLevel().getBoard().getPieces().size());
		
		mousePressed = createPressed(app, bv, 60, 60);
		if(bc != null)
		{
			bc.mousePressed(mousePressed);
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() != null);
		
		app.dispose();
	}
	
	public void testMoveInBoard()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getPieces().get(0);
		Position pos = new Position(app.getEditLevelView().getBoardView().getColFromX(60), app.getEditLevelView().getBoardView().getRowFromY(60));
		PlacedPiece pp = new PlacedPiece(pos, p);
		app.getModel().getCurrentLevel().getBoard().selectPiece(pp);
		app.getModel().getCurrentLevel().getBoard().addPiece(pp);
		
		BoardView bv = app.getEditLevelView().getBoardView();
		MouseEvent mousePressed = createPressed(app, bv, 100, 100);
		MouseListener[] ml = bv.getMouseListeners();
		BoardController bc = null;
		for(MouseListener m: ml) {
			if(m instanceof BoardController) {
				bc = (BoardController) m;
				bc.mousePressed(mousePressed);
			}
		}
		
		assertEquals(1, app.getModel().getCurrentLevel().getBoard().getPieces().size());
		app.dispose();
	}
	
	public void testDeactivatingTile()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		
		BoardView bv = app.getEditLevelView().getBoardView();
		MouseEvent mousePressed = createPressed(app, bv, 60, 60);
		MouseListener[] ml = bv.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressed);
				bc.mousePressed(mousePressed);
			}
		}
		
		Position pos = new Position(app.getEditLevelView().getBoardView().getColFromX(60), app.getEditLevelView().getBoardView().getRowFromY(60));
		assertTrue(app.getModel().getCurrentLevel().getBoard().getTileAtPosition(pos).isActive());
	}
	
	public void testHover()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		
		BoardView bv = app.getEditLevelView().getBoardView();
		MouseEvent mousePressed = createPressed(app, bv, 60, 60);
		MouseListener[] ml = bv.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mouseMoved(mousePressed);
				//Stupid code coverage thing for empty methods
				bc.mouseDragged(mousePressed);
				bc.mouseReleased(mousePressed);
				bc.mouseClicked(mousePressed);
				bc.mouseEntered(mousePressed);
				bc.mouseExited(mousePressed);
			}
		}
	}
	
//	public void testAddNumber()
//	{
//		LevelBuilderModel lb = new LevelBuilderModel();
//		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
//		for(int i = 1; i <= 10; i++) {
//			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
//		}
//		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
//		lb.setCurrentLevel(puzzle);
//		Application app = new Application(lb);
//		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
//		app.getEditLevelView().getActionToolbarView().getAddNumberButton().setSelected(true);
//		
//		BoardView bv = app.getEditLevelView().getBoardView();
//		MouseEvent mousePressed = createPressed(app, bv, 60, 60);
//		MouseListener[] ml = bv.getMouseListeners();
//		for(MouseListener m : ml) {
//			if(m instanceof BoardController) {
//				BoardController bc = (BoardController) m;
//				bc.mouseMoved(mousePressed);
//			}
//		}
//	}
}
