package diomedes.levelbuilder.control;

import javax.swing.JTextField;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.RectangleBoardFactory;
import junit.framework.TestCase;

/**
 * 
 * @author Kenedi
 *
 */

public class TestSetMaxTime extends TestCase{
	LevelBuilderModel lbModel;
	SetMaxTime move;
	JTextField f = new JTextField();
	
	@Override
	protected void setUp() {
		lbModel = new LevelBuilderModel();
		
		Board board = RectangleBoardFactory.makeRectangleBoard(6,6);
		Level testLevel = new Lightning(board, null, 200);
		lbModel.addLevel(testLevel);
		lbModel.setCurrentLevel(testLevel);
	}
	
	public void testConstructor(){
		move = new SetMaxTime(587, f);
		assertEquals (587, move.maxTime);
	}
	
	public void testIsValid(){
		move = new SetMaxTime(673, f);
		
		assertTrue(move.isValid(lbModel));
	}
	
	public void testSetMaxTime(){
		move = new SetMaxTime(587, f);
		move.doMove(lbModel);
		Level level = (Lightning)lbModel.getCurrentLevel();
		assertEquals (587, ((Lightning)level).getMaxTime());
	}

	public void testUndoMove(){
		move = new SetMaxTime(587, f);
		move.doMove(lbModel);
		
		SetMaxTime move2 = new SetMaxTime(1000, f);
		move2.doMove(lbModel);
		
		Level level = (Lightning)lbModel.getCurrentLevel();
		
		assertEquals (1000, ((Lightning)level).getMaxTime());
		
		move.undoMove(lbModel);
		
		assertEquals (587, ((Lightning)level).getMaxTime());
	}
}
