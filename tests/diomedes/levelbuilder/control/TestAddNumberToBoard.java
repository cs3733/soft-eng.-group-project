package diomedes.levelbuilder.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;

import diomedes.levelbuilder.LevelBuilderTestCase;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.ActionToolbarView;
import diomedes.levelbuilder.view.panel.BoardView;
import diomedes.shared.entity.Release;

public class TestAddNumberToBoard extends LevelBuilderTestCase {

	public void testAddRemoveNumberToBoard() {
		LevelBuilderModel lb = new LevelBuilderModel();
		Application app = new Application(lb);

		JButton createRelease = new JButton();
		createRelease.addActionListener(new CreateReleaseLevel(app));
		createRelease.doClick();
		
		ActionToolbarView atv = app.getEditLevelView().getActionToolbarView();
		ButtonGroup colors = atv.getColorButtonGroup();
		ButtonGroup numbers = atv.getNumberButtonGroup();
		Enumeration<AbstractButton> colorEnum = colors.getElements();
		JRadioButton redButton = (JRadioButton) colorEnum.nextElement();
		JRadioButton blueButton = (JRadioButton) colorEnum.nextElement();
		JRadioButton greenButton = (JRadioButton) colorEnum.nextElement();
		redButton.doClick();
		JRadioButton oneButton = (JRadioButton) numbers.getElements().nextElement();
		oneButton.doClick();
		JToggleButton addNumbers = atv.getAddNumberButton();
		addNumbers.doClick();
		
		Release lvl = (Release) app.getModel().getCurrentLevel();
		BoardView bv = app.getEditLevelView().getBoardView();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		MouseEvent mousePressedOnBoard = createPressed(app, bv, -51, -58);
		MouseListener[] ml = bv.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressedOnBoard);
			}
		}
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 1);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		greenButton.doClick();
		
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressedOnBoard);
			}
		}
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 1);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		blueButton.doClick();
		
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressedOnBoard);
			}
		}
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 1);
		
		MouseEvent mouseRightClickBoard = createRightClick(app, bv, -51, -58);
		ml = bv.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mouseRightClickBoard);
			}
		}
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		JButton undoButton = app.getEditLevelView().getActionToolbarView().getUndoButton();
		JButton redoButton = app.getEditLevelView().getActionToolbarView().getRedoButton();
		
		undoButton.doClick();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 1);
		
		redoButton.doClick();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		app.dispose();
	}
	
	public void testRandomNumbersAndClearNumbers() {
		LevelBuilderModel lb = new LevelBuilderModel();
		Application app = new Application(lb);

		JButton createRelease = new JButton();
		createRelease.addActionListener(new CreateReleaseLevel(app));
		createRelease.doClick();
		
		JButton randomNumbers = new JButton();
		randomNumbers.addActionListener(new AddRandomNumbers(app));
		
		JButton clearNumbers = new JButton();
		clearNumbers.addActionListener(new ClearNumbers(app));
		
		Release lvl = (Release) app.getModel().getCurrentLevel();
		BoardView bv = app.getEditLevelView().getBoardView();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		randomNumbers.doClick();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 6);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 6);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 6);
		
		clearNumbers.doClick();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		JButton undoButton = app.getEditLevelView().getActionToolbarView().getUndoButton();
		JButton redoButton = app.getEditLevelView().getActionToolbarView().getRedoButton();
		
		undoButton.doClick();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 6);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 6);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 6);
		
		redoButton.doClick();
		
		assertTrue(lvl.getTileGroups().get(0).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(1).getSize() == 0);
		assertTrue(lvl.getTileGroups().get(2).getSize() == 0);
		
		app.dispose();
	}
	
}
