package diomedes.levelbuilder.control;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import junit.framework.TestCase;

public class TestEditLevelController extends TestCase {

	public void testEntryMethod() {
		Application app = new Application(new LevelBuilderModel());
		EditLevel testEditLevel = new EditLevel(app);

		//Generate a mouse event to test if nothing is selected, and that the app doesn't switch panels
		testEditLevel.mouseClicked(null);
		
		//Set the selected level, and validate the mouseClicked method
		app.getLevelSelectView().setUpTable();
		
		app.dispose();
	}
}
