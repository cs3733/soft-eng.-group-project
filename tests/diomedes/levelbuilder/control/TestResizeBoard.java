package diomedes.levelbuilder.control;

import java.util.ArrayList;

import javax.swing.JButton;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Puzzle;
import junit.framework.TestCase;

/**
 * 
 * @author Kyle Carrero
 *
 */
public class TestResizeBoard extends TestCase {

	public void testResizeBoard() {
		LevelBuilderModel lb = new LevelBuilderModel();
		lb.setCurrentLevel(new Puzzle(new Board(), new Bullpen(new ArrayList<Piece>()), 1, 10));
		Application app = new Application(lb);
		EditLevelView lv = app.getEditLevelView();
		lv.setLevelToEdit(lb.getCurrentLevel());

		String boardBefore = app.getModel().getCurrentLevel().getBoard().toString();
		assertEquals(boardBefore, app.getModel().getCurrentLevel().getBoard().toString());

		/* Test decrement width */
		JButton b1 = new JButton();
		ResizeBoard rb1 = new ResizeBoard(app,lv, true, false);
		b1.addActionListener(rb1);
		b1.doClick();

		assertFalse(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));

		/* Test increment width */
		JButton b2 = new JButton();
		ResizeBoard rb2 = new ResizeBoard(app,lv, true, true);
		b2.addActionListener(rb2);
		b2.doClick();

		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		b2.doClick();
		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));

		/* Test decrement height */
		JButton b3 = new JButton();
		ResizeBoard rb3 = new ResizeBoard(app, lv, false, false);
		b3.addActionListener(rb3);
		b3.doClick();

		assertFalse(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));

		/* Test increment height */
		JButton b4 = new JButton();
		ResizeBoard rb4 = new ResizeBoard(app, lv, false, true);
		b4.addActionListener(rb4);
		b4.doClick();

		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		b4.doClick();
		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		
		app.dispose();
	}

	public void testUndoMove() {
		
		LevelBuilderModel lb = new LevelBuilderModel();
		lb.setCurrentLevel(new Puzzle(new Board(), new Bullpen(new ArrayList<Piece>()), 1, 10));
		Application app = new Application(lb);
		EditLevelView lv = app.getEditLevelView();
		lv.setLevelToEdit(lb.getCurrentLevel());

		String boardBefore = app.getModel().getCurrentLevel().getBoard().toString();
		assertEquals(boardBefore, app.getModel().getCurrentLevel().getBoard().toString());

		
		/* Test undo move for negative resize width */
		ResizeBoardMove rbm1 = new ResizeBoardMove(app, lv, true, false);

		rbm1.doMove(lb);
		assertFalse(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		rbm1.undoMove(lb);
		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		rbm1.undoMove(lb);
		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		while(rbm1.doMove(lb)) {
			assertFalse(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		}
		
		
		/* Test undo move for positive resize width */
		ResizeBoardMove rbm2 = new ResizeBoardMove(app, lv, true, true);
		String boardBefore2 = app.getModel().getCurrentLevel().getBoard().toString();
		
		rbm2.doMove(lb);
		assertFalse(boardBefore2.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		rbm2.undoMove(lb);
		assertTrue(boardBefore2.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		while(rbm2.doMove(lb)) {
			assertFalse(boardBefore2.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		}
		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		
		
		/* Test undo move for negative resize height */
		ResizeBoardMove rbm3 = new ResizeBoardMove(app, lv, false, false);

		rbm3.doMove(lb);
		assertFalse(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		rbm3.undoMove(lb);
		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		while(rbm3.doMove(lb)) {
			assertFalse(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		}
		
		
		/* Test undo move for positive resize height */
		ResizeBoardMove rbm4 = new ResizeBoardMove(app, lv, false, true);
		boardBefore2 = app.getModel().getCurrentLevel().getBoard().toString();
		
		rbm4.doMove(lb);
		assertFalse(boardBefore2.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		rbm4.undoMove(lb);
		assertTrue(boardBefore2.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		while(rbm4.doMove(lb)) {
			assertFalse(boardBefore2.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		}
		assertTrue(boardBefore.equals(app.getModel().getCurrentLevel().getBoard().toString()));
		
		app.dispose();
	}

}
