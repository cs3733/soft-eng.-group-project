package diomedes.levelbuilder.control;

import java.util.HashMap;

import javax.swing.JTextField;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.RectangleBoardFactory;
import diomedes.shared.entity.Tile;
import junit.framework.TestCase;

public class TestSetNumMaxMoves extends TestCase{
	LevelBuilderModel lbModel;
	SetNumMaxMoves move;
	JTextField f = new JTextField();
	
	@Override
	protected void setUp() {
		lbModel = new LevelBuilderModel();
		HashMap<Position, Tile> testTiles = new HashMap<Position, Tile>();
		
		Board board = RectangleBoardFactory.makeRectangleBoard(6,6);
		Level testLevel = new Puzzle(board, null, 1, 200);
		lbModel.addLevel(testLevel);
		lbModel.setCurrentLevel(testLevel);
	}
	
	public void testConstructor(){
		move = new SetNumMaxMoves(20, f);
		assertEquals (20, move.maxMoves);
	}
	
	public void testIsValid(){
		move = new SetNumMaxMoves(673, f);
		
		assertTrue(move.isValid(lbModel));
	}
	
	public void testSetNumMaxMoves(){
		move = new SetNumMaxMoves(17, f);
		move.doMove(lbModel);
		Level level = (Puzzle)lbModel.getCurrentLevel();
		assertEquals (17, ((Puzzle)level).getMaxMoves());
	}
	
	public void testUndoMove(){
		move = new SetNumMaxMoves(7, f);
		move.doMove(lbModel);
		
		SetNumMaxMoves move2 = new SetNumMaxMoves(10, f);
		move2.doMove(lbModel);
		
		Level level = (Puzzle)lbModel.getCurrentLevel();
		
		assertEquals (10, ((Puzzle)level).getMaxMoves());
		
		move.undoMove(lbModel);
		
		assertEquals (7, ((Puzzle)level).getMaxMoves());
	}
}
