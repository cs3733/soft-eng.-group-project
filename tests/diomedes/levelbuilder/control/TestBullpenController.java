package diomedes.levelbuilder.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;

import diomedes.levelbuilder.LevelBuilderTestCase;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.BoardView;
import diomedes.levelbuilder.view.panel.BullpenView;
import diomedes.levelbuilder.view.panel.PieceToolbarView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;

public class TestBullpenController extends LevelBuilderTestCase {
	
	public void testSelectPieceInBullpen() {
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		BullpenView bv = app.getEditLevelView().getBullpenView();
		bv.update();
		MouseEvent mouseClicked = createClicked(app, bv.getPieceViews(), 40, 40);
		MouseListener[] ml = bv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null);
		
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() == null);
		
		app.dispose();
	}
	
	public void testMovePieceToBoardFromBullpen() {
		LevelBuilderModel lb = new LevelBuilderModel();
		Application app = new Application(lb);

		JButton createPuzzle = new JButton();
		createPuzzle.addActionListener(new CreatePuzzleLevel(app));
		createPuzzle.doClick();
		
		PieceToolbarView ptv = app.getEditLevelView().getPieceToolbarView();
		MouseEvent mouseClicked = createClicked(app, ptv.getPieceViews(), 40, 40);
		MouseListener[] ml = ptv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof PieceToolbarController) {
				PieceToolbarController bc = (PieceToolbarController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertTrue(app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null);
		
		BullpenView bv = app.getEditLevelView().getBullpenView();
		bv.update();
		mouseClicked = createClicked(app, bv.getPieceViews(), 40, 40);
		ml = bv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertFalse(app.getModel().getCurrentLevel().getBullpen().getPieces().isEmpty());
//		System.out.println("Piece ID = " + app.getModel().getCurrentLevel().getBullpen().getSelectedPiece().getId());
		
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null);
		
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() == null);
		
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null);
		
		BoardView boardView = app.getEditLevelView().getBoardView();
		MouseEvent mousePressedOnBoard = createPressed(app, boardView, 0, 0);
		ml = boardView.getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressedOnBoard);
				boardView.repaint();
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() == null);
		assertFalse(app.getModel().getCurrentLevel().getBoard().getPieces().isEmpty());
		assertTrue(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() == null);
		
		for(MouseListener m : ml) {
			if(m instanceof BoardController) {
				BoardController bc = (BoardController) m;
				bc.mousePressed(mousePressedOnBoard);
				boardView.repaint();
			}
		}
		
		assertTrue(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() != null);
		
		app.dispose();
	}
	
	public void testMovePieceToBullpen()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 1; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		BullpenView bv = app.getEditLevelView().getBullpenView();
		bv.update();
		
		Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getPieces().get(0);
		Position pos = new Position(app.getEditLevelView().getBoardView().getColFromX(60), app.getEditLevelView().getBoardView().getRowFromY(60));
		PlacedPiece pp = new PlacedPiece(pos, p);
		app.getModel().getCurrentLevel().getBoard().selectPiece(pp);
		app.getModel().getCurrentLevel().getBoard().addPiece(pp);
		
		MouseEvent mouseClicked = createClicked(app, bv.getPieceViews(), 40, 40);
		MouseListener[] ml = bv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertEquals(0, app.getModel().getCurrentLevel().getBoard().getPieces().size());
		
		app.getModel().getCurrentLevel().getBoard().selectPiece(pp);
		app.getModel().getCurrentLevel().getBoard().addPiece(pp);
		
		mouseClicked = createClicked(app, bv.getPieceViews(), 400, 40);
		ml = bv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertEquals(0, app.getModel().getCurrentLevel().getBoard().getPieces().size());
		
		app.dispose();
	}
	
	public void testMoveToBullpenFromToolbar()
	{
		LevelBuilderModel lb = new LevelBuilderModel();
		Bullpen bullpen = new Bullpen(new ArrayList<Piece>());
		for(int i = 1; i <= 10; i++) {
			bullpen.addPiece(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		Puzzle puzzle = new Puzzle(new Board(), bullpen, 0, 10);
		lb.setCurrentLevel(puzzle);
		Application app = new Application(lb);
		app.getEditLevelView().setLevelToEdit(lb.getCurrentLevel());
		BullpenView bv = app.getEditLevelView().getBullpenView();
		bv.update();
		
		Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getPieces().get(0);
		app.getEditLevelView().getPieceToolbarView().getToolbar().setSelectedPiece(p);
		
		MouseEvent mouseClicked = createClicked(app, bv.getPieceViews(), 40, 40);
		MouseListener[] ml = bv.getPieceViews().getMouseListeners();
		for(MouseListener m : ml) {
			if(m instanceof BullpenController) {
				BullpenController bc = (BullpenController) m;
				bc.mouseClicked(mouseClicked);
			}
		}
		
		assertEquals(0, app.getModel().getCurrentLevel().getBoard().getPieces().size());
	}
}
