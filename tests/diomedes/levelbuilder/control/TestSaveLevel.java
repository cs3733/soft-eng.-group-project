package diomedes.levelbuilder.control;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.RectangleBoardFactory;
import diomedes.shared.entity.Resources;
import diomedes.shared.entity.Tile;
import junit.framework.TestCase;

public class TestSaveLevel extends TestCase {
	LevelBuilderModel lbModel;
	SaveLevel sl;
	
	@Override
	public void setUp() {
		lbModel = new LevelBuilderModel();
		HashMap<Position, Tile> testTiles = new HashMap<Position, Tile>();
		
		Board board = RectangleBoardFactory.makeRectangleBoard(6,6);
		ArrayList<Piece> bullpenPieces = new ArrayList<Piece>();
		bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(1));
		bullpenPieces.add(KabasujiPieceFactory.makeNumberedPiece(2));
		Bullpen bullpen = new Bullpen(bullpenPieces);
		Level testLevel = new Puzzle(board, bullpen, 200, 200);
		testLevel.isUnlocked();
		lbModel.addLevel(testLevel);
		lbModel.setCurrentLevel(testLevel);
	}
	public void test() {
		sl = new SaveLevel(lbModel);
		//Save the level
		sl.actionPerformed(null);
		
		File folder = new File(Resources.puzzleDir);
		File[] fileList = folder.listFiles(new FilenameFilter() {
							@Override
							public boolean accept(File dir, String name) {
								return name.contains("200.level");
							}
						});
		
		File fileToCheck = new File(Resources.puzzleDir+"level200.level");
		//Validate that the file exists
		assertTrue(fileToCheck.exists());
		//Delete it from the disk so that it doesn't affect playing the game
		fileToCheck.delete();
		
	}
}
