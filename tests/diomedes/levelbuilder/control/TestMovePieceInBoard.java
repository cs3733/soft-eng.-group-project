package diomedes.levelbuilder.control;

import java.security.InvalidParameterException;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.RectangleBoardFactory;
import junit.framework.TestCase;

/**
 * Tests the MovePieceInBoard class
 * @author Jordan Burklund
 *
 */
public class TestMovePieceInBoard extends TestCase {
	LevelBuilderModel lbModel;
	MovePieceInBoard move;
	
	@Override
	protected void setUp() {
		lbModel = new LevelBuilderModel();
		
		Board board = RectangleBoardFactory.makeRectangleBoard(6,6);
		Level testLevel = new Puzzle(board, null, 1, 200);
		lbModel.addLevel(testLevel);
		lbModel.setCurrentLevel(testLevel);
	}
	
	//Tests the constructor with null values
	public void testConstructorNull() {
		try{
			move = new MovePieceInBoard(null,null);
			fail();
		} catch(InvalidParameterException e) {
			//Test has passed
		} catch(Exception e) {
			//A different type of excpetion was thrown
			fail();
		}
		try{
			move = new MovePieceInBoard(new Position(0,1),null);
			fail();
		} catch(InvalidParameterException e) {
			//Test has passed
		} catch(Exception e) {
			//A different type of excpetion was thrown
			fail();
		}
	}
	
	//Tests the valid method for the move
	public void testValid() {
		//Add a piece to an empty board
		Board board = lbModel.getCurrentLevel().getBoard();
		PlacedPiece piece1 = new PlacedPiece(new Position(0,2),
									KabasujiPieceFactory.makeNumberedPiece(1));
		board.addPiece(piece1);
		//Add another piece that will intersect the second move
		PlacedPiece piece2 = new PlacedPiece(new Position(2,2), 
									KabasujiPieceFactory.makeNumberedPiece(1));
		board.addPiece(piece2);
		
		//Try to move the piece
		move = new MovePieceInBoard(new Position(1,2),piece1);
		assertTrue(move.isValid(lbModel));
		move.doMove(lbModel);
		
		//Try to move the piece that will intersect another
		move = new MovePieceInBoard(new Position(1,2), piece2);
		assertFalse(move.isValid(lbModel));
	}
	
	//Tests the doMove method
	public void testDoMove() {
		//Add a piece to an empty board
		Board board = lbModel.getCurrentLevel().getBoard();
		PlacedPiece piece1 = new PlacedPiece(new Position(0,2),
									KabasujiPieceFactory.makeNumberedPiece(1));
		board.addPiece(piece1);
		//Add another piece that will intersect the second move
		PlacedPiece piece2 = new PlacedPiece(new Position(2,2), 
									KabasujiPieceFactory.makeNumberedPiece(1));
		board.addPiece(piece2);
		//Piece that doesn't get added to the board
		PlacedPiece piece3 = new PlacedPiece(new Position(4,2),
									KabasujiPieceFactory.makeNumberedPiece(1));
		
		//Try to move the piece
		move = new MovePieceInBoard(new Position(1,2),piece1);
		assertTrue(move.doMove(lbModel));
		//Check that the move executed
		assertTrue(board.getPieces().contains(new PlacedPiece(new Position(1,2), piece1.getPiece())));
		
		
		//Try to move the piece that will intersect another
		move = new MovePieceInBoard(new Position(1,2), piece2);
		assertFalse(move.doMove(lbModel));
		assertFalse(move.isValid(lbModel));
		//Check that the move did not execute, and that the piece is in it's orriginal position
		assertTrue(board.getPieces().contains(new PlacedPiece(new Position(2,2), piece2.getPiece())));
		
		//Try to move a piece that is not in the board
		move = new MovePieceInBoard(new Position(3,2), piece3);
		assertFalse(move.doMove(lbModel));
	}
	
	//Tests the undoMove method
	public void testUndoMove() {
		//Tests that a piece is successfully moved, and then moved back
		Board board = lbModel.getCurrentLevel().getBoard();
		PlacedPiece piece1 = new PlacedPiece(new Position(0,2),
									KabasujiPieceFactory.makeNumberedPiece(1));
		board.addPiece(piece1);
		
		move = new MovePieceInBoard(new Position(1,2),piece1);
		assertTrue(move.doMove(lbModel));
		move.undoMove(lbModel);
		//Check that the piece is in it's old position and not the new position
		assertTrue(board.getPieces().contains(piece1));
		assertFalse(board.getPieces().contains(new PlacedPiece(new Position(1,2), piece1.getPiece())));
	}
}
