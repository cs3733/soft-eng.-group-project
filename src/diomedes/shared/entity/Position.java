package diomedes.shared.entity;

import java.io.Serializable;

/**
 * Position entitie, with a column and a row.
 * @author Myles Spencer
 * @author Jordan Burklund
 */
public class Position implements Serializable {

	int row;
	int col;

	/**
	 * @param int col
	 * @param int row 
	 */
	public Position(int col, int row) {
		this.row = row;
		this.col = col;
	}
	
	/**
	 * Gets the row value for the position
	 * @return row
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * Gets the column value for the position
	 * @return col
	 */
	public int getCol() {
		return col;
	}
	
	/**
	 * Checks if two Positions are at the same point
	 * @return true only if the two positions are at the same point
	 */
	@Override
	public boolean equals(Object o) {
		//Ensure that o is non null
		if(o == null) { return false; }
		
		//Attempt to cast 0 as a position, and catch the error if it cannot be casted (not a Position object)
		try {
			Position temp = (Position) o;
			//Check that the row and column match
			if((temp.getCol() == this.col) && (temp.getRow() == this.row)) {
				//They match, so these objects are equal
				return true;
			} else {
				return false;
			}
		} catch(ClassCastException e) {
			//Ignore the error, since the object cannot be casted
			return false;
		}
	}
	
	/**
	 * Creates a duplicate of this Position by creating a new Position and copying the values into it
	 * @return A Position with exactly the same values
	 */
	public Position copy() {
		return new Position(this.col, this.row);
	}
	
	/**
	 * Generates a string representation of the Position
	 * @return String in the format of "<row,col>"
	 */
	@Override
	public String toString() {
		return "<"+getCol()+","+getRow()+">";
	}
	
	/**
	 * Derelatizes a position by adding the current position and the given position, therefore making the current
	 * position no longer relative to a piece (or whatever it was relative to before)
	 * @param pos
	 * @return A new Position that is derelatized
	 */
	public Position derelatize(Position pos)
	{
		return new Position(pos.getCol() + col, pos.getRow() + row);
	}
	
	/**
	 * Calculates the hash value for this position.
	 * Values are hashed by (100*col + row)
	 * Note: This only works for positive values of row and column < 100
	 * TODO Not optimized, but it works
	 * @return a hash code for this position
	 */
	@Override
	/**
	 * Col*100 to differentaite row and column hash code
	 */
	public int hashCode() {
		return 100*col + row;
	}
}