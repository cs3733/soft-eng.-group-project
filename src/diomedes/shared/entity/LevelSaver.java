package diomedes.shared.entity;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.InvalidParameterException;

/**
 * Abstracts saving levels to memory
 * @author Jordan Burklund
 */
public class LevelSaver {
	Level[] levels;
	String folderPath;
	
	/**
	 * Constructor that takes a location to save the levels to,
	 * and a list of levels to save.
	 * @param folderPath String representing the path of the folder to save the levels in.
	 * Folder path can either end with / or can not have a trailing /
	 * @param levelsToSave List of levels to save
	 * @throws InvalidParameterException if either parameters are null
	 */
	public LevelSaver(String folderPath, Level[] levelsToSave) {
		if(folderPath == null) {
			throw new InvalidParameterException("Folder path cannot be null");
		}
		if(levelsToSave == null) {
			throw new InvalidParameterException("List of levels cannot be null");
		}
		this.folderPath = folderPath;
		this.levels = levelsToSave;
	}
	
	/**
	 * Saves the mementos of the levels to the specified folder
	 * @return true if the saving was successful
	 * @throws IOException throws an exception if the file cannot be loaded,
	 * or the data cannot be written to the file
	 */
	public boolean saveLevels() throws IOException {
		String fixedDirName;
		if(folderPath.endsWith("/")) {
			fixedDirName = folderPath;
		} else {
			fixedDirName = folderPath+"/";
		}
		for(Level level : levels) {
			//TODO change file extension based on Level type (llevel, plevel, rlevel)
			//TODO automatically save the levels in the correct location based on the level type
			FileOutputStream fileOutStream = new FileOutputStream(fixedDirName+levelFilename(level));
			ObjectOutputStream saveStream = new ObjectOutputStream(fileOutStream);
			saveStream.writeObject(level.generateMemento());
			saveStream.close();
			fileOutStream.close();
		}
		return true;
	}
	
	/**
	 * Generates the filename for the level
	 * @param level Level to generate the filename for
	 */
	public static String levelFilename(Level level) {
		return "level"+level.order+".level";
	}
}
