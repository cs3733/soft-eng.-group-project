package diomedes.shared.entity;


/**
 * Abstract class that contains characteristics of a level.
 * @author Aura Velarde
 */
public abstract class Level implements SaveableLevel {


	protected Board board;
	protected Bullpen bullpen;
	protected boolean unlocked;
	protected int order;
	protected int achievement;
	protected int currentAchievement;



	/**
	 * Level constructor
	 * @param Board board board for the level.
	 * @param Bullpen bullpen bullpen for the level.
	 * @param int order order of this particular level.
	 */
	public Level(Board board, Bullpen bullpen, int order) {
		this.board = board;
		this.bullpen = bullpen;
		this.unlocked = false;
		this.order = order;
		this.currentAchievement = 0;
	}

	public abstract boolean hasAchievement();
	
	/**
	 * Has an achievement been received? (NOT the current one)
	 * @return
	 */
	public boolean hadAchievement(){
		if(achievement > 0){
			return true;
		}
		return false;
	}
	

	/**
	 * 
	 */
	public abstract boolean hasWon();

	/**
	 * 
	 */
	public void unlockLevel() {
		this.unlocked = true;
	}
	public boolean isUnlocked() {
		return this.unlocked;
	}
	/**
	 * 
	 */
//	public abstract void setName(String name);
//	
//	public abstract String getName();

	public abstract String getVariationName();
	
	public int getOrder(){
		return this.order;
	}
	

	public int getAchievement(){
		return this.achievement;
	}
	
	public int getCurrentAchievement()
	{
		hasAchievement();
		return this.currentAchievement;
	}
	
	public void recordAchievement()
	{
		hasAchievement();
		if(this.currentAchievement > this.achievement)
		{
			this.achievement = this.currentAchievement;
		}
	}
	
	/**
	 * Gets the reference to the board in use
	 * @return board for this level
	 */
	public Board getBoard() {
		return board;
	}
	
	/**
	 * Gets the reference to the bullpen in use
	 * @return bullpen for this level
	 */
	public Bullpen getBullpen() {
		return bullpen;
	}
	
	/**
	 * Resets the level to a playable state with no pieces on the board
	 */
	public void reset()
	{
		//Return all pieces to the bullpen and remove them from the board
		for(PlacedPiece pp: board.getPieces())
		{
			bullpen.addPiece(pp.getPiece().copy());
		}
		board.getPieces().clear();
		
		//Make sure all tiles are uncovered(for lightning)
		for(Tile t: board.getTiles())
		{
			t.setIsCovered(false);
		}
		this.currentAchievement = 0;
		
		recalculate();
	}
	
	public void discard()
	{
		//Return all pieces to the bullpen and remove them from the board
		for(Piece bullpenPiece: bullpen.getPieces())
		{
			bullpen.removePiece(bullpenPiece); 
		}
		board.getPieces().clear();
		for(Tile tiles: board.getTiles()){
			tiles.activateTile();
		}
		//Make sure all tiles are uncovered(for lightning)
		for(Tile t: board.getTiles())
		{
			t.setIsCovered(false);
		}
		if (this.getVariationName().equalsIgnoreCase("Puzzle")){
			Level level2 = (Puzzle)this;
			
			((Puzzle) level2).setNumMaxMoves(0);
		}
		if (this.getVariationName().equalsIgnoreCase("Lightning")){
			Level level3 = (Lightning)this;
			
			((Lightning) level3).setMaxTime(0);
		}
	}
	
	/**
	 * Recalculates any attributes that it can to make sure they are up to date
	 */
	public abstract void recalculate();
	
}