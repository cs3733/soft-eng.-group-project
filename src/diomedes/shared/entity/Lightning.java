package diomedes.shared.entity;

//import java.util.Timer;

/**
 * Lightning class that stores the state of the game.
 * @author Aura Velarde
 * @author Myles Spencer
 */
public class Lightning extends Level {

	public static final String NAME = "Lightning";

	int maxTime;
	int numTilesLeft;

	private int boardWidth;

	private int boardHeight;

	/**
	 * Constructor for Lightning
	 * @param Board board 
	 * @param Bullpen bullpen 
	 * @param int order
	 */
	
	
	public Lightning(Board board, Bullpen bullpen, int order) {
		super(board, bullpen, order);
		this.numTilesLeft = board.getSize();
	}
	
	/**Set the number of tiles left
	 * @param numTilesLeft number of tiles left */
	public void setNumTilesLeft(int numTilesLeft){
		this.numTilesLeft = numTilesLeft;
	}
	
	/**
	 * Decreases the number of tiles left by the amount specified
	 * @param decrease amount specified! cannot be greater than six, nor leave a negative number
	 */
	public void decreaseNumTilesLeft(int decrease){
		this.numTilesLeft = this.numTilesLeft - decrease;
	}
	
	/** Get the number of tiles left */
	public int getNumTilesLeft(){
		return numTilesLeft;
	}

	/**
	 * If each tile on the board has been touched by the piece. The game has been wone */
	public boolean hasWon() {
		if(numTilesLeft == 0){
			return true;
		}
		return false;
	}

	/**Has the user gained an achievement? If so, have they won? */
	public boolean hasAchievement() {
		if(hasWon()){
			currentAchievement = 3;
			return true;
		}
		else if(numTilesLeft <= 6){
			currentAchievement = 2;
			return true;
		}
		else if (numTilesLeft <= 12){
			currentAchievement = 1;
			return true;
		}
		return false;
	}

	/**
	 * Returns the variation name of the specific level
	 */
	@Override
	public String getVariationName() {
		return NAME;
	}


//	@Override
//	public void setName(String name) {
//		this.lvlName = name;
//	}
//	
//	@Override
//	public String getName() {
//		return this.lvlName;
//	}
	
	/**Sets the max amount of time
	 * @param int max Amount of times in second, cannot be negative */
	public void setMaxTime(int max){
		this.maxTime = max;
	}

	/**Retrieves the amount of time allowed to complete level */
	public int getMaxTime(){
		return maxTime;
	}
	@Override
	public String toString() {
		String intro = "Lightning Level unlocked?: " + unlocked + "\n" ;
		String level = ("The Level " + getVariationName() + "\n");
		String won = ("has: " + achievement + " stars, thus has the player won?: " + hasWon());
		return intro + level + won;

	}

	/** Generates a memento so that this level can be saved	 
	 * @return LevelMemento representing the important information for the level.
	 * Returns a LightningMemento
	 * @author Jordan Burklund
	 */
	@Override
	public LevelMemento generateMemento() {
		return new LightningMemento(bullpen.pieces, board.pieces, board.tiles, board.hints,
									board.width, board.height, this.unlocked, this.order, this.achievement,
									this.maxTime);
	}

	/** Restores the level to the state described by the given LightningMemento
	 * @param memento LightningMemento to restore the level to
	 * @return false if the level could not be restored from the memento.
	 * Could be caused by not passing in a LightningMemento, or corrupt data
	 * in the memento
	 * @author Jordan Burklund
	 */
	@Override
	public boolean restoreFromMemento(LevelMemento memento) {
		if(memento == null) {
			return false;
		}
		if(!(memento instanceof LightningMemento)) {
			return false;
		}
		LightningMemento restore = (LightningMemento) memento;
		this.bullpen = new Bullpen(restore.bullpenPieces);
		this.board = new Board(restore.boardTiles);
		this.board.pieces = restore.boardPieces;
		this.board.hints.addAll(restore.boardHints);
		this.board.setWidth(restore.boardWidth);
		this.board.setHeight(restore.boardHeight);
		this.unlocked = restore.unlocked;
		this.order = restore.order;
		this.achievement = restore.achievement;
		this.maxTime = restore.maxTime;
		return true;
	}

	@Override
	/**
	 * Resets the number of tiles left
	 */
	public void recalculate()
	{
		this.numTilesLeft = board.getSize();
	}


}

