package diomedes.shared.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Class used for saving relevant information about a Puzzle Level.
 * @author Jordan Burklund
 */
public class PuzzleMemento extends LevelMemento {
	int maxMoves;
	int numTilesLeft;//Set by the size of the board @autor Aura

	public PuzzleMemento(ArrayList<Piece> bullpenPieces, HashSet<PlacedPiece> boardPieces,
			HashMap<Position, Tile> boardTiles, HashSet<PlacedPiece> boardHints,
			int boardWidth, int boardHeight, boolean unlocked, int order, int achievement, int maxMoves, int numTilesLeft) {
		super(bullpenPieces, boardPieces, boardTiles, boardHints, boardWidth, boardHeight, unlocked, order, achievement);
		this.maxMoves = maxMoves;
		this.numTilesLeft = numTilesLeft;
	}

}
