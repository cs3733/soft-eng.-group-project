package diomedes.shared.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Class used for saving relevant information about a Lightning Level.
 * @author Jordan Burklund
 */
public class LightningMemento extends LevelMemento {
	/** Amount of time for the Level **/
	int maxTime;

	public LightningMemento(ArrayList<Piece> bullpenPieces, HashSet<PlacedPiece> boardPieces,
			HashMap<Position, Tile> boardTiles, HashSet<PlacedPiece> boardHints,
			int boardWidth, int boardHeight, boolean unlocked, int order, int achievement, int maxTime) {
		super(bullpenPieces, boardPieces, boardTiles, boardHints, boardWidth, boardHeight, unlocked, order, achievement);
		this.maxTime = maxTime;
	}

}
