package diomedes.shared.entity;

/**
 * Puzzle entity class
 * @author Aura Velarde
 * @author Myles Spencer
 * @author Kenedi
 * 
 */
public class Puzzle extends Level {

	public static final String NAME = "Puzzle";


	int maxMoves;
	int numMovesLeft;
	int numTilesLeft;
//	String lvlName;
	int numPieceLeft;


private int boardWidth;


private int boardHeight;

	/**
	 * Creates a valid puzzle level.
	 * Order must be 1
	 * MaxMoves must be greater than 0
	 * @param Board board 
	 * @param Bullpen bullpen 
	 * @param int maxMoves
	 */
	public Puzzle(Board board, Bullpen bullpen,int order, int maxMoves) {
		super(board, bullpen, order);
		this.maxMoves = maxMoves;
		this.numMovesLeft = maxMoves;
	}
	
	/**
	 * Set the number of tiles left to be covered depending on the size of the board.
	 * 
	 * @param numTilesLeft Must be a multiple of six
	 */
	public void setNumTilesLeft(int numTilesLeft){
		this.numTilesLeft = numTilesLeft;
	}
	/**
	 * Decreases the amount of tiles left when a piece is added.
	 */
	public void decreaseNumTilesLeft(){
		this.numTilesLeft = numTilesLeft-6;
		//Update the number of pieces left
		numPieceLeft();
	}
	
	/**
	 * If the player returns a piece to bullpen numTilesLeft must be increased by six
	 */
	public void increaseNumTilesLeft(){
		this.numTilesLeft = numTilesLeft+6;
		//Update the number of pieces left
		numPieceLeft();
	}
	/**Decrease the number of moves left by one */
	public void decreaseNumMovesLeft(){
		this.numMovesLeft = numMovesLeft-1;
		//Updates the number of pieces left
		numPieceLeft();
	}
	
//	/**Increase the number of moves left by one */
//	public void increaseNumMovesLeft(){
//		this.numMovesLeft = numMovesLeft+1;
//	}

	/**Ensure that this won't return 0 when there's some pieces left
	 * Would this be handled here or Board?
	 */
	public void numPieceLeft() {
		this.numPieceLeft = numTilesLeft/6;
	}
	
	/**
	 * Sets the maximum number of moves
	 * @param moves
	 * @author Kenedi
	 */
	public void setNumMaxMoves(int moves){
		this.maxMoves = moves;
		this.numMovesLeft = moves;
	}

	@Override
	public boolean hasWon() {
		if(numTilesLeft == 0){
			return true;
		}
		return false;

	}

	@Override
	public String getVariationName() {
		return NAME;
	}


//	@Override
//	public void setName(String name) {
//		this.lvlName = name;
//	}
//
//	@Override
//	public String getName() {
//		return this.lvlName;
//	}

	@Override
	/**
	 * Calculates the achievements for the current game.
	 */
	public boolean hasAchievement() {
		if(numPieceLeft == 2){
			currentAchievement = 1;
			return true;
		}
		else if (numPieceLeft == 1){
			currentAchievement = 2;
			return true;
		}
		else if (hasWon()){
			currentAchievement = 3;
			return true;
		}
		/**Reason it's set to 0 is just
		 * in case the user moves a piece
		 * that had given them an achievement
		 * and lose it. */
		currentAchievement = 0;
		return false;
	}
	
	
	/**Returns the max number of moves for this level */
	public int getMaxMoves(){
		return maxMoves;
	}
	
	/**Returns the number of moves left */
	public int getNumMovesLeft(){
		return numMovesLeft;
	}
	
	/**Returns the number of tiles left */
	public int getNumTilesLeft(){
		return numTilesLeft;
	}
	
	/**Returns number of pieces left */
	public int getNumPieceLeft(){
		return numPieceLeft;
	}
	
	/**
	 * Returns the order of this level
	 */
	public int getOrder(){
		return this.order;
	}
	
	@Override
	/**
	 * toString method to debug puzzle
	 */
	public String toString() {
		String intro = "Puzzle Level unlocked?: " + unlocked + "\n" ;
		String level = ("The Level " + getVariationName() + "\n");
		String numMoves = ("has: " + numMovesLeft + " number of moves left. \n");
		String numPieces = ("has: " + numPieceLeft + " pieces left. \n");
		String won = ("has: " + achievement + " stars, thus has the player won?: " + hasWon());
		return intro + level + numMoves + numPieces + won;

	}
	
	/** Generates a memento so that this level can be saved	 
	 * @return LevelMemento representing the important information for the level.
	 * Returns a PuzzleMemento
	 * @author Jordan Burklund
	 */
	@Override
	public LevelMemento generateMemento() {
		return new PuzzleMemento(bullpen.pieces, board.pieces, board.tiles, board.hints,
									board.width, board.height, this.unlocked, this.order, this.achievement,
									this.maxMoves, this.numTilesLeft);
	}

	/** Restores the level to the state described by the given PuzzleMemento
	 * @param memento PuzzleMemento to restore the level to
	 * @return false if the level could not be restored from the memento.
	 * Could be caused by not passing in a PuzzleMemento, or corrupt data
	 * in the memento
	 * @author Jordan Burklund
	 */
	@Override
	public boolean restoreFromMemento(LevelMemento memento) {
		if(memento == null) {
			return false;
		}
		if(!(memento instanceof PuzzleMemento)) {
			return false;
		}
		PuzzleMemento restore = (PuzzleMemento) memento;
		this.bullpen = new Bullpen(restore.bullpenPieces);
		this.board = new Board(restore.boardTiles);
		this.board.pieces = restore.boardPieces;
		this.board.hints.addAll(restore.boardHints);
		this.board.setWidth(restore.boardWidth);
		this.board.setHeight(restore.boardHeight);
		this.unlocked = restore.unlocked;
		this.order = restore.order;
		this.achievement = restore.achievement;
		this.maxMoves = restore.maxMoves;
		this.numMovesLeft = restore.maxMoves;
		this.numTilesLeft = restore.numTilesLeft;
		return true;
	}

	@Override
	/**
	 * Restores the Puzzle Level
	 */
	public void recalculate()
	{
		this.numTilesLeft = board.getSize();
		this.numMovesLeft = this.maxMoves;
	}
	
}