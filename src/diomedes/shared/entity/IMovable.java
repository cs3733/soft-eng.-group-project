package diomedes.shared.entity;

/**
 * Models that implement this interface will have the capacity to handle Moves.
 * @author Kyle Carrero
 *
 */
public interface IMovable {

}
