package diomedes.shared.entity;

import java.security.InvalidParameterException;
import java.util.ArrayList;

/**
 * Bullpen state entities, records the current state of the bullpen.
 * @author Kyle Carrero
 * @author Myles Spencer
 */

public class Bullpen {

	ArrayList<Piece> pieces;
	Piece selectedPiece;


	/**
	 * @param Piece pieces[*]
	 */
	
	public Bullpen(ArrayList<Piece> pieces) {
		if(pieces == null) {
			throw new InvalidParameterException("Null list of pieces");
		}
		this.pieces = pieces;
		this.selectedPiece = null;
	}

	/**
	 * 
	 * @return The pieces found in the bullpen, null if no pieces
	 */
	
	public ArrayList<Piece> getPieces() {
		if(pieces == null) {
			throw new RuntimeException("Cannot get null pieces.");
		}
		return pieces;
	}
	
	/**
	 * Removes the given piece from the bullpen
	 * @param piece
	 * @return
	 */
	public boolean removePiece(Piece piece) {
		return pieces.remove(piece);
	}
	
	public void addPiece(Piece p) {
		if(p == null) {
			throw new InvalidParameterException("Cannot add null piece to bullpen");
		}
		pieces.add(p);
	}
	
	/**
	 * Gets the selected piece in the bullpen
	 * @return The selected piece in the bullpen or null if nothing is selected
	 */
	public Piece getSelectedPiece()
	{
		if(selectedPiece != null)
		{
			return selectedPiece;
		}
		
		return null;
	}
	
	/**
	 * Sets the selected piece in the bullpen
	 * @param selected The piece to be the selected one
	 */
	public void setSelectedPiece(Piece selected)
	{
		if(selectedPiece != null) {
			selectedPiece.deselectPiece();
		}
		selectedPiece = selected;
		selectedPiece.selectPiece();
	}
	
	/**
	 * Clear selected piece in the bullpen
	 */
	public void clearSelectedPiece()
	{
		if(selectedPiece != null) {
			selectedPiece.deselectPiece();
		}
		selectedPiece = null;
	}

}