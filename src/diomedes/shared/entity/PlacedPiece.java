package diomedes.shared.entity;

import java.io.Serializable;

/**
 * Class for storing information on where a piece is placed on a board,
 * or other item.
 * @author Jordan Burklund
 */
public class PlacedPiece implements Serializable {

	Position boardPosition;
	Piece piece;



	/**
	 * Creates a new piece at the specified location.
	 * @param Position boardPosition Position relative to the <0,0> coordinate of the board
	 * in units of column/row
	 * @param Piece piece Piece to be placed on the board
	 */
	public PlacedPiece(Position boardPosition, Piece piece) {
		if(boardPosition == null || piece == null) {
			throw new NullPointerException("Parameters cannot be null");
		}
		//Copy the values of the position so that they can only be modified internally (for security)
		this.boardPosition = boardPosition.copy();
		//Just use the reference for the piece
		this.piece = piece;
	}
	
	/**
	 * Gets the position of the piece
	 * @return position of the piece
	 */
	public Position getPosition() {
		return boardPosition;
	}
	
	/**
	 * Gets the stored piece
	 * @return piece
	 */
	public Piece getPiece() {
		return piece;
	}
	
	@Override
	/**
	 * Two placed pieces are the same if they are in the same position, and the same piece
	 */
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		if(o instanceof PlacedPiece) {
			PlacedPiece testPiece = (PlacedPiece) o;
			return piece.equals(testPiece.piece) &&
					boardPosition.equals(testPiece.boardPosition);
		} else {
			return false;
		}
	}
	
	@Override
	/**
	 * Hash code for piece takes it's ID and the board position
	 */
	public int hashCode() {
		return piece.id*10000 + boardPosition.hashCode();
	}
	
	public PlacedPiece copy()
	{
		return new PlacedPiece(boardPosition, piece);
	}
}