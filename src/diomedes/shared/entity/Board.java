package diomedes.shared.entity;

import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * State of the board.
 * @author Myles Spencer
 */
public class Board {

	HashSet<PlacedPiece> pieces = new HashSet<PlacedPiece>();
	HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
	HashSet<PlacedPiece> hints = new HashSet<PlacedPiece>();
	PlacedPiece selectedPiece;
	
	int height;
	int width;

	
	/**
	 * Default Constructor for a Board.
	 * Constructs an empty 12x12 board of NumberedTiles (which can act as normal tiles).
	 * @author Kyle Carrero
	 */
	public Board() {
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>();
		Position p;
		for(int i = 0; i < 12; i++) {
			for(int j = 0; j < 12; j++) {
				p = new Position(i, j);
				tiles.put(p, new NumberedTile(p, null, null));
			}
		}
		
		this.tiles = tiles;
		this.height = 12;
		this.width = 12;
		this.selectedPiece = null;
	}
	
	/**
	 * @param HashSet<PlacedPiece> hints
	 */
	public Board(HashMap<Position, Tile> tiles) {
		if(tiles == null)
		{
			throw new InvalidParameterException("Null list of tiles");
		}
		this.tiles = tiles;
	}

	/**
	 * Returns a boolean whether or not the specified tile is covered
	 * @param Position position
	 * @return Boolean whether or not it is covered or null if the position does not exist
	 */
	public Boolean isTileCovered(Position position) {
		Tile tile = tiles.get(position);	
		if(tile == null)
		{
			return null;
		}
		return tile.isCovered;
	}

	/**
	 * @param PlacedPiece piece
	 */
	public void addPiece(PlacedPiece piece) {
		pieces.add(piece);
		for(Position pos: piece.getPiece().getSquares())
		{
			tiles.get(pos.derelatize(piece.getPosition())).setIsCovered(true);
		}
	}

	/**
	 * @param PlacedPiece piece
	 */
	public boolean isValid(PlacedPiece piece) {
		for(Position pos: piece.getPiece().getSquares())
		{
			Position derel = pos.derelatize(piece.getPosition());
			if((isTileCovered(derel) == null || isTileCovered(derel)) || !isTileActive(derel))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the Tile in the given Position is active or not.
	 * @param derel The Derelatized position of the piece position
	 * @return True if the tile is Active, false otherwise.
	 */
	private boolean isTileActive(Position pos) {
		Tile tile = tiles.get(pos);	
		if(tile == null)
		{
			return false;
		}
		return tile.isActive();
	}
	
	/**
	 * Getter for the tile at a certain position
	 * @param pos position in which the tile is at
	 * @return Tile found in that position
	 */
	public Tile getTileAtPosition(Position pos) {
		Tile tile = tiles.get(pos);
		if(tile == null) {
			return null;
		}
		return tile;
	}
	
	/**
	 * Removes the given piece from the board
	 * @param piece Piece to remove  Note: references must match
	 * a piece already in the board.
	 * @return true if the piece was removed.
	 */
	public boolean removePiece(PlacedPiece piece) {
		boolean isRemoved = pieces.remove(piece);
		
		if(isRemoved)
		{
			for(Position pos: piece.piece.getSquares())
			{
				tiles.get(pos.derelatize(piece.getPosition())).setIsCovered(false);
			}
		}
		
		return isRemoved;
	}

	/**
	 * Getter for getTiles
	 * @return Collection<Tile> all tile values
	 */
	public Collection<Tile> getTiles()
	{
		return tiles.values();
	}
	
	/**
	 * Getter for pieces
	 * @return hashSet<PlacedPiece> the pieces
	 */
	public HashSet<PlacedPiece> getPieces()
	{
		return pieces;
	}
	
	/**
	 * Getter for the hints
	 * @return HashSet<PlacedPiece> all the hints
	 */
	public HashSet<PlacedPiece> getHints()
	{
		return hints;
	}
	
	
	/**
	 * Sets the height of the board
	 * @param height 
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	
	/**
	 * Sets the width of the board
	 * @param width
	 */
	public void setWidth(int width) {
		this.width = width;
	}
	/**
	 * Returns the heeight of the board
	 * @return height
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Returns the width of the board.
	 * @return width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Adds to the column
	 * @return if it was sucessful
	 */
	public boolean addColumn() {
		if(width < 12) {
			Position p;
			Tile t;
			for(int i = 0; i < height; i++) {
				t = tiles.get(new Position(width, i));
				if(!t.isCovered) {
					t.activateTile();
				}
			}
			width++;
			return true;
		}
		return false;
	}

	/**
	 * Removes from the column
	 * @return if it was sucessful
	 */
	public boolean removeColumn() {
		if(width > 5) {
			width--;
			Tile t;
			for(int i = 0; i < height; i++) {
				t = tiles.get(new Position(width, i));
				if(!t.isCovered) {
					t.deactivateTile();
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Adds to the Row
	 * @return if it was sucessful
	 */
	public boolean addRow() {
		if(height < 12) {
			Position p;
			Tile t;
			for(int i = 0; i < width; i++) {
				t = tiles.get(new Position(i, height));
				if(!t.isCovered) {
					t.activateTile();
				}
			}
			height++;
			return true;
		}
		return false;
	}

	/**
	 * Remove to the Row
	 * @return if it was sucessful
	 */
	public boolean removeRow() {
		if(height > 5) {
			height--;
			Tile t;
			for(int i = 0; i < width; i++) {
				t = tiles.get(new Position(i, height));
				if(!t.isCovered) {
					t.deactivateTile();
				}
			}
			return true;
		}
		return false;
	}
	
	@Override
	/**
	 * To string to describe board
	 */
	public String toString() {
		String str = "Board(" + width + ", " + height + ")";
		
		return str;
	}
	
	/**
	 * Adds a hint to the board.
	 * @param piece The piece where a hint should be placed at.
	 */
	public void addHint(PlacedPiece piece)
	{
		hints.add(piece);
	}
	
	/**
	 * Removes a hint from the board.
	 * @param piece The hint to remove from the board.
	 */
	public void removeHint(PlacedPiece piece) {
		hints.remove(piece);
	}
	
	/**
	 * Gets the selected piece in the Board
	 * @return The selected piece in the Board or null if nothing is selected
	 */
	public PlacedPiece getSelectedPiece()
	{
		if(selectedPiece != null)
		{
			return selectedPiece;
		}
		
		return null;
	}
	
	/**
	 * Sets the selected piece in the Board
	 * @param selected The piece to be the selected one
	 */
	public void selectPiece(PlacedPiece selected)
	{
		selectedPiece = selected;
		selectedPiece.getPiece().selectPiece();
	}
	/**
	 * Sets the selected piece based on the position of the piece on the board
	 * @param position The position of the piece to be selected
	 */
	public boolean selectPiece(Position position)
	{
		if(isTileCovered(position))
		{
			for(PlacedPiece placed: pieces)
			{
				for(Position pos: placed.getPiece().getSquares())
				{
					if(pos.derelatize(placed.getPosition()).equals(position))
					{
						selectedPiece = placed;
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Clear selected piece in the Board
	 */
	public void clearSelectedPiece()
	{
		if(selectedPiece != null) {
			selectedPiece.getPiece().deselectPiece();
		}
		selectedPiece = null;
	}
	
	/**
	 * Gets the number of active tiles on the board
	 * @return The number of active tiles
	 */
	public int getSize()
	{
		int num = 0;
		for(Tile tile: getTiles())
		{
			if(tile.isActive)
			{
				num++;
			}
		}
		return num;
	}
}