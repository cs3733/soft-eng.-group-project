package diomedes.shared.entity;

public class Resources {
	public static final String lightningDir = "levels/lightning/";
	public static final String puzzleDir = "levels/puzzle/";
	public static final String releaseDir = "levels/release/";
}
