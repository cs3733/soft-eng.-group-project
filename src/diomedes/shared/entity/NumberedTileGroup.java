package diomedes.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Contains the group of numbered tiles.
 * 
 * @author Myles Spencer
 * @author Aura Velarde
 * @author Kenedi Heather
 */
public class NumberedTileGroup implements Iterable<NumberedTile>, Serializable {

	ArrayList<NumberedTile> numberedTiles = new ArrayList<NumberedTile>();
	boolean[] covered;

	/**
	 * Constructor for the numbered tile group.
	 * @param NumberedTile tiles
	 */
	public NumberedTileGroup(ArrayList<NumberedTile> tiles) {
		if(tiles == null) {
			throw new IllegalArgumentException("Cannot have a NumberedTileGroup with null list of NumberedTiles");
		}
		
		covered = new boolean[6];
		this.numberedTiles = tiles;
	}

	/**
	 * Constructor that initializes the class with no numbered tiles
	 */
	public NumberedTileGroup() {
		this.numberedTiles = new ArrayList<NumberedTile>();
		covered = new boolean[6];
	}
	
	/**
	 * Add a tile to the group. Make sure if it's already in to update it.
	 * However; also need to make sure
	 * @param nt the created numbered tile.
	 */
	public void addNumberedTile(Position p, ReleaseData rd){
		
		boolean added = false;
		NumberedTile nt = new NumberedTile(p, this, rd);

		for(NumberedTile n: numberedTiles){
//						if(n.equals(nt)){ //Updates the current piece since it's the same!
//							numberedTiles.remove(nt);
//							numberedTiles.add(nt);
//							added = true;
////							break;
//						}
//			else 
			if(n.boardPosition.equals(nt.boardPosition)){ //Replaces the current tile with the new one
				numberedTiles.remove(n);
				numberedTiles.add(nt);
				added = true;
//				break;
			}
		}
		if(!added){ //New addition!
			numberedTiles.add(nt);
//			System.out.println("hi");
		}

	}

	/**
	 * Add a tile to the group. Make sure if it's already in to update it.
	 * However; also need to make sure
	 * @param nt the created numbered tile.
	 */
	public void addNumberedTile(NumberedTile nt){
		boolean added = false;

		for(NumberedTile n: numberedTiles){
			if(n.equals(nt)){ //Updates the current piece since it's the same!
				numberedTiles.remove(nt);
				numberedTiles.add(nt);
				added = true;
				//break;
			}
			else if(n.boardPosition.equals(nt.boardPosition)){ //Replaces the current tile with the new one
				numberedTiles.remove(n);
				numberedTiles.add(nt);
				added = true;
				//break;
			}
		}
		if(!added){ //New addition!
			numberedTiles.add(nt);
		}

	}
	
	/**
	 * Removes a numberedTile from the arrayList at a given position.
	 * @param p position of the NumberedTile
	 */
	public void removeNumberedTile(Position p){
		for(NumberedTile n: numberedTiles){
			if(n.boardPosition.equals(p)){
				numberedTiles.remove(n);
				break;
			}
		}
	}

	/**
	 * Does this NumberedTileGroup contain this numbered tile?
	 * @param p Position of the tile
	 * @param num the Number of the tile
	 * @return true or false
	 */

	public boolean contains(Position p, ReleaseData rd){
		for(NumberedTile n: numberedTiles){
			if(n.boardPosition.equals(p) && n.getReleaseData().equals(rd)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Removes all elements in the numberedTiles arrayList.
	 */
	public void removeAll(){
		numberedTiles.removeAll(numberedTiles);
	}

	/**
	 * 
	 * @return the size of the numberedTile arrayList
	 */
	public int getSize(){
		return numberedTiles.size();
	}

	/**
	 * Returns an iterator for numbered tiles */
	public Iterator<NumberedTile> iterator() {
		return numberedTiles.iterator();
	}

	/**
	 * 
	 * @param p the position of a NumberedTile
	 */
	public void setIsCovered(Position p){
		for(NumberedTile n: numberedTiles){
			if(n.boardPosition.equals(p)){
				n.setIsCovered(true);
				break;
			}
		}
	}
	/**
	 * Makes a new object with the same tiles
	 * @return NumberedTileGroup with same characteristics
	 */

	public NumberedTileGroup makeCopy(){
		ArrayList<NumberedTile> copyTiles = new ArrayList<NumberedTile>();
		NumberedTileGroup copy = new NumberedTileGroup(copyTiles);
		for(NumberedTile n: numberedTiles){
			copy.addNumberedTile(n.getPosition(), n.getReleaseData());
		}

		return copy;

	}

	/**
	 * Checks if the passed in object is equal to this NumberedTileGroup
	 * That means that they contain the same tiles, in the same position with the same numbers. 
	 */
	@Override
	public boolean equals(Object obj){
		boolean result = true; //Let's start assuming it's true
		if(obj instanceof NumberedTileGroup){ //if it's the same type we are out for a good start
			for(NumberedTile nt: (NumberedTileGroup)obj){
				if(this.contains(nt.boardPosition, nt.getReleaseData())){
					result = true; //Maintain true
				}
				else{
					result = false; //If it does not contain the tile, then it's false
					return result;
				}


			}
			return result;  //Return the result
		}
		return false; //If not the correct object type
	}
	/**
	 * Checks if all of the numbers have been accounted for.
	 * @return
	 */
	public boolean isCovered(){

		for(NumberedTile nt: numberedTiles){
//			System.out.println("I exist");
			if(nt.getNumber() == 1 && nt.isCovered){
				this.covered[0] = true;
			} 
			
			if(nt.getNumber() == 2 && nt.isCovered){
				this.covered[1] = true;
			} 

			if(nt.getNumber() == 3 && nt.isCovered){
				this.covered[2] = true;
			} 

			if(nt.getNumber() == 4 && nt.isCovered){
				this.covered[3] = true;
			} 

			if(nt.getNumber() == 5 && nt.isCovered){
				this.covered[4] = true;
			} 

			if(nt.getNumber() == 6 && nt.isCovered){
				this.covered[5] = true;
			} 

		}
		if(isTrue()){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Returns true if the whole array has "true" values.
	 * @param coveredOne2
	 * @return
	 */
	public boolean isTrue(){
		for(int i = 0; i < 6; i++){
			if(!this.covered[i]){
				return false;
			}
		}
		return true;
	}
	/**
	 * Get which numbers have been accounted for in the game.
	 * @return array of booleans where each index is the number covered.
	 */
	public boolean[] getCovered(){
		return this.covered;
	}

	public ArrayList<NumberedTile> getNumberedTiles() {
		return this.numberedTiles;
	}

}