package diomedes.shared.entity;

/**
 * Interface that allows a level to be saved and restored from disk
 * @author Jordan Burklund
 *
 */
public interface SaveableLevel {
	/** Generates a memento so that the level can be saved to disk
	 * This method will always return a LevelMemento, but subclasses can
	 * return any class that is a subclass of LevelMemento
	 */
	public LevelMemento generateMemento();
	/** Restores the level to the state described by the given LevelMemento
	 * @param memento LightningMemento to restore the level to
	 * @return false if the level could not be restored from the memento.
	 * Could be caused by not passing in the correct LevelMemento sublcass,
	 * corrupt data in the memento, or other reasons specified in the subclass
	 * implementation
	 */
	public boolean restoreFromMemento(LevelMemento memento);
}
