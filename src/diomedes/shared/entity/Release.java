package diomedes.shared.entity;

import java.util.ArrayList;

/**
 * Release entities, handles the current state for the level.
 * @author Myles Spencer
 * @author Aura Velarde
 */
public class Release extends Level {

	public static final String NAME = "Release";

	//	String lvlName;
	ArrayList<NumberedTileGroup> tileGroups;
	int numTileGroupsCovered;

	private int boardWidth;

	private int boardHeight;

	/**
	 * 
	 * @param board The Board for the level
	 * @param bullpen The Bullpen for the level
	 * @param order The order of the level
	 * @param tileGroups The NumberedTileGroups that will keep track of the releaseData on tiles.
	 * By convention the first NumberedTileGroup in the list is RED, the second is GREEN and the third is BLUE. Remember the order RGB.
	 */
	public Release(Board board, Bullpen bullpen,int order, ArrayList<NumberedTileGroup> tileGroups) {
		super(board, bullpen, order);
		if(tileGroups == null || tileGroups.size() != 3) {
			throw new IllegalArgumentException("There must be exactly 3, non-null, NumberedTileGroups for a Release Level");
		}
		this.tileGroups = tileGroups;
	}

	/**
	 * Helper method to find out which ones out of the tile groups have been covered.
	 */
	public void numTileGroupsCovered() {
		/**Here go through iterable and check
		 *  boolean is covered */
		this.numTileGroupsCovered = 0;
		for(NumberedTileGroup ntg: this.tileGroups){

			if(ntg.isCovered()){
				numTileGroupsCovered++;
			} 
		}
	}
	/**
	 * Calculates the number of tile groups covered and returns it
	 * @return number of tile groups covered
	 */

	public int getNumTilesGroupCovered(){
		numTileGroupsCovered();
		return numTileGroupsCovered;
	}


	/**TODO Don't think it's needed. ASK */
	//public void setNumTilesGroup(ArrayList<NumberedTileGroup> tiles){
	//	this.tileGroups = tiles;
	//
	//}
	//
	//public ArrayList<NumberedTileGroup> getNumTilesGroup(){
	//	return this.tileGroups;
	//}

	/**You win release once you have covered three of the groups covered */
	public boolean hasWon() {
		if(numTileGroupsCovered == 3){
			return true;
		}
		return false;
	}

	/**Returns the name of the variation */
	public String getVariationName() {
		return NAME;
	}

	/**TODO don't think it's needed */
	/**Set the name of the level */
	//public void setName(String name) {
	//	this.lvlName = name;
	//}
	//
	//@Override
	//public String getName() {
	//	return this.lvlName;
	//}

	@Override
	public String toString() {

		String intro = "Release Level unlocked?: " + unlocked + "\n" ;
		String level = ("The Level " + getVariationName() + "\n");
		String numTiles = ("has: " + numTileGroupsCovered + " number of groups covered. \n");
		String won = ("has: " + achievement + " stars, thus has the player won?: " + hasWon());
		return intro + level + numTiles + won;

	}

	/**For release we need to determine how many groups have been covered to determine the
	 * achievement */
	public boolean hasAchievement() {
		this.numTileGroupsCovered();
		if(numTileGroupsCovered == 1){
			currentAchievement = 1;
			return true;
		}
		else if(numTileGroupsCovered == 2){
			currentAchievement = 2;
			return true;
		}
		else if(hasWon()){
			currentAchievement = 3;
			return true;
		}

		return false;
	}

	/** Generates a memento so that this level can be saved	 
	 * @return LevelMemento representing the important information for the level.
	 * Returns a ReleaseMemento
	 * @author Jordan Burklund
	 */
	@Override
	public LevelMemento generateMemento() {
		return new ReleaseMemento(bullpen.pieces, board.pieces, board.tiles, board.hints,
				board.width, board.height, this.unlocked, this.order, this.achievement,
				this.tileGroups);
	}

	/** Restores the level to the state described by the given ReleaseMemento
	 * @param memento ReleaseMemento to restore the level to
	 * @return false if the level could not be restored from the memento.
	 * Could be caused by not passing in a ReleaseMemento, or corrupt data
	 * in the memento
	 * @author Jordan Burklund
	 */
	@Override
	public boolean restoreFromMemento(LevelMemento memento) {
		if(memento == null) {
			return false;
		}
		if(!(memento instanceof ReleaseMemento)) {
			return false;
		}
		ReleaseMemento restore = (ReleaseMemento) memento;
		this.bullpen = new Bullpen(restore.bullpenPieces);
		this.board = new Board(restore.boardTiles);
		this.board.pieces = restore.boardPieces;
		this.board.hints.addAll(restore.boardHints);
		this.board.setWidth(restore.boardWidth);
		this.board.setHeight(restore.boardHeight);
		this.unlocked = restore.unlocked;
		this.order = restore.order;
		this.achievement = restore.achievement;
		this.tileGroups = restore.tileGroups;
		return true;
	}

	/**
	 * Resets all the covered tiles to false.
	 */
	@Override
	public void recalculate()
	{
		for(NumberedTileGroup ntg: tileGroups){
			boolean[] c =	ntg.getCovered();
			for(int i = 0; i < c.length; i++){
				c[i] = false;
			}
		}
		
		this.numTileGroupsCovered();

	}

	public ArrayList<NumberedTileGroup> getTileGroups() {
		return tileGroups;
	}


}