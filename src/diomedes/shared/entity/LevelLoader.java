package diomedes.shared.entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;

/**
 * Abstracts loading levels from memory
 * @author Jordan Burklund
 *
 */
public class LevelLoader {
	String folderPath;
	/**
	 * Constructor that takes a location to load levels from
	 * @param folderPath
	 * @throws InvalidParameterException if the folderPath is null
	 */
	public LevelLoader(String folderPath) {
		if(folderPath == null) {
			throw new InvalidParameterException("Folder path cannot be null");
		}
		this.folderPath = folderPath;
	}
	
	/**
	 * Loads all of the level mementos in the specified folder
	 * @return List of all levelMementos loaded from the folder
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public ArrayList<Level> loadLevels() {
		String fixedDirName;
		if(folderPath.endsWith("/")) {
			fixedDirName = folderPath;
		} else {
			fixedDirName = folderPath+"/";
		}
		
		ArrayList<Level> levels = new ArrayList<Level>();
		File folder = new File(fixedDirName);
		File[] fileList = folder.listFiles(new FilenameFilter() {
							@Override
							public boolean accept(File dir, String name) {
								return name.contains(".level");
							}
						});
		
		for(File file : fileList) {
			try {
				FileInputStream fileInStream = new FileInputStream(file);
				ObjectInputStream inStream = new ObjectInputStream(fileInStream);
				LevelMemento memento = (LevelMemento) inStream.readObject();
				levels.add(LevelFactory.createFromMemento(memento));
				inStream.close();
				fileInStream.close();
			} catch (IOException | ClassNotFoundException e) {
				//Consume any exceptions while loading a level, and just don't add it to the list
			}
		}
		return levels;
	}

}
