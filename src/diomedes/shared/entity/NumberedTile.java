package diomedes.shared.entity;

/**
 * Numbered Tile for Release.
 * @author Kenedi Heather
 * @author Aura Velarde
 */

public class NumberedTile extends Tile{
	
	/**
	 * The number/color information for release data
	 */
	ReleaseData rd;
	NumberedTileGroup group;


	public NumberedTile(Position boardPosition, NumberedTileGroup group, ReleaseData rd) {
		super(boardPosition);
		
		this.rd = rd;
		this.group = group;
	}
	
	/**
	 * Gets the tile numebr.
	 * @return the number on the tile
	 */
	public int getNumber(){
		return this.rd.getNumber();
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof NumberedTile){
			if(this.boardPosition.equals(((NumberedTile)obj).getPosition())){
				if(this.rd.equals(((NumberedTile) obj).getReleaseData())){
					return true;
				}
			}
		}
		return false;
		
	}
	
	/**
	 * Gets the ReleaseData for this tile. If this tile has no number/color information to display, this will return null.
	 * @return The ReleaseData for the tile, null if no ReleaseData is Set
	 */
	public ReleaseData getReleaseData() {
		return this.rd;
	}
	
	/**
	 * Sets the ReleaseData for this tile to the given value.
	 * @param data The ReleaseData for the 
	 */
	public void setReleaseData(ReleaseData data) {
		this.rd = data;
//		System.out.println("Added ReleaseData:\n Color: " + data.getColor().toString() + "\n Number: " + data.getNumber());
	}
	
	/**
	 * Clears the ReleaseData for this Tile.
	 */
	public void clearReleaseData() {
//		if(this.releaseData != null) {
//			System.out.println("Clearing ReleaseData:\n Color: " + this.releaseData.getColor().toString() + "\n Number: " + this.releaseData.getNumber());
//		}
		this.rd = null;
	}

	public void addTileGroup(NumberedTileGroup ntg) {
		this.group = ntg;
		if(!ntg.contains(this.boardPosition, this.rd)) {
			ntg.addNumberedTile(this);
		}
	}

	/**
	 * Removes this tile from it's group and sets the group for this tile to null.
	 */
	public void clearTileGroup() {
		if(this.rd != null && this.boardPosition != null && this.group.contains(this.boardPosition, this.rd)) {
			this.group.removeNumberedTile(this.boardPosition);
		}
		this.group = null;
	}
	
	public NumberedTile copy() {
		return new NumberedTile(this.boardPosition, this.group, this.rd);
	}

	/**
	 * Gets the NumberedTileGroup associated with this tile
	 * @return
	 */
	public NumberedTileGroup getTileGroup() {
		return this.group;
	}
}
