package diomedes.shared.entity;

import java.security.InvalidParameterException;
import java.util.HashMap;

/**
 * Factory class for generating generic rectangular boards
 * @author Jordan Burklund
 *
 */
public class RectangleBoardFactory {
	/**
	 * Generates a new Board with the given number of rows and columns
	 * @param rows Number of rows for this board.
	 * @param cols Number of columns for this board.
	 * @return Generated board
	 */
	static public Board makeRectangleBoard(int rows, int cols) {
		if(rows <= 0) {
			throw new InvalidParameterException("Rows cannot be less than 1");
		}
		if(cols <= 0) {
			throw new InvalidParameterException("Columns cannot be less than 1");
		}
		
		HashMap<Position, Tile> tiles = new HashMap<Position, Tile>(rows*cols);
		for(int x=0; x<cols; x++) {
			for(int y=0; y<rows; y++) {
				Position pos = new Position(x,y);
				tiles.put(pos, new Tile(pos));
			}
		}
		return new Board(tiles);
	}
}
