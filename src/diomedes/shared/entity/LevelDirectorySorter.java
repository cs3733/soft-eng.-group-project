package diomedes.shared.entity;

import java.security.InvalidParameterException;

/**
 * Class that gives the directory in memory of where a level should be
 * @author Jordan Burklund
 */
public class LevelDirectorySorter {
	/**
	 * Gets the directory of where the level should be based on the type of the level
	 * @param level The Level to find the directory location for
	 * @return The path to the directory of where the level should be located.
	 * @throws InvalidParameterException if the level type cannot be handled
	 */
	public static String getDirectoryForLevel(Level level) {
		//Set the appropriate directory based on the level type
		if(level instanceof Lightning) {
			return Resources.lightningDir;
		} else if(level instanceof Puzzle) {
			return Resources.puzzleDir;
		} else if(level instanceof Release) {
			return Resources.releaseDir;
		}
		throw new InvalidParameterException("Level type could not be handled");
	}
}
