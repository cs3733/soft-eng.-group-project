package diomedes.shared.entity;

import java.awt.Color;
import java.io.Serializable;
/**
 * Release Data entitie, keeps track of the color and number.
 */
public class ReleaseData implements Serializable {

	int number;
	Color color;
	/**
	 * Constructor for ReleaseData
	 * @param number 
	 * @param color
	 */
	public ReleaseData(int number, Color color) {
		if(color == null) {
			throw new IllegalArgumentException("Color cannot be null");
		}
		if(!color.equals(Color.RED) && !color.equals(Color.GREEN) && !color.equals(Color.BLUE)) {
			throw new IllegalArgumentException("Color must be either Red, Green, or Blue");
		}
		if(number <= 0 || number > 6) {
			throw new IllegalArgumentException("Number must be between 1 and 6");
		}

		this.number = number;
		this.color = color;
	}

	/**
	 * Getter for the current color
	 * @return Color current color
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * Getter for the number
	 * @return int current number
	 */
	public int getNumber() {
		return this.number;
	}

	@Override
	/**
	 * Compares to another release data.
	 */
	public boolean equals(Object obj){
		if(obj instanceof ReleaseData){
			if(this.color.equals(((ReleaseData)obj).getColor())){
				if(this.number == ((ReleaseData) obj).getNumber()){
					return true;
				}
			}
		}
		return false;

	}

	/**
	 * Checks if the color is equal to Color.RED
	 * @return true if color is red, false otherwise
	 */
	public boolean isRed() {
		if(this.color.getRed() == 255 && this.color.getGreen() == 0 && this.color.getBlue() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the color is equal to Color.GREEN
	 * @return true if color is green, false otherwise
	 */
	public boolean isGreen() {
		if(this.color.getRed() == 0 && this.color.getGreen() == 255 && this.color.getBlue() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the color is equal to Color.BLUE
	 * @return true if color is blue, false otherwise
	 */
	public boolean isBlue() {
		if(this.color.getRed() == 0 && this.color.getGreen() == 0 && this.color.getBlue() == 255) {
			return true;
		}
		return false;
	}

}
