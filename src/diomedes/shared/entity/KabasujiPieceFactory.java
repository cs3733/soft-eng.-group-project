package diomedes.shared.entity;

import java.security.InvalidParameterException;

/**
 * Factory to create all of the kabasuji pieces
 *
 */
public class KabasujiPieceFactory {
	
	public static Piece makeNumberedPiece(int pieceNumber) {
		if(pieceNumber < 1 || pieceNumber > 35) {
			throw new InvalidParameterException("Piece number "+pieceNumber+"could not be created");
		}
		
		return new Piece(shapes[pieceNumber-1], pieceNumber);
	}
	
	/** Constants for creating numbered Kabasuji pieces
	 */
	static final Position[] squares1 = {	new Position(0,0),
											new Position(0,-1),
											new Position(0,-2),
											new Position(0,1),
											new Position(0,2),
											new Position(0,3) };
	static final Position[] squares2 = {	new Position(0,0),
											new Position(0,1),
											new Position(0,2),
											new Position(1,2),
											new Position(0,-1),
											new Position(0,-2) };
	static final Position[] squares3 = {	new Position(0,0),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(0,-2),
											new Position(0,1),
											new Position(0,2) };
	static final Position[] squares4 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,1),
											new Position(0,2),
											new Position(0,-1),
											new Position(0,-2) };
	static final Position[] squares5 = {	new Position(0,0),
											new Position(0,1),
											new Position(1,1),
											new Position(-1,0),
											new Position(-2,0),
											new Position(-2,-1) };
	static final Position[] squares6 = {	new Position(0,0),
											new Position(-1,0),
											new Position(-2,0),
											new Position(0,-1),
											new Position(1,-1),
											new Position(2,-1) };
	static final Position[] squares7 = {	new Position(0,0),
											new Position(0,1),
											new Position(1,2),
											new Position(1,1),
											new Position(0,-1),
											new Position(0,-2) };
	static final Position[] squares8 = {	new Position(0,0),
											new Position(-1,0),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(0,1),
											new Position(0,2) };
	static final Position[] squares9 = {	new Position(0,0),
											new Position(0,1),
											new Position(-1,1),
											new Position(0,-1),
											new Position(0,-2),
											new Position(1,-2) };
	static final Position[] squares10 = {	new Position(0,0),
											new Position(0,1),
											new Position(-1,1),
											new Position(1,1),
											new Position(0,-1), 
											new Position(0,-2) };
	static final Position[] squares11 = {	new Position(0,0),
											new Position(-1,0),
											new Position(-2,0),
											new Position(0,1),
											new Position(0,2),
											new Position(0,3) };
	static final Position[] squares12 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,1),
											new Position(1,1),
											new Position(0,-1),
											new Position(1,-1) };
	static final Position[] squares13 = {	new Position(0,0),
											new Position(0,1),
											new Position(1,1),
											new Position(-1,0),
											new Position(-1,-1),
											new Position(-1,-2) };
	static final Position[] squares14 = {	new Position(0,0),
											new Position(1,0),
											new Position(1,1),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(-1,-2) };
	static final Position[] squares15 = {	new Position(0,0),
											new Position(0,-1),
											new Position(1,0),
											new Position(1,1),
											new Position(0,1),
											new Position(0,2) };
	static final Position[] squares16 = {	new Position(0,0),
											new Position(0,1),
											new Position(1,0),
											new Position(-1,0),
											new Position(0,-1),
											new Position(0,-2) };
	static final Position[] squares17 = { 	new Position(0,0),
											new Position(0,1),
											new Position(1,0),
											new Position(0,-1),
											new Position(0,-2),
											new Position(1,-2) };
	static final Position[] squares18 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,-1),
											new Position(-1,0),
											new Position(-1,1),
											new Position(-1,2) };
	static final Position[] squares19 = {	new Position(0,0),
											new Position(-1,0),
											new Position(-2,0),
											new Position(0,-1),
											new Position(0,-2),
											new Position(1,-2) };
	static final Position[] squares20 = {	new Position(0,0),
											new Position(-1,0),
											new Position(-2,0),
											new Position(-1,1),
											new Position(0,1),
											new Position(1,1) };
	static final Position[] squares21 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,1),
											new Position(1,1),
											new Position(2,1),
											new Position(2,2) };
	static final Position[] squares22 = {	new Position(0,0),
											new Position(0,1),
											new Position(1,0),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(-1,0) };
	static final Position[] squares23 = {	new Position(0,0),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(0,1),
											new Position(0,2),
											new Position(-1,2) };
	static final Position[] squares24 = {	new Position(0,0),
											new Position(0,1),
											new Position(1,0),
											new Position(1,-1),
											new Position(-1,0),
											new Position(-1,-1) };
	static final Position[] squares25 = {	new Position(0,0),
											new Position(0,-1),
											new Position(1,-1),
											new Position(0,1),
											new Position(1,1),
											new Position(2,1) };
	static final Position[] squares26 = {	new Position(0,0),
											new Position(1,-1),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(-1,0),
											new Position(-1,1) };
	static final Position[] squares27 = {	new Position(0,0),
											new Position(0,1),
											new Position(-1,1),
											new Position(1,0),
											new Position(0,-1),
											new Position(0,-2) };
	static final Position[] squares28 = {	new Position(0,0),
											new Position(0,-1),
											new Position(-1,0),
											new Position(-2,0),
											new Position(0,1),
											new Position(0,2) };
	static final Position[] squares29 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(-2,-1),
											new Position(-2,0) };
	static final Position[] squares30 = {	new Position(0,0),
											new Position(-1,0),
											new Position(-1,-1),
											new Position(1,1),
											new Position(1,0),
											new Position(1,-1) };
	static final Position[] squares31 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,-1),
											new Position(-1,-1),
											new Position(-1,0),
											new Position(-1,1) };
	static final Position[] squares32 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,1),
											new Position(0,2),
											new Position(-1,0),
											new Position(-1,-1) };
	static final Position[] squares33 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,-1),
											new Position(-1,0),
											new Position(-2,0),
											new Position(-2,1) };
	static final Position[] squares34 = {	new Position(0,0),
											new Position(-1,0),
											new Position(0,1),
											new Position(1,0),
											new Position(2,0),
											new Position(1,-1) };
	static final Position[] squares35 = {	new Position(0,0),
											new Position(1,0),
											new Position(0,1),
											new Position(-1,0),
											new Position(-1,-1),
											new Position(-2,-1) };
	static final Position[][] shapes = { squares1, squares2, squares3, squares4, squares5, squares6,
		squares7, squares8, squares9, squares10, squares11, squares12, squares13, squares14, squares15,
		squares16, squares17, squares18, squares19, squares20, squares21, squares22, squares23,
		squares24, squares25, squares26, squares27, squares28, squares29, squares30, squares31,
		squares32, squares33, squares34, squares35 };
}
