package diomedes.shared.entity;

import java.io.Serializable;
import java.security.InvalidParameterException;

/**
 * Entity class for piece. 
 * @author Myles Spencer
 * @author Jordan Burklund
 */
public class Piece implements Serializable {
	private Position[] squares;
	int id;
	boolean isSelected = false;


	/**
	 * Creates a duplicate list of the input square positions so that flip and rotate only affect this piece
	 * The input list must have a length of 6, and must not contain duplicates; otherwise an
	 * InvalidParameterException will be thrown
	 * @param Position squares[6]
	 */
	public Piece(Position[] squares, int id) {
		//Check that the list of squares is non-null
		if(squares == null) {
			throw new InvalidParameterException("Null list of points");
		}
		
		//Check that the list of squares has exactly 6 squares
		if(squares.length != 6) {
			throw new InvalidParameterException("Too many or too few squares");
		}
		
		this.id = id;
		
		// Check that the squares are not duplicates of one another
		for(int i=0; i<squares.length-1; i++) {
			for(int j=i+1; j<squares.length; j++) {
				if(squares[i].equals(squares[j])) {
					throw new InvalidParameterException("Duplicate points: "+squares[i].toString());
				}
			}
		}
		
		// Add squares to the internal list by creating copies
		this.setSquares(new Position[6]);
		for(int i=0; i<this.getSquares().length; i++) {
			this.getSquares()[i] = squares[i].copy();
		}
		
	}

	/**
	 * Mutate the positions by rotating left 90 degrees
	 */
	public void rotateLeft() {
		//Rotation matrix 	[0,-1] 
		//					[1, 0]
		for(Position p : this.getSquares()) {
			//Cache the row value before it is mutated
			int cachedRow = p.row;
			p.row = -p.col;
			p.col = cachedRow;
		}
	}

	/**
	 * Mutate the positions by rotating right 90 degrees
	 */
	public void rotateRight() {
		//Rotation matrix	[ 0,1]
		//					[-1,0] 
		for(Position p : this.getSquares()) {
			//Cache the row value before it is mutated
			int cachedRow = p.row;
			p.row = p.col;
			p.col = -cachedRow;
		}
	}

	/**
	 * Mutate the positions by flipping the positions along the horizontal axis
	 */
	public void flipHorizontal() {
		//Transformation matrix	[-1,0]
		//						[ 0,1]
		for(Position p : this.getSquares()) {
			p.row = -p.row;
		}
	}

	/**
	 * Mutate the positions by flipping the positions along the vertical axis
	 */
	public void flipVertical() {
		//Transformation matrix	[1, 0]
		//						[0,-1]
		for(Position p : this.getSquares()) {
			p.col = -p.col;
		}
	}

	/**
	 * Gets all the position for the squares
	 * @return
	 */
	public Position[] getSquares() {
		return squares;
	}
	
	/**
	 * 
	 * @return id of the piece
	 */
	public int getId(){
		return id;
	}

	/**
	 * Set the positions
	 * @param squares
	 */
	void setSquares(Position[] squares) {
		this.squares = squares;
	}
	
	@Override
	/**
	 * Compares to pieces
	 */
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		if(o instanceof Piece) {
			Piece p = (Piece) o;
			if(p.id == this.id) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Getter for isSelected
	 * @return true if it is selected
	 */
	public boolean isSelected() {
		return isSelected;
	}
	
	/**
	 * Lets you select a piece
	 */
	
	public void selectPiece() {
		isSelected = true;
	}
	
	/**
	 * Deselect a piece
	 */
	public void deselectPiece() {
		isSelected = false;
	}
	
	/**
	 * Makes a copy of the piece
	 * @return Piece copied piece
	 */
	public Piece copy()
	{
		return new Piece(squares, id);
	}
}