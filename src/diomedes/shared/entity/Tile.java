package diomedes.shared.entity;

import java.io.Serializable;

/**
 * Tile class that has the state of the tile.
 * @author Kenedi Heather
 * @author Aura Velarde
 */
public class Tile implements Serializable {

	/**
	 * Position of the tile on the board
	 */
	Position boardPosition;

	/**
	 * Whether or not a tile is covered by a piece
	 */
	boolean isCovered;
	
	/**
	 * If a Tile is not Active, then it will not be a valid tile to place a piece on, and will not be rendered in Kabasuji.
	 */
	boolean isActive;

	/**
	 * @param Position boardPosition
	 */
	public Tile(Position boardPosition) {
		this.boardPosition = boardPosition;
		this.isCovered = false;
		this.isActive = true;
	}
	
	public boolean getIsCovered(){
		return isCovered;
	}
	
	public Position getPosition(){
		return boardPosition; 
	}
	
	public void setIsCovered(boolean c){
		this.isCovered = c;
	}
	/**
	 * Determines if these two tiles are equal
	 * @param tile that you are comparing to the one given
	 * @return true or false
	 */
	@Override
	public boolean equals(Object tile){
		if(tile instanceof Tile){
		return (this.boardPosition.getCol() == ((Tile) tile).getPosition().getCol()) &&
				(this.boardPosition.getRow() == ((Tile) tile).getPosition().getRow());
	}
		return false;
	}
	
	@Override
	public int hashCode(){
		int code;
		
		code = (this.boardPosition.getCol() * 3) + (this.boardPosition.getRow() * 5);
		
		return code;
	}
	
	/**
	 * Activates the Tile to be used and rendered.
	 */
	public void activateTile() {
		this.isActive = true;
	}
	
	/**
	 * Deactivates the Tile, removing it from use and from being rendered.
	 */
	public void deactivateTile() {
		this.isActive = false;
	}
	
	/**
	 * Checks to see if the Tile is active or not.
	 * @return True if Tile is active, false otherwise.
	 */
	public boolean isActive() {
		return this.isActive;
	}

	public void coverTile() {
		this.isCovered = true;
	}
	
}