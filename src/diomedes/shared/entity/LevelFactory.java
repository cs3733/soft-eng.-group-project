package diomedes.shared.entity;

import java.util.ArrayList;

/** Abstracts generating new levels, and levels
 * saved in memory
 * @author JordanBurklund
 */
public class LevelFactory {
	/**
	 * Generates the appropriate Level from the given memento.  Mememento
	 * must be one of the special memento types to generate the corresponding
	 * special level type (i.e. Lightning, Puzzle, Release)
	 * @param memento
	 * @return the generated level.  Null if the level could not be generated
	 */
	public static Level createFromMemento(LevelMemento memento) {
		Level level = null;
		if(memento instanceof LightningMemento) {
			level = new Lightning(new Board(), new Bullpen(new ArrayList<Piece>()), 0);
			level.restoreFromMemento(memento);
		}
		if(memento instanceof PuzzleMemento) {
			level = new Puzzle(new Board(), new Bullpen(new ArrayList<Piece>()), 0, 0);
			level.restoreFromMemento(memento);
		}
		if(memento instanceof ReleaseMemento) {
			ArrayList<NumberedTileGroup> tileGroups = new ArrayList<NumberedTileGroup>();
			tileGroups.add(new NumberedTileGroup());
			tileGroups.add(new NumberedTileGroup());
			tileGroups.add(new NumberedTileGroup());
			level = new Release(new Board(), new Bullpen(new ArrayList<Piece>()), 0, tileGroups);
			level.restoreFromMemento(memento);
		}
		return level;
	}
}
