package diomedes.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Class used for saving relevant information about a Level.
 * @author Jordan Burklund
 */
public class LevelMemento implements Serializable {
	/** List of pieces in the bullpen **/
	ArrayList<Piece> bullpenPieces;
	/** List of pieces in the board **/
	HashSet<PlacedPiece> boardPieces;
	/** List of tiles on the board **/
	HashMap<Position, Tile> boardTiles;
	/** List of hints for the board **/
	HashSet<PlacedPiece> boardHints;
	
	/** Keeps track of the board width **/
	int boardWidth;
	
	/** Keeps track of the board height **/
	int boardHeight;
	
	/** Keeps track of if the level is unlocked **/
	boolean unlocked;
	/** Where this level is in the game order.  i.e. first, second, third level **/
	int order;
	/** The level of achievement the user has currently obtained **/
	int achievement;
	
	public LevelMemento(ArrayList<Piece> bullpenPieces, HashSet<PlacedPiece> boardPieces, HashMap<Position, Tile> boardTiles, HashSet<PlacedPiece> boardHints,
			int boardWidth, int boardHeight, boolean unlocked, int order, int achievement) {
		this.bullpenPieces = bullpenPieces;
		this.boardPieces = boardPieces;
		this.boardTiles = boardTiles;
		this.boardHints = boardHints;
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		this.unlocked = unlocked;
		this.order = order;
		this.achievement = achievement;
	}
}