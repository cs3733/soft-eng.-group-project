package diomedes.shared.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Memento for Release.
 */
public class ReleaseMemento extends LevelMemento {
	ArrayList<NumberedTileGroup> tileGroups;
	
	public ReleaseMemento(ArrayList<Piece> bullpenPieces, HashSet<PlacedPiece> boardPieces,
			HashMap<Position, Tile> boardTiles, HashSet<PlacedPiece> boardHints,
			int boardWidth, int boardHeight, boolean unlocked, int order, int achievement,
			ArrayList<NumberedTileGroup> tileGroups) {
		super(bullpenPieces, boardPieces, boardTiles, boardHints, boardWidth, boardHeight, unlocked, order, achievement);
		this.tileGroups = tileGroups;
	}
	

}
