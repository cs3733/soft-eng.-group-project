package diomedes.shared.view;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import diomedes.shared.entity.Level;

/**
 * Class that describes how level should draw to show it's achievements when
 * in a table, like in the LevelSelectView
 * @author Jordan Burklund
 *
 */
@SuppressWarnings("serial")
public class LevelAchievmentRenderer extends JLabel implements TableCellRenderer {

	public LevelAchievmentRenderer() {
		this.setOpaque(true);
	}
	
	/**
	 * Describes how to draw the Achievements in a table, like the Level Select View
	 * If the level is locked, this will display "Locked".  If the level is unlocked,
	 * but doesn't have any achievements, this will display "Unlocked".
	 * Otherwise, this will display the number of Achievements the Level has
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int col) {
		//Display the correct color if this element is selected
		if(isSelected) {
			this.setBackground(table.getSelectionBackground());
		} else {
			this.setBackground(table.getBackground());
		}
		
		//Show nothing if the object is not a level
		if(obj == null || ! (obj instanceof Level)) {
			this.setText("");
			return this;
		}
		
		Level level = (Level) obj;
		level.reset();
		//Show the correct text based on the level status
		if(!level.isUnlocked()) {
			this.setText("Locked");
		} else {
			if(!level.hadAchievement()) {
				this.setText("Unlocked");
			} else  {
				//TODO use an icon instead of text?
				this.setText("Achievement: "+level.getAchievement());
			}
		}
		
		return this;
	}

}
