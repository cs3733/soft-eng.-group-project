package diomedes.shared.view;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import diomedes.shared.entity.Level;

/**
 * Level Name rendering for the table.
 */
@SuppressWarnings("serial")
public class LevelNameRenderer extends JLabel implements TableCellRenderer {

	public LevelNameRenderer() {
		this.setOpaque(true);
	}
	
	@Override
	/**
	 * Renders the table.
	 */
	public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int col) {
		if(obj == null || obj == "") {
			this.setText("");
		}
		else {
			Level lvl = (Level) obj;
			this.setText(lvl.getVariationName() + " " + Integer.toString(lvl.getOrder()));
		}
		if(isSelected) {
			this.setBackground(table.getSelectionBackground());;
		}
		else {
			this.setBackground(table.getBackground());
		}
		return this;
	}

}
