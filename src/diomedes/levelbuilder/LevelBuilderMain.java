package diomedes.levelbuilder;

import java.util.ArrayList;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.LevelLoader;
import diomedes.shared.entity.Resources;

/**
 * Main class for LevelBuilder application.
 */
public class LevelBuilderMain
{
	/**
	 * Entry method for the LevelBuilder Application
	 * @param args System Arguments
	 */
	public static void main(String[] args) 
	{
		//Setup the splash screen!
		LevelBuilderSplashScreen splash = new LevelBuilderSplashScreen();
		
		//Lists of levels for loading into the top-level model
		ArrayList<Level> lightningLevels = new ArrayList<Level>();
		ArrayList<Level> puzzleLevels = new ArrayList<Level>();
		ArrayList<Level> releaseLevels = new ArrayList<Level>();
		
		//Load the levels
		splash.setText("Loading Lightning Levels");
		LevelLoader loader = new LevelLoader(Resources.lightningDir);
		lightningLevels = loader.loadLevels();
		
		splash.setText("Loading Puzzle Levels");
		loader = new LevelLoader(Resources.puzzleDir);
		puzzleLevels = loader.loadLevels();
		
		splash.setText("Loading Release Levels");
		loader = new LevelLoader(Resources.releaseDir);
		releaseLevels = loader.loadLevels();
		
		//Simulate Loading resources for now...
		splash.setText("Hiring Bob Ross");
		try { Thread.sleep(2000); } catch (InterruptedException e) {}
		splash.setText("Painting the Happy Trees");
		try { Thread.sleep(2000); } catch (InterruptedException e) {}
		splash.close();
		
		LevelBuilderModel model = new LevelBuilderModel();
		for(Level l:lightningLevels) { model.addLevel(l); }
		for(Level l:puzzleLevels) { model.addLevel(l); }
		for(Level l:releaseLevels) { model.addLevel(l); }
		
		Application app = new Application(model);
	}
}
