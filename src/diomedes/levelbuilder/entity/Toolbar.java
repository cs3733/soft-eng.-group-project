package diomedes.levelbuilder.entity;

import java.util.ArrayList;

import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Piece;

/**
 * @author Kyle Carrero
 * @author Myles Spencer
 */
public class Toolbar {

	ArrayList<Piece> pieces;
	Piece selectedPiece;


	/**
	 * Creates a toolbar object that contains all of the existing pieces to build a level from.
	 */
	public Toolbar() {
		this.pieces = new ArrayList<Piece>();
		this.selectedPiece = null;
		for(int i = 1; i <= 35; i++) {
			pieces.add(KabasujiPieceFactory.makeNumberedPiece(i));
		}
		this.selectedPiece = null;
	}


	public ArrayList<Piece> getPieces() {
		return pieces;
	}
	
	/**
	 * Gets the selected piece in the toolbar
	 * @return The selected piece in the toolbar or null if nothing is selected
	 */
	public Piece getSelectedPiece()
	{
		if(selectedPiece != null)
		{
			return selectedPiece;
		}
		
		return null;
	}
	
	/**
	 * Sets the selected piece in the toolbar
	 * @param selected The piece to be the selected one
	 */
	public void setSelectedPiece(Piece selected)
	{
		selectedPiece = selected;
		selected.selectPiece();
	}
	
	/**
	 * Clear selected piece in the toolbar
	 */
	public void clearSelectedPiece()
	{
		if(selectedPiece != null)
		{
			selectedPiece.deselectPiece();
			selectedPiece = null;
		}
	}
}