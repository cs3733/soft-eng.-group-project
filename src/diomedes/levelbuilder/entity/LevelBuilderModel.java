package diomedes.levelbuilder.entity;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;

import diomedes.levelbuilder.control.Move;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Level;

/**
 * Top level model for the Level builder
 * @author Jordan Burklund
 *
 */
public class LevelBuilderModel implements IMovable
{
	//TODO probably not the best structure...
	ArrayList<Level> levels;
	Level currentLevel;
	
	/**
	 * For storing the moves to undo in level builder.
	 * @author Kyle Carrero
	 */
	Stack<Move> undoStack = new Stack<Move>();
	
	/**
	 * For storing the moves to redo in level builder.
	 * @author Kyle Carrero
	 */
	Stack<Move> redoStack = new Stack<Move>();
	
	public LevelBuilderModel()
	{
		//TODO initial implementation.  Can definitely be better
		levels = new ArrayList<Level>(15);
		this.currentLevel = null;
	}
	
	public Level getCurrentLevel() {
		return currentLevel;
	}
	
	public void setCurrentLevel(Level level) {
		currentLevel = level;
	}
	
	/**
	 * Adds a level to the list
	 * TODO how to handle the level order number...
	 * @param level Level to add to the list
	 */
	public void addLevel(Level level) {
		levels.add(level);
	}
	
	/**
	 * Returns the undoStack so all moves can be undone
	 * @return the undoStack
	 */
	public Stack<Move> getUndoStack(){
		return undoStack;
	}
	
	/**
	 * Gets a copy of the list of levels
	 * @return a copy of the current list of levels for the level builder
	 * Creates a shallow duplicate of the list so that external code cannot
	 * modify the internal list
	 */
	public ArrayList<Level> getLevels() {
		return (ArrayList<Level>) levels.clone();
	}
	
	/**
	 * Removes a level from the list of available levels
	 * @param level Level to Remove
	 * @return true if the level was removed successfully
	 */
	public boolean removeLevel(Level level) {
		return levels.remove(level);
	}
	
	/**
	 * Pushes a move made onto the stack so that it can be undone.
	 * @param m
	 * @author Kyle Carrero
	 */
	public void pushMove(Move m) {
		undoStack.push(m);
	}

	/**
	 * Pops the last move from the undo stack if the stack is not empty.
	 * @return The last move performed on LevelBuilder, to be undone. If stack is empty, return null.
	 * @author Kyle Carrero
	 */
	public Move popMove() {
		try {
			Move m = undoStack.pop();
			return m;
		}
		catch(EmptyStackException e) {
//			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Clears the Moves in the Undo Stack.
	 * @author Kyle Carrero
	 */
	public void clearUndoMoves() {
		undoStack.removeAllElements();
	}
	
	/**
	 * Pushes a move made onto the stack so that it can be redone.
	 * @param m
	 * @author Kyle Carrero
	 */
	public void pushRedoMove(Move m) {
		redoStack.push(m);
	}

	/**
	 * Pops the last move from the redo stack if the stack is not empty.
	 * @return The last move performed on LevelBuilder, to be redone. If stack is empty, return null.
	 * @author Kyle Carrero
	 */
	public Move popRedoMove() {
		try {
			Move m = redoStack.pop();
			return m;
		}
		catch(EmptyStackException e) {
//			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Clears the Moves in the Redo Stack.
	 * @author Kyle Carrero
	 */
	public void clearRedoMoves() {
		redoStack.removeAllElements();
	}
	
	public int getNumLightning(){
		int n = 0;
		for(Level L: levels){
			if(L.getVariationName().equalsIgnoreCase("LIGHTNING")){
				n++;
			}
		}
		return n;
	}
	
	public int getNumPuzzle(){
		int n = 0;
		for(Level L: levels){
			if(L.getVariationName().equalsIgnoreCase("PUZZLE")){
				n++;
			}
		}
		return n;
	}
	
	public int getNumRelease(){
		int n = 0;
		for(Level L: levels){
			if(L.getVariationName().equalsIgnoreCase("RELEASE")){
				n++;
			}
		}
		return n;
	}
}

