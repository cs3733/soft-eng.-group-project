package diomedes.levelbuilder.view;
 
import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import diomedes.levelbuilder.control.CloseApplication;
import diomedes.levelbuilder.control.CreateLightningLevel;
import diomedes.levelbuilder.control.CreatePuzzleLevel;
import diomedes.levelbuilder.control.CreateReleaseLevel;
import diomedes.levelbuilder.control.DiscardChanges;
import diomedes.levelbuilder.control.FlipPieceHorizontal;
import diomedes.levelbuilder.control.FlipPieceVertical;
import diomedes.levelbuilder.control.GoToLevelSelect;
import diomedes.levelbuilder.control.RedoAction;
import diomedes.levelbuilder.control.RotatePieceLeft;
import diomedes.levelbuilder.control.RotatePieceRight;
import diomedes.levelbuilder.control.UndoAction;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.levelbuilder.view.panel.EditStatusView;
import diomedes.levelbuilder.view.panel.LevelSelectView;
import diomedes.levelbuilder.view.panel.MainMenuView;
import diomedes.levelbuilder.view.panel.SelectVariationView;
import diomedes.levelbuilder.view.popup.SaveAssuranceView;
 
/**
 * Creates application for Level Builder.
 * @author Aura
 *
 */
public class Application extends JFrame
{
    JPanel content;
    MainMenuView mainMenuPanel;
    SelectVariationView selectVariationPanel;
    LevelSelectView levelSelectPanel;
    EditLevelView editLevelView;
    JMenuBar menuBar;
    LevelBuilderModel model;
    JMenuItem saveItem;
    SaveAssuranceView saveAssurance;
   
    /**
     * Constructor for the application
     * @param model current model
     */
    public Application(LevelBuilderModel model)
    {
        //Set the top level model
        this.model = model;
       
        //Create the content panel
        content = new JPanel(new CardLayout());
        setContentPane(content);
       
        //Add Main Menu
        mainMenuPanel = new MainMenuView(this);
        content.add(mainMenuPanel, MainMenuView.NAME);
       
        //Add Select Variation
        selectVariationPanel = new SelectVariationView(this);
        content.add(selectVariationPanel, SelectVariationView.NAME);
       
        //Add Level Select
        levelSelectPanel = new LevelSelectView(this, model);
        content.add(levelSelectPanel, LevelSelectView.NAME);
       
        //Add Edit Level View
        editLevelView = new EditLevelView(this, model);
        content.add(editLevelView, EditLevelView.NAME);
       
        //Add Menu Bar
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);
       
        JMenu fileTab = new JMenu("File");
        menuBar.add(fileTab);
       
        JMenu newTab = new JMenu("New");
        fileTab.add(newTab);
       
        JMenuItem puzzleItem = new JMenuItem("Puzzle");
        newTab.add(puzzleItem);
        puzzleItem.addActionListener(new CreatePuzzleLevel(this));
       
        JMenuItem releaseItem = new JMenuItem("Release");
        newTab.add(releaseItem);
        releaseItem.addActionListener(new CreateReleaseLevel(this));
       
        JMenuItem lightningItem = new JMenuItem("Lightning");
        newTab.add(lightningItem);
        lightningItem.addActionListener(new CreateLightningLevel(this));
       
        JMenuItem editItem = new JMenuItem("Edit");
        fileTab.add(editItem);
        editItem.addActionListener(new GoToLevelSelect(this));
       
        fileTab.addSeparator();
       
        saveItem = new JMenuItem("Save");
        fileTab.add(saveItem);
        saveItem.setEnabled(false);
 
        fileTab.addSeparator();
       
        JMenuItem discardItem = new JMenuItem("Discard Changes");
        fileTab.add(discardItem);
        discardItem.addActionListener(new DiscardChanges(this));
        discardItem.setEnabled(false);
       
        JMenuItem closeItem = new JMenuItem("Close Application");
        closeItem.addActionListener(new CloseApplication(this));
        fileTab.add(closeItem);
       
        JMenu editTab = new JMenu("Edit");
        menuBar.add(editTab);
       
        JMenuItem undoItem = new JMenuItem("Undo");
        editTab.add(undoItem);
        undoItem.addActionListener(new UndoAction(this, model));
        undoItem.setEnabled(false);
       
        JMenuItem redoItem = new JMenuItem("Redo");
        editTab.add(redoItem);
        redoItem.addActionListener(new RedoAction(this, model));
        redoItem.setEnabled(false);
       
        editTab.addSeparator();
       
        JMenu pieceSub = new JMenu("Modify Piece");
        editTab.add(pieceSub);
        pieceSub.setEnabled(false);
       
        JMenuItem flipHorizontalItem = new JMenuItem("Flip Horizontal");
        pieceSub.add(flipHorizontalItem);
        flipHorizontalItem.addActionListener(new FlipPieceHorizontal(this, model));
       
        JMenuItem flipVerticalItem = new JMenuItem("Flip Vertical");
        pieceSub.add(flipVerticalItem);
        flipVerticalItem.addActionListener(new FlipPieceVertical(this, model));
       
        JMenuItem rotateLeftItem = new JMenuItem("Rotate Left");
        pieceSub.add(rotateLeftItem);
        rotateLeftItem.addActionListener(new RotatePieceLeft(this, model));
       
       
        JMenuItem rotateRightItem = new JMenuItem("Rotate Right");
        pieceSub.add(rotateRightItem);
        rotateRightItem.addActionListener(new RotatePieceRight(this, model));
       
//      JMenuItem resizeBoardItem = new JMenuItem("Resize Board");
//      editTab.add(resizeBoardItem);
//      resizeBoardItem.setEnabled(false);
       
        saveAssurance = new SaveAssuranceView(this);
       
        //Set some important settings
        setVisible(true);
        setTitle("LevelBuilder");
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBounds((1920/2) - 475, (1080/2) - 475, 800, 800); //We should probably change these numbers eventually
        addWindowListener(new CloseApplication(this));
        setLocation(0, 0);
    }
   
    /**
     * Sets the current view
     * @param name of the card layout to be set
     */
    public void setContentPanel(String name)
    {
        CardLayout cl = (CardLayout) content.getLayout();
        try
        {
            cl.show(content, name);
        }
        catch(Exception e)
        {
            System.err.println("Error setting content panel.\n");
        }
    }
   /**
    * Sets editing menu items 
    * @param isEnabled succeeds if true
    */
    public void setEditorMenuItems(boolean isEnabled)
    {
        JMenu fileTab = menuBar.getMenu(0);
        for(int i = 0; i < fileTab.getItemCount(); i++)
        {
            if(!(fileTab.getItem(i) instanceof JMenuItem))
            {
                continue;
            }
            switch(fileTab.getItem(i).getText())
			{
				case "Save":
				case "Discard Changes":
					fileTab.getItem(i).setEnabled(isEnabled);
					break;
				case "New":
				case "Edit":
					fileTab.getItem(i).setEnabled(!isEnabled);
					break;
			}
        }
       
        if(isEnabled) {
            saveItem.addActionListener(saveAssurance);
        }
        if(!isEnabled) {
            saveItem.removeActionListener(saveAssurance);
        }
       
        JMenu editTab = menuBar.getMenu(1);
        for(int i = 0; i < editTab.getItemCount(); i++)
        {
            if(!(editTab.getItem(i) instanceof JMenuItem))
            {
                continue;
            }
            switch(editTab.getItem(i).getText())
            {
                case "Resize Board":
                case "Undo":
                case "Redo":
                case "Modify Piece":
                    editTab.getItem(i).setEnabled(isEnabled);
                    break;
            }
        }
    }
   
    /**
     * Getter for Edit Level View
     * @return editLevelView
     */
    public EditLevelView getEditLevelView()
    {
        return editLevelView;
    }
 
    /**
     * Getter for editStatusView
     * @return editStatusView
     */ 
    public EditStatusView getEditStatusView() {
        return editLevelView.getEditStatusView();
    }
   
    /**
     * Determines whether a piece is currently selected.
     * @return True if a piece is selected, false if no piece is currently selected
     */
    public boolean isPieceSelected() {
        if(this.getEditLevelView().getBoard().getSelectedPiece() != null) {
            return true;
        }
        else if(this.getEditLevelView().getBullpen().getSelectedPiece() != null) {
            return true;
        }
        else if(this.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null) {
            return true;
        }
        return false;
    }
   
    /**
     * Deselects the currently selected piece if there is one.
     * @return True if piece successfully deselected, false if no piece to deselect.
     */
    public boolean deselectSelectedPiece() {
        if(this.getEditLevelView().getBoard().getSelectedPiece() != null) {
            this.getEditLevelView().getBoard().clearSelectedPiece();
            this.getEditLevelView().getBoardView().repaint();
            return true;
        }
        else if(this.getEditLevelView().getBullpen().getSelectedPiece() != null) {
            this.getEditLevelView().getBullpen().clearSelectedPiece();
            this.getEditLevelView().getBullpenView().repaint();
            return true;
        }
        else if(this.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null) {
            this.getEditLevelView().getPieceToolbarView().getToolbar().clearSelectedPiece();
            this.getEditLevelView().getPieceToolbarView().repaint();
            return true;
        }
        return false;
    }
   
    /**
     * Gets the LevelBuilderModel associated with the application.
     * @return The LevelBuilderModel associated with the application.
     * @author Kyle Carrero
     */
    public LevelBuilderModel getModel() {
        return model;
    }
   
    /**
     * Gets the LevelSelectView associated with the application
     * @return LevelSelectView for this application
     */
    public LevelSelectView getLevelSelectView() {
        return (LevelSelectView) this.levelSelectPanel;
    }
   
}