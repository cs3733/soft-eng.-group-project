package diomedes.levelbuilder.view.panel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import diomedes.levelbuilder.control.ActionToolbarColorController;
import diomedes.levelbuilder.control.ActionToolbarNumberController;
import diomedes.levelbuilder.control.AddHint;
import diomedes.levelbuilder.control.AddNumberButtonController;
import diomedes.levelbuilder.control.AddRandomNumbers;
import diomedes.levelbuilder.control.ClearNumbers;
import diomedes.levelbuilder.control.FlipPieceHorizontal;
import diomedes.levelbuilder.control.FlipPieceVertical;
import diomedes.levelbuilder.control.RedoAction;
import diomedes.levelbuilder.control.RemoveHint;
import diomedes.levelbuilder.control.ResizeBoard;
import diomedes.levelbuilder.control.RotatePieceLeft;
import diomedes.levelbuilder.control.RotatePieceRight;
import diomedes.levelbuilder.control.UndoAction;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.ReleaseData;

@SuppressWarnings("serial")
public class ActionToolbarView extends JPanel {
	private JLabel boardWidthText;
	private JLabel boardHeightText;
	
	JButton btnFlipUp;
	JButton btnFlipLeft;
	JButton btnRotateLeft;
	JButton btnRotateRight;
	JButton btnAddHints;
	JToggleButton btnRemoveHints;
	JButton btnRandomize;
	JButton btnClearNumbers;
	JButton btnUndo;
	JButton btnRedo;
	JLabel lblBoardSize;
	JButton upArrowHeight;
	JButton downArrowHeight;
	JButton upArrowWidth;
	JButton downArrowWidth;
	
	ButtonGroup colors;
	ButtonGroup numbers;
	JRadioButton rdbtnRed;
	JRadioButton rdbtnBlue;
	JRadioButton rdbtnGreen;
	JRadioButton radioButton1;
	JRadioButton radioButton2;
	JRadioButton radioButton3;
	JRadioButton radioButton4;
	JRadioButton radioButton5;
	JRadioButton radioButton6;
	JToggleButton tglbtnAddNumbers;
	Component horizontalStrut;
	Component horizontalStrut_1;
	Component horizontalStrut_2;
	
	Application app;
	LevelBuilderModel lb;
	EditLevelView lv;
	
	Color releaseColor;
	Integer releaseNumber;	
	

	public ActionToolbarView(Application app, LevelBuilderModel lb, EditLevelView lv){
		this.app = app;
		this.lb = lb;
		this.lv = lv;
		this.releaseColor = null;
		this.releaseNumber = null;
		setBorder(new LineBorder(new Color(0, 0, 0), 2));
		addButtons();
		addReleaseButtons();
	}
	
	void addButtons(){
		btnFlipUp = new JButton("Flip Vertical");
		btnFlipUp.addActionListener(new FlipPieceVertical(app, lb));
		add(btnFlipUp);

		btnFlipLeft = new JButton("Flip Horizontal");
		btnFlipLeft.addActionListener(new FlipPieceHorizontal(app, lb));
		add(btnFlipLeft);

		btnRotateLeft = new JButton("Rotate Left");
		btnRotateLeft.addActionListener(new RotatePieceLeft(app, lb));
		add(btnRotateLeft);

		btnRotateRight = new JButton("Rotate Right");
		btnRotateRight.addActionListener(new RotatePieceRight(app, lb));
		add(btnRotateRight);

		btnAddHints = new JButton("Add Hints");
		btnAddHints.addActionListener(new AddHint(app, lb));
		add(btnAddHints);

		btnRemoveHints = new JToggleButton("Remove Hints");
		btnRemoveHints.addActionListener(new RemoveHint(app, lb));
		add(btnRemoveHints);

		btnRandomize = new JButton("<html><center>" + "Randomize" + "<br>" + "Numbers" + "</center></html>");
		btnRandomize.addActionListener(new AddRandomNumbers(app));
		add(btnRandomize);

		btnClearNumbers = new JButton("<html><center>" + "Clear" + "<br>" + "Numbers" + "</center></html>");
		btnClearNumbers.addActionListener(new ClearNumbers(app));
		add(btnClearNumbers);

		btnUndo = new JButton("Undo");
		btnUndo.addActionListener(new UndoAction(app, lb));
		add(btnUndo);

		btnRedo = new JButton("Redo");
		btnRedo.addActionListener(new RedoAction(app, lb));
		add(btnRedo);
		
		horizontalStrut_1 = Box.createHorizontalStrut(30);
		add(horizontalStrut_1);
		
		lblBoardSize = new JLabel("Board Size:");
		lblBoardSize.setBounds(new Rectangle(0, 0, 100, 100));
		add(lblBoardSize);
		
		horizontalStrut = Box.createHorizontalStrut(30);
		add(horizontalStrut);
		

		
		boardHeightText = new JLabel();
		if(app.getModel().getCurrentLevel() != null) {
			boardHeightText.setText(Integer.toString(app.getModel().getCurrentLevel().getBoard().getHeight()));
		}
		else {
			boardHeightText.setText("12");
		}
		boardHeightText.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2, true));
		add(boardHeightText);
		
		upArrowHeight = new JButton("\u25b2");
		upArrowHeight.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		upArrowHeight.addActionListener(new ResizeBoard(app, lv, false, true));
		
		add(upArrowHeight);
		
		downArrowHeight = new JButton("\u25bc");
		downArrowHeight.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		downArrowHeight.addActionListener(new ResizeBoard(app, lv, false, false));
		
		add(downArrowHeight);
		
		horizontalStrut_2 = Box.createHorizontalStrut(15);
		add(horizontalStrut_2);
		
		boardWidthText = new JLabel();
		if(app.getModel().getCurrentLevel() != null) {
			boardWidthText.setText(Integer.toString(app.getModel().getCurrentLevel().getBoard().getWidth()));
		}
		else {
			boardWidthText.setText("12");
		}
		boardWidthText.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2, true));
		add(boardWidthText);
		
		upArrowWidth = new JButton("\u25b2");
		upArrowWidth.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		upArrowWidth.addActionListener(new ResizeBoard(app, lv, true, true));
		add(upArrowWidth);
		
	
		downArrowWidth = new JButton("\u25bc");
		downArrowWidth.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		downArrowWidth.addActionListener(new ResizeBoard(app, lv, true, false));
		add(downArrowWidth);
		


	}
	
	public void updateText(Level level) {
		if(level != null) {
			boardWidthText.setText(Integer.toString(level.getBoard().getWidth()));
			boardHeightText.setText(Integer.toString(level.getBoard().getHeight()));
		}
		else {
			boardWidthText.setText("12");
			boardHeightText.setText("12");
		}
	}
	
	/**Add the buttons pertaining to release */
	void addReleaseButtons(){
		//Instantiate Release Buttons
		colors = new ButtonGroup();
		numbers = new ButtonGroup();
		rdbtnRed = new JRadioButton("Red");
		rdbtnBlue = new JRadioButton("Blue");
		rdbtnGreen = new JRadioButton("Green");
		radioButton1 = new JRadioButton("1");
		radioButton2 = new JRadioButton("2");
		radioButton3 = new JRadioButton("3");
		radioButton4 = new JRadioButton("4");
		radioButton5 = new JRadioButton("5");
		radioButton6 = new JRadioButton("6");
		
		
		//Create Button Groups
		add(rdbtnRed);
		rdbtnRed.setForeground(Color.RED);
		rdbtnRed.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnRed.addActionListener(new ActionToolbarColorController(app, rdbtnRed));
		colors.add(rdbtnRed);
		
		
		add(rdbtnBlue);
		rdbtnBlue.setForeground(Color.BLUE);
		rdbtnBlue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnBlue.addActionListener(new ActionToolbarColorController(app, rdbtnBlue));
		colors.add(rdbtnBlue);
		
		
		add(rdbtnGreen);
		rdbtnGreen.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnGreen.setForeground(Color.GREEN);
		rdbtnGreen.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnGreen.addActionListener(new ActionToolbarColorController(app, rdbtnGreen));
		colors.add(rdbtnGreen);
		
	
		
		add(radioButton1);
		radioButton1.setHorizontalAlignment(SwingConstants.RIGHT);
		radioButton1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		radioButton1.addActionListener(new ActionToolbarNumberController(app, radioButton1));
		numbers.add(radioButton1);
		
		
		add(radioButton2);
		radioButton2.setHorizontalAlignment(SwingConstants.LEFT);
		radioButton2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		radioButton2.addActionListener(new ActionToolbarNumberController(app, radioButton2));
		numbers.add(radioButton2);
		
		add(radioButton3);
		radioButton3.setHorizontalAlignment(SwingConstants.RIGHT);
		radioButton3.setFont(new Font("Tahoma", Font.PLAIN, 11));
		radioButton3.addActionListener(new ActionToolbarNumberController(app, radioButton3));
		numbers.add(radioButton3);
		

		add(radioButton4);
		radioButton4.setHorizontalAlignment(SwingConstants.LEFT);
		radioButton4.setFont(new Font("Tahoma", Font.PLAIN, 11));
		radioButton4.addActionListener(new ActionToolbarNumberController(app, radioButton4));
		numbers.add(radioButton4);
		

		add(radioButton5);
		radioButton5.setHorizontalAlignment(SwingConstants.RIGHT);
		radioButton5.setFont(new Font("Tahoma", Font.PLAIN, 11));
		radioButton5.addActionListener(new ActionToolbarNumberController(app, radioButton5));
		numbers.add(radioButton5);
		
		add(radioButton6);
		radioButton6.setHorizontalAlignment(SwingConstants.LEFT);
		radioButton6.setFont(new Font("Tahoma", Font.PLAIN, 11));
		radioButton6.addActionListener(new ActionToolbarNumberController(app, radioButton6));
		numbers.add(radioButton6);
		
		tglbtnAddNumbers = new JToggleButton("Add Numbers");
		tglbtnAddNumbers.addActionListener(new AddNumberButtonController(app));
		add(tglbtnAddNumbers);
		
		
	}
	
	public JLabel getBoardWidthText() {
		return boardWidthText;
	}
	
	public JLabel getBoardHeightText() {
		return boardHeightText;
	}
	
	public JToggleButton getRemoveHintsButton()
	{
		return btnRemoveHints;
	}
	
	/**
	 * @return The Color ButtonGroup from the ActionToolbarView
	 */
	public ButtonGroup getColorButtonGroup() {
		return this.colors;
	}
	
	/**
	 * @return The Number ButtonGroup from the ActionToolbarView
	 */
	public ButtonGroup getNumberButtonGroup() {
		return this.numbers;
	}
	
	/**
	 * @return The JToggleButton used for toggling adding numbers to the board
	 */
	public JToggleButton getAddNumberButton() {
		return this.tglbtnAddNumbers;
	}

	/**
	 * Gets the ReleaseData based off of which radio buttons have been selected.
	 * @return A new ReleaseData with the information selected in the buttons, or null if no selection has been made yet
	 */
	public ReleaseData getReleaseDataFromRadioButtons() {
		if(this.releaseColor != null && this.releaseNumber != null) {
			return new ReleaseData(this.releaseNumber, this.releaseColor);
		}
		return null;
	}
	
	/**
	 * Sets the releaseColor attribute to the value of c if not null.
	 * @param c The Color you wish to set as the releaseColor
	 */
	public void setReleaseColor(Color c) {
		if(c != null) {
			this.releaseColor = c;
		}
	}
	
	/**
	 * Sets the releaseNumber attribute to the value of c if not null.
	 * @param i The Number you wish to set as the releaseNumber
	 */
	public void setReleaseNumber(Integer i) {
		if(i != null) {
			this.releaseNumber = i;
		}
	}

	/**
	 * Gets the Undo Button for testing purposes
	 * @return The Undo button
	 */
	public JButton getUndoButton() {
		return this.btnUndo;
	}
	
	/**
	 * Gets the Redo Button for testing purposes
	 * @return The Redo button
	 */
	public JButton getRedoButton() {
		return this.btnRedo;
	}
}
