
package diomedes.levelbuilder.view.panel;

import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import diomedes.levelbuilder.view.Application;

/**
 * View for editing a status
 * @author Kenedi
 * @author Aura
 *
 */
@SuppressWarnings("serial")
public class EditStatusView extends JPanel{
	JLabel label  = new JLabel("Options");
	JTextField textField = new JTextField(); ;
	boolean hasCreatedText;
	boolean hasCreatedLabel;
	String oldStatus;


	/**
	 * Constructor for edit status view.
	 * @param app current appliaction
	 */
	public EditStatusView(Application app){

		label = new JLabel("Options");
		textField = new JTextField();
		this.hasCreatedText = false;
		this.hasCreatedLabel = false;
		this.oldStatus = textField.getText();
	}

	/**
	 * @author Kenedi
	 * @return textField
	 */
	public JTextField getTextField(){
		return textField;
	}

	/**
	 * Gets the Jlabel
	 * @return label
	 */
	public JLabel getLabel(){
		return label;
	}
	
	/**
	 * Gets the old status
	 * @return oldstatus
	 */
	public String getOldStatus(){
		return oldStatus;
	}
	
	/**
	 * Resets the text field 
	 */
	public void resetTextField(){
		textField.setText(null);
	}

	/**Make a Text Field*/
	public void setTextField(boolean setText){
		if(!hasCreatedText && setText){
			add(textField);
			textField.setColumns(4);
			hasCreatedText = true;
		}
		else{
			if(hasCreatedText)
				textField.setVisible(setText);
		}


	}
	
	/**
	 * Adds the correct action listener for the specific level.
	 * @author Kenedi
	 * @param al ActionListener for specific variation
	 */
	public void setActionListener(ActionListener al){
		for(ActionListener actL: textField.getActionListeners()){
			textField.removeActionListener(actL);
		}
		textField.addActionListener(al);
	}
	
	/**Update the label's message */
	public void setTextLabel(String str){
		if(!hasCreatedLabel){
			label.setText(str);
			add(label);
			hasCreatedLabel = true;

		}
		else{
			label.setText(str);
			label.setVisible(true);
		}
	}

	/**
	 * Sets the text label
	 * @param setLabel
	 */
	public void setTextLabel(boolean setLabel){
		if(hasCreatedLabel){
			label.setVisible(setLabel);
		}

	}
	
	/**
	 * Sets the old status
	 * @param oldS the old status
	 */
	public void setOldStatus(String oldS){
		this.oldStatus = oldS;
	}


}


