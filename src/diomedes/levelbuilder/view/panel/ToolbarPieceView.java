package diomedes.levelbuilder.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import diomedes.levelbuilder.entity.Toolbar;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Position;

/**
 * Shows the toolbar piece view.
 * @author Myles Spencer
 *
 */

public class ToolbarPieceView extends JPanel
{
	Application app;
	Toolbar toolbar;
	int squareSize;
	int padding;
	int height;
	int width;
	
	/**
	 * Toolbar piece view constructor
	 * @param app currenta application
	 * @param tb the toolbar
	 */
	public ToolbarPieceView(Application app, Toolbar tb)
	{
		this.app = app;
		this.toolbar = tb;
		this.width = 80;
		this.height = width * this.toolbar.getPieces().size();
		this.padding = 0;
		this.squareSize = (width - (2 * padding)) / 6;
		setSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
		setBounds(0, 0, width, height);
//		System.out.println("Width: " + width + " Height: " + height + " # of Pieces: " + this.toolbar.getPieces().size());
	}
	
	@Override
	/**
	 * Paints the toolbar view
	 */
	public void paintComponent(Graphics g) 
	{
		int squareRadius = 5;
		int spacing = 1;
		for(int i = 0; i < toolbar.getPieces().size(); i++)
		{
			Piece p = toolbar.getPieces().get(i);
			boolean isSelected = false;
			if(p.equals(toolbar.getSelectedPiece()))
			{
				isSelected = true;
			}
			
			for(Position pos: p.getSquares())
			{
				int x = (this.width/2) + (pos.getCol() * squareSize) + spacing;
				int y = (this.width/2) + (i * 100) + (pos.getRow() * squareSize) + spacing;
				if(isSelected)
				{
					g.setColor(Color.YELLOW);
				}
				else
				{
					g.setColor(Color.GREEN);
				}
				g.fillRoundRect(x, y, squareSize, squareSize, squareRadius, squareRadius);
				g.setColor(Color.BLACK);
				g.drawRoundRect(x, y, squareSize, squareSize, squareRadius, squareRadius);
			}
		}
	}
}
