package diomedes.levelbuilder.view.panel;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import diomedes.levelbuilder.control.BoardController;
import diomedes.levelbuilder.control.DiscardChanges;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.entity.Toolbar;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Release;

/**
 * View for editing a level.
 * @author Myles Spencer
 *
 */

public class EditLevelView extends JPanel{
	
	public static final String NAME = "EditLevelView";
	
	PieceToolbarView pieceToolbar;
	BullpenView bullpenView;
//	AchievementView achievements;
	BoardView boardView;
	ActionToolbarView actionToolbar;
	EditStatusView statusView;
	
	/* Entities */
	Board board;
	Bullpen bullpen;
	
	//Level to edit
	Level level;
	
	Application app;
	

	/**
	 * Constructor for editing a level view.
	 */
	public EditLevelView(Application app, LevelBuilderModel lb) 
	{
		setBorder(new EmptyBorder(5, 5, 5, 5));
		
		this.app = app;
		actionToolbar = new ActionToolbarView(app, app.getModel(), this);
		statusView = new EditStatusView(app);
	}
	
	/**
	 * Sets up the layout for the edit level view
	 */
	private void setup(boolean isRelease)
	{
		boardView = new BoardView(app, board);
		boardView.addMouseListener(new BoardController(app));
		boardView.addMouseMotionListener(new BoardController(app));
	
		pieceToolbar = new PieceToolbarView(app, new Toolbar());
		pieceToolbar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pieceToolbar.setPreferredSize(new Dimension(125, 500));
		
		bullpenView = new BullpenView(app, bullpen);
		
		setRelease(isRelease);
		
		JButton btnBackToMain = new JButton("Return");
		btnBackToMain.addActionListener(new DiscardChanges(app));
		
		btnBackToMain.setFont(new Font("Tahoma", Font.PLAIN, 11));
		

		GroupLayout gl_contentPane = new GroupLayout(this);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(pieceToolbar, GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
						.addComponent(btnBackToMain, 0, 0, Short.MAX_VALUE))
					.addGap(40)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(bullpenView, GroupLayout.PREFERRED_SIZE, 459, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(statusView, GroupLayout.PREFERRED_SIZE, 297, GroupLayout.PREFERRED_SIZE)
								.addComponent(boardView, GroupLayout.PREFERRED_SIZE, 358, GroupLayout.PREFERRED_SIZE))
							.addGap(46)
							.addComponent(actionToolbar, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)))
					.addGap(39))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addContainerGap()
								.addComponent(bullpenView, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(statusView, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
										.addGap(33)
										.addComponent(boardView, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE))
									.addComponent(actionToolbar, GroupLayout.PREFERRED_SIZE, 520, GroupLayout.PREFERRED_SIZE)))
							.addComponent(btnBackToMain, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
						.addComponent(pieceToolbar, GroupLayout.PREFERRED_SIZE, 601, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(48, Short.MAX_VALUE))
		);
		
		
		setLayout(gl_contentPane);
	}
	
	/**
	 * Sets the level to edit
	 * @param level Level to edit
	 */
	public void setLevelToEdit(Level level) {
		//TODO Validate the level being passed
		if(level == null) {
			return;
		}
		this.level = level;
		this.bullpen = level.getBullpen();
		this.board = level.getBoard();
		this.actionToolbar.updateText(level);
		this.removeAll();
		if(level instanceof Release) {
			setup(true);
		}
		else {
			setup(false);
		}
	}
	
	/**
	 * Enables and disables the appropriate buttons for a Release level
	 */
	public void setRelease(boolean isEnabled) {
		
		actionToolbar.radioButton1.setVisible(isEnabled);
		actionToolbar.radioButton2.setVisible(isEnabled);
		actionToolbar.radioButton3.setVisible(isEnabled);
		actionToolbar.radioButton4.setVisible(isEnabled);
		actionToolbar.radioButton5.setVisible(isEnabled);
		actionToolbar.radioButton6.setVisible(isEnabled);
		actionToolbar.rdbtnRed.setVisible(isEnabled);
		actionToolbar.rdbtnBlue.setVisible(isEnabled);
		actionToolbar.rdbtnGreen.setVisible(isEnabled);
		actionToolbar.tglbtnAddNumbers.setVisible(isEnabled);
		actionToolbar.btnClearNumbers.setVisible(isEnabled);
		actionToolbar.btnRandomize.setVisible(isEnabled);

	}

	/**
	 * Gets the current status view.
	 * @return status view
	 */
	public EditStatusView getEditStatusView() {
		return statusView;
	}
	
	/**
	 * Returns the current board.
	 * @return board.
	 */
	public Board getBoard() {
		return this.board;
	}
	
	/**
	 * Returns the bullpen
	 * @return bullpen
	 */
	public Bullpen getBullpen() {
		return bullpen;
	}

	/**
	 * Returns the view of the action toolbar view.
	 * @return the action toolbar view.
	 */
	public ActionToolbarView getActionToolbarView() {
		return actionToolbar;
	}
	
	/**
	 * Returns the current board view.
	 * @return
	 */
	public BoardView getBoardView() {
		return boardView;
	}
	
	/**
	 * Returns the piece toolbar view
	 * @return piecetoolbar the view for the toolbar
	 */
	public PieceToolbarView getPieceToolbarView() {
		return pieceToolbar;

	}
	
	/**
	 * Returns the bullpen view
	 * @return bullpenview
	 */
	public BullpenView getBullpenView()
	{
		return bullpenView;
	}
}

