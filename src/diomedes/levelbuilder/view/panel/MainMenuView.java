package diomedes.levelbuilder.view.panel;

import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import diomedes.levelbuilder.control.CloseApplication;
import diomedes.levelbuilder.control.GoToLevelSelect;
import diomedes.levelbuilder.control.NewLevel;
import diomedes.levelbuilder.view.Application;

/**
 * The view for the main menu

 */
public class MainMenuView extends JPanel
{
	public static final String NAME = "MainMenuView";
	JButton newButton;
	JButton exitButton;
	JButton editButton;
	
	/**
	 * Sets up the view for the main menu
	 * @param app the current application
	 */
	public MainMenuView(Application app) {
		setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JLabel lblKabasuji = new JLabel("Level Builder");
		lblKabasuji.setHorizontalAlignment(SwingConstants.CENTER);
		lblKabasuji.setFont(new Font("Times New Roman", Font.BOLD, 32));
		
		newButton = new JButton("Create New Level");
		newButton.setFont(new Font("Times New Roman", Font.BOLD, 20));
		newButton.addActionListener(new NewLevel(app));
		
		exitButton = new JButton("Exit");
		exitButton.setFont(new Font("Times New Roman", Font.BOLD, 20));
		exitButton.addActionListener(new CloseApplication(app));
		
		editButton = new JButton("Edit Levels");
		editButton.setFont(new Font("Times New Roman", Font.BOLD, 20));
		editButton.addActionListener(new GoToLevelSelect(app));
		
		GroupLayout gl_contentPane = new GroupLayout(this);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(157)
					.addComponent(lblKabasuji, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(185, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(178)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(editButton, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
						.addComponent(exitButton, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
						.addComponent(newButton, GroupLayout.PREFERRED_SIZE, 159, Short.MAX_VALUE))
					.addGap(209))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(73)
					.addComponent(lblKabasuji, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(115)
					.addComponent(newButton, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(editButton, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addComponent(exitButton, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(28, Short.MAX_VALUE))
		);
		setLayout(gl_contentPane);
	}
}
