package diomedes.levelbuilder.view.panel;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import diomedes.levelbuilder.control.DeleteLevel;
import diomedes.levelbuilder.control.EditLevel;
import diomedes.levelbuilder.control.ReturnToMainMenu;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Level;
import diomedes.shared.view.LevelAchievmentRenderer;
import diomedes.shared.view.LevelNameRenderer;

/**
 * View for the level selection
 *
 */
public class LevelSelectView extends JPanel {

	public static final String NAME = "LevelSelectView";
	private JTable table;
	LevelBuilderModel model;
	
	/**
	 * Sets up the view for the level select
	 * @param app current application
	 * @param model current model
	 */
	public LevelSelectView(Application app, LevelBuilderModel model) {
		this.model = model;
		
		JLabel lblVariationName = new JLabel("Created Levels");
		lblVariationName.setHorizontalAlignment(SwingConstants.CENTER);
		lblVariationName.setFont(new Font("Times New Roman", Font.BOLD, 20));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportBorder(null);
		
		JButton btnPlaySelectedLevel = new JButton("Edit Selected Level");
		btnPlaySelectedLevel.addMouseListener(new EditLevel(app));
		
		JButton btnDeleteSelectedLevel = new JButton("Delete Selected Level");
		btnDeleteSelectedLevel.addMouseListener(new DeleteLevel(model, app));
		
		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ReturnToMainMenu(app));
		
		GroupLayout gl_contentPane = new GroupLayout(this);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(67)
					.addComponent(lblVariationName, GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
					.addGap(73))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(85)
					.addComponent(btnPlaySelectedLevel)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(btnDeleteSelectedLevel)
					.addGap(108))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnReturn)
					.addContainerGap(411, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblVariationName, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE, false)
						.addComponent(btnPlaySelectedLevel)
						.addComponent(btnDeleteSelectedLevel))
					.addGap(46)
					.addComponent(btnReturn)
					.addContainerGap())
		);
		
		table = new JTable();
		
		scrollPane.setViewportView(table);
		setLayout(gl_contentPane);
	}
	
	/**
	 * Sets up the table from the list of levels in the top-level model
	 * This method needs to be called before the View is displayed so that
	 * all of the currently available levels are displayed
	 */
	public void setUpTable() {
		ArrayList<Level> levels = model.getLevels();
		Object[][] obj = new Object[levels.size()][2];
		
		//Add all of the levels in the list to the table
		for(int i=0; i<levels.size(); i++) {
			obj[i][0] = levels.get(i);
			obj[i][1] = levels.get(i);
		}
		
		//Configure the table
		table.setModel(new DefaultTableModel(obj, new String[] {"Level Name", "Achievements" }) {
			//Array representing the data type of each column
			Class[] columnTypes = new Class[] {String.class, String.class};
			//Method to get the data types for each column
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			//Set both columns to not be editable
			boolean[] columnEditables = new boolean[] {false, false};
			//Check if the cell is editable
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(150);
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(0).setCellRenderer(new LevelNameRenderer());
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setMinWidth(50);
		table.getColumnModel().getColumn(1).setCellRenderer(new LevelAchievmentRenderer());
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	}
	
	/**
	 * Get the level that is currently selected in the table
	 * @return Level that is selected in the table
	 * @author Jordan Burklund
	 */
	public Level getTableSelectedLevel() {
		int selectedRow = table.getSelectedRow();
		if(selectedRow > -1) {
			//A row is selected, get the level in this row
			if(table.getModel().getValueAt(selectedRow, 0) == null) {
				//This is an empty row
				return null;
			} else {
				return (Level) table.getModel().getValueAt(selectedRow, 0);
			}
		} else {
			return null;
		}
	}
}
