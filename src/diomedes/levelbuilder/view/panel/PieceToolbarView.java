package diomedes.levelbuilder.view.panel;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import diomedes.levelbuilder.control.PieceToolbarController;
import diomedes.levelbuilder.control.PieceToolbarScrollbarListener;
import diomedes.levelbuilder.entity.Toolbar;
import diomedes.levelbuilder.view.Application;

/**
 * Piece Toolbar view
 * @author Kyle Carrero
 * @author Myles Spencer
 *
 */
public class PieceToolbarView extends JScrollPane {

	Toolbar toolbar;
	Application application;
	JPanel toolbarPieces;
	
	/**
	 * Constructor for the piece toolbar.
	 * @param application current application
	 * @param toolbar current toolbar.
	 */
	public PieceToolbarView(Application application, Toolbar toolbar) {
		this.toolbar = toolbar;
		this.application = application;
		
		
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.getVerticalScrollBar().addAdjustmentListener(new PieceToolbarScrollbarListener(application));
		this.toolbarPieces = new ToolbarPieceView(application, toolbar);
		this.toolbarPieces.addMouseListener(new PieceToolbarController(application, this));
		setViewportView(toolbarPieces);
	}
	
	/**
	 * Updates the view.
	 */
	public void update()
	{
		updateUI();
		toolbarPieces.repaint();
	}

	/**
	 * Gets the piece views.
	 * @return JPanel the piece views
	 */
	public JPanel getPieceViews() {
		return toolbarPieces;
	}
	
	/**
	 * Gets the toolbar 
	 * @return Toolbar 
	 */
	public Toolbar getToolbar() {
		return toolbar;
	}
	
}
