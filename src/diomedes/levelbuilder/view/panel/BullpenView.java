package diomedes.levelbuilder.view.panel;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import diomedes.levelbuilder.control.BullpenController;
import diomedes.levelbuilder.control.BullpenScrollbarListener;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Bullpen;

/**
 * @author Kyle Carrero
 * @author Myles Spencer
 */
@SuppressWarnings("serial")
public class BullpenView extends JScrollPane {

	Bullpen bullpen;
	Application application;
	BullpenPieceView bullpenPieces;
	
	public BullpenView(Application application, Bullpen bullpen) {
		this.bullpen = bullpen;
		this.application = application;
		
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.getHorizontalScrollBar().addAdjustmentListener(new BullpenScrollbarListener(application));
		this.bullpenPieces = new BullpenPieceView(application);
		this.bullpenPieces.addMouseListener(new BullpenController(application));

		setViewportView(bullpenPieces);
	}
	
	public void update()
	{
		updateUI();
		bullpenPieces.repaint();
	}
	
	public BullpenPieceView getPieceViews() {
		return bullpenPieces;
	}

	/**
	 * Gets the bullpen for this view.
	 * @return The bullpen rendered by this view.
	 */
	public Bullpen getBullpen() {
		return bullpen;
	}
}
