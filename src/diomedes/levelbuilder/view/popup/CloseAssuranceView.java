package diomedes.levelbuilder.view.popup;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.levelbuilder.view.Application;

@SuppressWarnings("serial")

/**
 * Ensures the closing of the app
 */
public class CloseAssuranceView extends JDialog implements ActionListener {

	private JOptionPane optionPane;
	JButton exitButton;
	JButton returnButton;
	Application app;

	/**
	 * Create the frame.
	 */
	public CloseAssuranceView(Application app) {
		this.app = app;
		
		exitButton = new JButton();
		exitButton.setText("Close Application");
		exitButton.addActionListener(this);
		
		returnButton = new JButton();
		returnButton.setText("Cancel");
		returnButton.addActionListener(this);
		
		Object[] options = {exitButton, returnButton};
		optionPane = new JOptionPane("Are you sure you want to close Level Builder?",
									JOptionPane.QUESTION_MESSAGE,
									JOptionPane.YES_NO_OPTION,
									null,
									options,
									options[1]);
		add(optionPane);
		setLocationRelativeTo(app);
		setVisible(true);
		setModal(true);
		setTitle("Close?");
		setSize(new Dimension(400, 125));
		
	}

	@Override
	/**
	 * Closes the window and/or the app depending on the user's choice.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(returnButton)) {
			dispose();
		}		
		else
		{
			app.dispose();
			dispose();
		}
	}
}
