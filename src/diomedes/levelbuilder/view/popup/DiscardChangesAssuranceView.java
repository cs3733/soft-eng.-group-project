package diomedes.levelbuilder.view.popup;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.levelbuilder.control.DiscardChangesController;
import diomedes.levelbuilder.view.Application;

@SuppressWarnings("serial")
/**
 * Assurance to discard changes.
 */
public class DiscardChangesAssuranceView extends JDialog implements ActionListener {

	private JOptionPane optionPane;
	JButton exitButton;
	JButton returnButton;
	Application app;

	/**
	 * Create the frame.
	 * @param app The Application
	 */
	public DiscardChangesAssuranceView(Application app) {
		this.app = app;
		
		exitButton = new JButton();
		exitButton.setText("Discard Changes");
		exitButton.addActionListener(new DiscardChangesController(app, this));
		
		returnButton = new JButton();
		returnButton.setText("Return to Level");
		returnButton.addActionListener(this);	
		
		Object[] options = {exitButton, returnButton};
		optionPane = new JOptionPane("Are you sure you want to discard your changes?",
									JOptionPane.QUESTION_MESSAGE,
									JOptionPane.YES_NO_OPTION,
									null,
									options,
									options[1]);
		add(optionPane);
		setLocationRelativeTo(app);
		setVisible(true);
		setModal(true);
		setTitle("Close with saving?");
		setSize(new Dimension(450, 125));
		
	}

	@Override
	/**
	 * Disposes the window.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(returnButton)) {
			dispose();
		}		
	}
}
