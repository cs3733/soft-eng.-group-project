package diomedes.levelbuilder.view.popup;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.levelbuilder.control.SaveLevel;
import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.MainMenuView;

/**
 * View to ensure the user is ready to save.
 */
public class SaveAssuranceView extends JDialog implements ActionListener {

	private JOptionPane optionPane;
	private JDialog popupWindow;
	JButton exitButton;
	JButton returnButton;
	JButton btn;
	Application app;

	/**
	 * Create the frame.
	 * @param app The Application
	 */
	public SaveAssuranceView(Application app) {
		this.app = app;
		popupWindow = new JDialog(app);
		
		exitButton = new JButton();
		exitButton.setText("Save Level");
		exitButton.addActionListener(this);;
		
		returnButton = new JButton();
		returnButton.setText("Cancel");
		returnButton.addActionListener(this);

	}
	
	/**
	 * Sets up the pop up.
	 */
	public void setUp() {
		Object[] options = {exitButton, returnButton};
		optionPane = new JOptionPane("Are you sure you want to save?",
									JOptionPane.QUESTION_MESSAGE,
									JOptionPane.YES_NO_OPTION,
									null,
									options,
									options[1]); 
		add(optionPane);
		setLocationRelativeTo(app);
		setVisible(true);
		setModal(true);
		setTitle("Close with saving?");
		setSize(new Dimension(450, 125));
	}

	@Override
	/**
	 * Performs based on the decision made by the user. If saved; it saves. Otherwise sets up.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(returnButton)) {
			dispose();
		}
		else if(e.getSource().equals(exitButton)){
			LevelBuilderModel lb = app.getModel();
			btn = new JButton();
			btn.setVisible(false);
			btn.addActionListener(new SaveLevel(lb));
			btn.doClick();
			app.setContentPanel(MainMenuView.NAME);
			app.setEditorMenuItems(false);
			dispose();
		}		
		else {
			setUp();
		}
	}
}