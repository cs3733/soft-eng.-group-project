package diomedes.levelbuilder.control;

import java.security.InvalidParameterException;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.PlacedPiece;

/**
 * Move piece to bullpen from board.
 *
 */
public class MoveToBullpenFromBoard extends Move {
	
	PlacedPiece piece;
	Bullpen bullpen;
	
	/**
	 * Constructor for the move class
	 * @param piece piece being selected
	 * @param bullpen bullpen
	 */
	public MoveToBullpenFromBoard(PlacedPiece piece, Bullpen bullpen) {
		//Can't do anything if the bullpen isn't set
		if(bullpen == null) {
			throw new InvalidParameterException("bullpen is not set");
		}
		//Can't do anything if the piece to move isn't set
		if(piece == null) {
			throw new InvalidParameterException("Piece to move not set");
		}
		
		this.piece = piece;
		this.bullpen = bullpen;
	}

	@Override
	/**
	 * Undo the move
	 */
	public boolean undoMove(IMovable lb) {
		Level level = ((LevelBuilderModel) lb).getCurrentLevel();
		Board board = ((LevelBuilderModel) lb).getCurrentLevel().getBoard();
		
		//Add the piece to the bullpen
		if(bullpen.removePiece(piece.getPiece())) {
			bullpen.clearSelectedPiece();
			board.clearSelectedPiece();
			board.addPiece(piece.copy());
			
			return true;
		}

		return false;
	}

	@Override
	/**
	 * Adds the piece to the bullpen
	 */
	public boolean doMove(IMovable lb) {
		//Check if the move is valid
		if(!isValid(lb)) {
			return false;
		}
		
		Level level = ((LevelBuilderModel) lb).getCurrentLevel();
		Board board = ((LevelBuilderModel) lb).getCurrentLevel().getBoard();
		
		//Add the piece to the bullpen
		bullpen.addPiece(piece.getPiece());
		board.clearSelectedPiece();
		board.removePiece(piece);
		
		return true;
	}

	@Override
	/**
	 * Check if there's a selected piece
	 */
	public boolean isValid(IMovable lb) {
		Level level = ((LevelBuilderModel) lb).getCurrentLevel();
		//is the piece in the move a selected piece
		if(level.getBoard().getSelectedPiece() != piece) {
			return false;
		}
		
		return true;
	}

}
