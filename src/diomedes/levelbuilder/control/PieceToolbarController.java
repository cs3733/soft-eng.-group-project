package diomedes.levelbuilder.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.PieceToolbarView;
import diomedes.shared.entity.Piece;

/**
 * Controller for the Piece Toolbar.
 * @author Kyle Carrero
 * @author Myles Spencer
 */
public class PieceToolbarController implements MouseListener {
	
	Application app;
	LevelBuilderModel model;
	PieceToolbarView toolbarView;
	/**
	 * Constructor for the Piece Toolbar Controller
	 * @param application current application
	 * @param toolbarView toolbar view
	 */
	public PieceToolbarController(Application application, PieceToolbarView toolbarView) {
		this.app = application;
		this.model = application.getModel();
		this.toolbarView = toolbarView;
	}

	@Override
	/**
	 * Handles selecting pieces to the toolbar to move to Board or Bullpen.
	 */
	public void mouseClicked(MouseEvent me) {
		int index = (me.getY() / 100);
		if(index < toolbarView.getToolbar().getPieces().size())
		{
			Piece s = toolbarView.getToolbar().getSelectedPiece();
			toolbarView.getToolbar().clearSelectedPiece();
			Piece p = toolbarView.getToolbar().getPieces().get(index);
			if(model.getCurrentLevel().getBoard().getSelectedPiece() != null && !app.getEditLevelView().getActionToolbarView().getAddNumberButton().isSelected())
			{
				model.getCurrentLevel().getBoard().removePiece(model.getCurrentLevel().getBoard().getSelectedPiece());
				model.getCurrentLevel().getBoard().clearSelectedPiece();
				app.getEditLevelView().getBoardView().repaint();
			}
			else if(model.getCurrentLevel().getBullpen().getSelectedPiece() != null)
			{
				Move m = new MoveToToolbarFromBullpen(app, model.getCurrentLevel().getBullpen().getSelectedPiece(), model.getCurrentLevel().getBullpen());
				if(m.doMove(model))
				{
					model.pushMove(m);
				}
				app.getEditLevelView().getBullpenView().repaint();
			}
			else if(s != p && !app.getEditLevelView().getActionToolbarView().getAddNumberButton().isSelected())
			{
				toolbarView.getToolbar().setSelectedPiece(p);
			}
			toolbarView.update();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
