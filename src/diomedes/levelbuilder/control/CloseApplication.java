package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.popup.CloseAssuranceView;

/**
 * Controller to close the application
 */
public class CloseApplication implements ActionListener, WindowListener
{
	Application app;
	
	/**
	 * Default constructor for closing the application
	 * @param app	currenta application
	 */
	public CloseApplication(Application app)
	{
		this.app = app;
	}

	@Override
	/**
	 * Ensures user wants to close the app.
	 */
	public void actionPerformed(ActionEvent e)
	{
		CloseAssuranceView ca = new CloseAssuranceView(app);
	}

	@Override
	public void windowActivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosing(WindowEvent arg0)
	{
		CloseAssuranceView ca = new CloseAssuranceView(app);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

}
