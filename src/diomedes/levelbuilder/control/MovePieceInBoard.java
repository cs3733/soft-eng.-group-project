package diomedes.levelbuilder.control;

import java.security.InvalidParameterException;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;

/**
 * Class that handles moving a piece from one board position to another
 * Has methods to do the move, undo the move, and check if the move is valid
 * @author Jordan Burklund
 *
 */
public class MovePieceInBoard extends Move {
	Position newBoardPos;
	PlacedPiece piece;
	
	/**
	 * Constructor specifying the piece to move, and the new desired Position
	 * @param newPosition New desired position of the piece
	 * @param piece Piece to move to the new position
	 * @throws InvalidParameterException if either parameters are null
	 */
	public MovePieceInBoard(Position newPosition, PlacedPiece piece) {
		//Can't do anything if the position isn't set
		if(newPosition == null) {
			throw new InvalidParameterException("New Position not set");
		}
		//Can't do anything if the piece to move isn't set
		if(piece == null) {
			throw new InvalidParameterException("Piece to move not set");
		}
		
		this.newBoardPos = newPosition;
		this.piece = piece;
	}

	/**
	 * Moves the piece back to it's original position
	 * Assumes that the move was initially valid
	 * @param lb Top level model
	 * @return true if completed correctly
	 */
	@Override
	public boolean undoMove(IMovable lb) {
		Board board = ((LevelBuilderModel) lb).getCurrentLevel().getBoard();
		board.removePiece(new PlacedPiece(newBoardPos, piece.getPiece()));
		board.addPiece(piece);
		return true;
	}

	/**
	 * Attempts to move the piece to the desired position on the board
	 * If the move is not valid, this returns false, the move is not executed
	 * @param lb
	 * @return true if the move could be completed successfully
	 */
	@Override
	public boolean doMove(IMovable lb) {
		//Temporarily remove the piece from the board
		Board board = ((LevelBuilderModel) lb).getCurrentLevel().getBoard();
		if(!board.removePiece(piece)) {
			//Could not remove the piece from the board because it isn't in the board
			return false;
		}
		//Check if the move is valid
		if(!isValid(lb)) {
			//Move was not valid, add the piece back in its original location
			board.addPiece(piece);
			return false;
		}
		//Add the piece back to the board at the new position
		//(Creates a new Piece rather than modifying the old one)
		board.addPiece(new PlacedPiece(newBoardPos, piece.getPiece()));
		return true;
	}

	/**
	 * Check if the move is valid
	 * @return true if the move is valid
	 */
	@Override
	public boolean isValid(IMovable lb) {
		Board board = ((LevelBuilderModel) lb).getCurrentLevel().getBoard();
		//Try to add the piece to the board at the new position
		if(board.isValid(new PlacedPiece(newBoardPos, piece.getPiece()))) {
			return true;
		}
		return false;
	}
	

}
