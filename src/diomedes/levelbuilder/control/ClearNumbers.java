package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Release;

/**
 * Controller to cleaer all the numbers from the board
 */
public class ClearNumbers implements ActionListener {

	Application app;

	/**
	 * Clears the number constructor
	 * @param app
	 */
	public ClearNumbers(Application app) {
		this.app = app;
	}

	@Override
	/**
	 * Clears all the numbers from the board.
	 */
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel() instanceof Release) {
			MoveClearNumbers mcn = new MoveClearNumbers(app);
			if(mcn.doMove(app.getModel())) {
				app.getModel().pushMove(mcn);
				app.getEditLevelView().getBoardView().repaint();
			}
		}
	}
	
}
