package diomedes.levelbuilder.control;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.PlacedPiece;

/**
 * The move for adding a hint to the board.
 * @author Kyle Carrero
 * @author Myles Spencer
 *
 */
public class AddHintMove extends Move {

	Application app;
	LevelBuilderModel lb;
	Board board;
	PlacedPiece hintPiece;

	/**
	 * Constructor to add a hint.
	 * @param app current application.
	 * @param lb
	 */
	public AddHintMove(Application app, LevelBuilderModel lb) {
		this.app = app;
		this.lb = lb;
		this.board = lb.getCurrentLevel().getBoard();
		this.hintPiece = board.getSelectedPiece();
	}
	
	@Override
	/**
	 * Removes the hint, and adds the piece. 
	 */
	public boolean undoMove(IMovable lb) {
		board.removeHint(hintPiece);
		board.addPiece(hintPiece);
		app.getModel().getCurrentLevel().getBullpen().removePiece(hintPiece.getPiece());

		return true;
	}

	@Override
	/**
	 * Adds a hint, and removes the piece.
	 */
	public boolean doMove(IMovable lb) {
		if(!isValid(lb)) {
			return false;
		}
		
		board.addHint(hintPiece);
		board.removePiece(hintPiece);
		app.getModel().getCurrentLevel().getBullpen().addPiece(hintPiece.getPiece());
		
		return true;
	}

	@Override
	/**
	 * Checks that there's a hint piece.
	 */
	public boolean isValid(IMovable lb) {
		if(hintPiece != null) {
			return true;
		}
		return false;
	}

}
