package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.MainMenuView;

/**
 * Controller to return to the main menu
 */
public class ReturnToMainMenu implements ActionListener
{
	Application app;
	/**
	 * Constructor for returning to main menu.
	 * @param app current application.
	 */
	public ReturnToMainMenu(Application app)
	{
		this.app = app;
	}

	@Override
	/**
	 * Switches to Main Menu View once clicked.
	 */
	public void actionPerformed(ActionEvent e)
	{
		app.setContentPanel(MainMenuView.NAME);
		app.setEditorMenuItems(false);
	}
}
