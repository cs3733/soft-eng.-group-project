package diomedes.levelbuilder.control;

import diomedes.shared.entity.IMovable;

/**
 * Abstract move class.
 * @author Aura
 * inspired by @author Heineman
 */
public abstract class Move {
	
	/**
	 * Constructor
	 */
	protected Move(){};
		
	/**
	 * Each class must know if it can or not undo 
	 * @param lb current state of level builder
	 * @return true if valid*/
	public abstract boolean undoMove(IMovable lb);
	
	
	/**
	 * Each move should know how to execute itself 
	 * @param lb current state of Level Builder
	 * @return true if valid*/
	public abstract boolean doMove(IMovable lb);
	
	/**
	 * Is the move valid?
	 * @param lb current state of Level Builder
	 * @return true if valid
	 */
	public abstract boolean isValid(IMovable lb);

}
