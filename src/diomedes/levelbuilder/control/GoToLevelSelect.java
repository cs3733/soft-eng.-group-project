package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.LevelSelectView;

//TODO switch to mouse listener?
/**
 *Shows the level select
 *
 */
public class GoToLevelSelect implements ActionListener {
	Application app;

	/**
	 * Constructor for GotoLevelSelect
	 * @param app current application
	 */
	public GoToLevelSelect(Application app)
	{
		this.app = app;
	}

	@Override
	/**
	 * Sets the correct view, meaning it moves to level select of correct variation.
	 */
	public void actionPerformed(ActionEvent e) {
		app.setContentPanel(LevelSelectView.NAME);
		LevelSelectView lsview = app.getLevelSelectView();
		lsview.setUpTable();
		app.setEditorMenuItems(false);
	}
}
