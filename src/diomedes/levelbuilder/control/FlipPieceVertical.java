package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Piece;

public class FlipPieceVertical implements ActionListener {

	Application app;
	LevelBuilderModel lb;

	/**
	 * Flips pieces vertically. 
	 * @param app current application
	 * @param lb currenet model
	 */
	public FlipPieceVertical(Application app, LevelBuilderModel lb) {
		this.app = app;
		this.lb = lb;
	}

	@Override
	/**
	 * Flips the piece vertically. 
	 */
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
			Piece p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
			p.flipVertical();
			app.getEditLevelView().repaint();
		}
		else if(app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null) {
			Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece();
			p.flipVertical();
			app.getEditLevelView().repaint();
		}
	}
	
}
