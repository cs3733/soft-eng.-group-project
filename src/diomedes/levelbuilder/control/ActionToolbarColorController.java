package diomedes.levelbuilder.control;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import diomedes.levelbuilder.view.Application;

/**
 * Controls the radio button for adding a colored number in release level
 * @author Aura
 *
 */
public class ActionToolbarColorController implements ActionListener {

	/** 
	 * current application
	 */
	Application app;
	/**
	 * current button
	 */
	JRadioButton btn;
	
	/**
	 * controller needed t select a color for a numbered tile in release
	 * @param app currrent application
	 * @param btn current button
	 */
	public ActionToolbarColorController(Application app, JRadioButton btn) {
		this.app = app;
		this.btn = btn;
	}
	
	@Override
	/**
	 * Selects the color for the numberedTile in release
	 */
	public void actionPerformed(ActionEvent arg0) {
		Color c = null;
		ButtonGroup bg = app.getEditLevelView().getActionToolbarView().getColorButtonGroup();
		if(bg.isSelected(this.btn.getModel())) {
			switch(this.btn.getText()) {
			case "Red": c = Color.RED;
				break;
			case "Blue": c = Color.BLUE;
				break;
			case "Green": c = Color.GREEN;
				break;
			}
			
			app.getEditLevelView().getActionToolbarView().setReleaseColor(c);
		}
	}

}
