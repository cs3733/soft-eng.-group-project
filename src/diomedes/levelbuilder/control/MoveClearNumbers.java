package diomedes.levelbuilder.control;

import java.util.ArrayList;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.NumberedTile;
import diomedes.shared.entity.Tile;

/**
 * Move class for clearing numbers from a release board.
 */
public class MoveClearNumbers extends Move {
	
	Application app;
	ArrayList<NumberedTile> clearedTiles;
	
	/**
	 * Constructor for the move.
	 * @param app current application.
	 */
	public MoveClearNumbers(Application app) {
		this.app = app;
		this.clearedTiles = new ArrayList<NumberedTile>();
	}
	
	@Override
	/**
	 * Undoes the move
	 */
	public boolean undoMove(IMovable lb) {
		if(clearedTiles == null || clearedTiles.isEmpty()) {
			return false;
		}
		
		Board board = app.getModel().getCurrentLevel().getBoard();
		NumberedTile tile;
		for(NumberedTile nt : clearedTiles) {
			if(nt != null && nt.isActive()) {
				tile = (NumberedTile) board.getTileAtPosition(nt.getPosition());
				tile.setReleaseData(nt.getReleaseData());
				tile.addTileGroup(nt.getTileGroup());
			}
		}
		
		clearedTiles.clear();
		
		return true;
	}

	@Override
	/**
	 * Clears all of the numbers from the board.
	 */
	public boolean doMove(IMovable lb) {
		if(!isValid(lb)) {
			return false;
		}
		
//		Release lvl = (Release) app.getModel().getCurrentLevel();
		Board board = app.getModel().getCurrentLevel().getBoard();
		for(Tile tile : board.getTiles()) {
			NumberedTile nt = (NumberedTile) tile;
			if(nt.getReleaseData() != null) {
				clearedTiles.add(nt.copy());
				nt.clearTileGroup();
				nt.clearReleaseData();
			}
		}
		
		return true;
	}

	@Override
	/**
	 * Always able to clear the board of release numbers
	 */
	public boolean isValid(IMovable lb) {
		return true;
	}

}
