package diomedes.levelbuilder.control;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.Release;

/**
 * Controller to launch the Level editor with the selected level to edit
 * @author Jordan Burklund
 */
//TODO Launch the level builder with the selected
//See Delete level
public class EditLevel extends MouseAdapter {
	Application app;
	
	public EditLevel(Application app) {
		this.app = app;
	}
	
	/**
	 * Switch to the Level Editor with the selected level when the user
	 * Clicks on the button
	 */
	public void mouseClicked(MouseEvent me) {
		System.out.println("Attempting to edit a level");
		Level selectedLevel = app.getLevelSelectView().getTableSelectedLevel();
		if(selectedLevel == null) {
			//No level was selected, abort and do nothing
			return;
		}
		//Properly set up the Level Editor based on the level type
		//TODO should do differently...
		app.setEditorMenuItems(true);
		if(selectedLevel instanceof Lightning) {
			app.getEditLevelView().setRelease(false);
			app.getEditStatusView().setTextLabel("Time Allowed:");
			app.getEditStatusView().setTextField(true);
			app.getEditStatusView().setActionListener(new ChooseTimeout(app, app.getModel()));
		}
		if(selectedLevel instanceof Puzzle) {
			app.getEditLevelView().setRelease(false);
			app.getEditStatusView().setTextLabel("Number of Moves Allowed: ");
			app.getEditStatusView().setTextField(true);
			app.getEditStatusView().setActionListener(new ChooseMaxMoves(app, app.getModel()));
		}
		if(selectedLevel instanceof Release) {
			app.getEditLevelView().setRelease(true);
			app.getEditStatusView().setTextLabel(false);
			app.getEditStatusView().setTextField(false);
		}
		app.getModel().setCurrentLevel(selectedLevel);
		app.getEditLevelView().setLevelToEdit(selectedLevel);
		
		//Finally switch to the Edit panel
		app.setContentPanel(EditLevelView.NAME);
	}

}
