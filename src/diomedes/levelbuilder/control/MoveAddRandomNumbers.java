package diomedes.levelbuilder.control;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.NumberedTile;
import diomedes.shared.entity.NumberedTileGroup;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Release;
import diomedes.shared.entity.ReleaseData;
import diomedes.shared.entity.Tile;

/**
 * Move to add random numbers to the board.
 */
public class MoveAddRandomNumbers extends Move {

	Application app;
	ReleaseData[] releaseData = new ReleaseData[18];
	boolean[] usedData = new boolean[18];
	ArrayList<NumberedTile> randomTiles;
	MoveClearNumbers clear;

	/**
	 * Constructor to add random numbers to the release board.
	 * @param app
	 */
	public MoveAddRandomNumbers(Application app) {
		this.app = app;
		fillArrayWithReleaseData();
		randomTiles = new ArrayList<NumberedTile>();
	}
	
	@Override
	/**
	 * Undoes the move.
	 */
	public boolean undoMove(IMovable lb) {
		if(randomTiles == null || randomTiles.isEmpty()) {
			return false;
		}
		
		Board board = app.getModel().getCurrentLevel().getBoard();
		NumberedTile tile;
		for(NumberedTile nt : randomTiles) {
			tile = (NumberedTile) board.getTileAtPosition(nt.getPosition());
			tile.clearTileGroup();
			tile.clearReleaseData();
		}
		
		randomTiles.clear();
		for(int i = 0; i < usedData.length; i++) {
			usedData[i] = false;
		}
		
		if(!clear.undoMove(lb)) {
			return false;
		}
		app.getEditLevelView().getBoardView().repaint();
		
		return true;
	}

	@Override
	/**
	 * Realizes the moving random numbers to the board
	 */
	public boolean doMove(IMovable lb) {
		if(!isValid(lb)) {
			return false;
		}
		
		clear = new MoveClearNumbers(app);
		if(!clear.doMove(lb)) {
			return false;
		}
		
		Release lvl = (Release) app.getModel().getCurrentLevel();
		Board board = app.getModel().getCurrentLevel().getBoard();
		int col;
		int row;
		int colorNum;
		Random rand = new Random();
		NumberedTile nt;
		while(randomTiles.size() < 18) {
			col = Math.abs(rand.nextInt() % 360);
			row = Math.abs(rand.nextInt() % 360);
			ReleaseData rd = null;
			
			nt = (NumberedTile) board.getTileAtPosition(new Position(col, row));
			if(nt != null && nt.getReleaseData() == null && nt.isActive()) {
				
				while(rd == null) {
					colorNum = Math.abs(rand.nextInt() % 18);
					if(colorNum < 18 && !usedData[colorNum]) {
						rd = releaseData[colorNum];
						usedData[colorNum] = true;
					}
				}
				
				nt.setReleaseData(rd);
				NumberedTileGroup ntg = null;
				if(rd.getColor().getRed() == 255) {
					ntg = lvl.getTileGroups().get(0);
				}
				else if(rd.getColor().getGreen() == 255) {
					ntg = lvl.getTileGroups().get(1);
				}
				else {
					ntg = lvl.getTileGroups().get(2);
				}
				nt.addTileGroup(ntg);
				
				randomTiles.add(nt.copy());
			}
		}
		return true;
	}

	@Override
	/**
	 * Checks if it's valid, meaning if it has the minimum amount of tiles.
	 */
	public boolean isValid(IMovable lb) {
		Board board = app.getModel().getCurrentLevel().getBoard();
		int activeCount = 0;
		for(Tile t : board.getTiles()) {
			if(t.isActive()) {
				activeCount++;
			}
		}
		if(activeCount < 18) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Sets number 1-6 to the colors. 
	 */
	private void fillArrayWithReleaseData() {
		Color color;
		for(int i = 0; i < releaseData.length; i++) {
			if(i > 5 && i <= 11) {
				color = Color.GREEN;
				releaseData[i] = new ReleaseData((i - 6) + 1, color);
//				System.out.println("new ReleaseData(" + ((i - 6) + 1) + ", " + color.toString() + ")");
			}
			else if(i > 11) {
				color = Color.BLUE;
				releaseData[i] = new ReleaseData((i - 12) + 1, color);
//				System.out.println("new ReleaseData(" + ((i - 12) + 1) + ", " + color.toString() + ")");
			}
			else {
				color = Color.RED;
				releaseData[i] = new ReleaseData(i + 1, color);
//				System.out.println("new ReleaseData(" + (i + 1) + ", " + color.toString() + ")");
			}
			usedData[i] = false;
		}
	}

}
