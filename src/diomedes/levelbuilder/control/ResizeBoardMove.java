package diomedes.levelbuilder.control;

import javax.swing.JLabel;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Puzzle;

/**
 * Resized the board.
 * @author Kyle
 *
 */
public class ResizeBoardMove extends Move {
	
	LevelBuilderModel lb;
	EditLevelView lv;
	boolean isXDirection;
	boolean isPositive;
	Board board;
	int row; //to be able to calculate the number of tiles in the board
	int column;
	Application app;
	
	/**
	 * Constructor for ResizeBoardMove
	 * @param app Current application
	 * @param lv Current level
	 * @param isXDirection Direction for X
	 * @param isPositive Direction for Y
	 */
	public ResizeBoardMove(Application app, EditLevelView lv, boolean isXDirection, boolean isPositive) {
		this.app = app;
		this.lb = app.getModel();
		this.lv = lv;
		this.isXDirection = isXDirection;
		this.isPositive = isPositive;
		this.board = lb.getCurrentLevel().getBoard();
	}
	
	@Override
	/**
	 * Checks if you can undo the move.
	 */
	public boolean undoMove(IMovable lb) {
		if(isXDirection) {
			if(isPositive) {
				if(board.removeColumn()) {
					JLabel txt = lv.getActionToolbarView().getBoardWidthText();
					txt.setText(Integer.toString(board.getWidth())); //decrement the text field
					column = board.getWidth();
					return true;
				}
			}
			else {
				if(board.addColumn()) {
					JLabel txt = lv.getActionToolbarView().getBoardWidthText();
					txt.setText(Integer.toString(board.getWidth())); //increment the text field
					column = board.getWidth();
					return true;
				}
			}
			if(app.getModel().getCurrentLevel().getVariationName() == "Puzzle"){
				Puzzle level = (Puzzle) app.getModel().getCurrentLevel();
				level.setNumTilesLeft(row*column);
			}
		}
		else {
			if(isPositive) {
				if(board.removeRow()) {
					JLabel txt = lv.getActionToolbarView().getBoardHeightText();
					txt.setText(Integer.toString(board.getHeight())); //decrement the text field
					row = board.getHeight();
					return true;
				}
			}
			else {
				if(board.addRow()) {
					JLabel txt = lv.getActionToolbarView().getBoardHeightText();
					txt.setText(Integer.toString(board.getHeight())); //decrement the text field
					row = board.getHeight();
					return true;
				}
			}
			
			if(app.getModel().getCurrentLevel().getVariationName() == "Puzzle"){
				Puzzle level = (Puzzle) app.getModel().getCurrentLevel();
				level.setNumTilesLeft(row*column);
			}
		}
		return false;
	}

	@Override
	/**
	 * Performs the move of adding to row or column, or substracting to the row or column
	 */
	public boolean doMove(IMovable lb) {
		if(isValid(lb)) {
			if(isXDirection) {
				if(isPositive) {
					if(board.addColumn()) {
						JLabel txt = lv.getActionToolbarView().getBoardWidthText();
						txt.setText(Integer.toString(board.getWidth())); //increment the text field
						column = board.getWidth();
						return true;
					}
				}
				else {
					if(board.removeColumn()) {
						JLabel txt = lv.getActionToolbarView().getBoardWidthText();
						txt.setText(Integer.toString(board.getWidth())); //decrement the text field
						column = board.getWidth();
						return true;
					}
				}
				
				if(app.getModel().getCurrentLevel().getVariationName() == "Puzzle"){
					Puzzle level = (Puzzle) app.getModel().getCurrentLevel();
					level.setNumTilesLeft(row*column);
				}
			}
			else {
				if(isPositive) {
					if(board.addRow()) {
						JLabel txt = lv.getActionToolbarView().getBoardHeightText();
						txt.setText(Integer.toString(board.getHeight())); //increment the text field
						row = board.getHeight();
						return true;
					}
				}
				else {
					if(board.removeRow()) {
						JLabel txt = lv.getActionToolbarView().getBoardHeightText();
						txt.setText(Integer.toString(board.getHeight())); //decrement the text field
						row = board.getHeight();
						return true;
					}
				}
				
				if(app.getModel().getCurrentLevel().getVariationName() == "Puzzle"){
					Puzzle level = (Puzzle) app.getModel().getCurrentLevel();
					level.setNumTilesLeft(row*column);
				}
			}
			
			return true;
		}
		
		return false;
	}

	@Override
	/**
	 * Checks if the move is valid.
	 */
	public boolean isValid(IMovable lb) {
		int width = board.getWidth();
		int height = board.getHeight();
		
		if((width <= 6 && isXDirection && !isPositive) || (width >= 12 && isXDirection && isPositive)) {
			return false;
		}
		if((height <= 6 && !isXDirection && !isPositive) || (height >= 12 && !isXDirection && isPositive)) {
			return false;
		}
		
		return true;
	}

}
