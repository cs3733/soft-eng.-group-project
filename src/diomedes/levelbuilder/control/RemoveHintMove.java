package diomedes.levelbuilder.control;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.PlacedPiece;

/**
 * Remove a hint from the Board in Level Builder.
 * @author Aura
 * With a lot of code from @Kyle 's move class
 *
 */
public class RemoveHintMove extends Move {

	Application app;
	LevelBuilderModel lb;
	Board board;
	PlacedPiece hintPiece;

	/**
	 * Constructor for removing a hint from the board.
	 * @param app Current application
	 * @param lb Current level builder model
	 */
	public RemoveHintMove(Application app, PlacedPiece hint) {
		this.app = app;
		this.lb = app.getModel();
		this.board = lb.getCurrentLevel().getBoard();
		this.hintPiece = hint;
	}

	/**
	 * Undoes the move
	 */
	@Override
	public boolean undoMove(IMovable lb) {
		board.addHint(hintPiece);
//		app.getModel().getCurrentLevel().getBullpen().addPiece(hintPiece.getPiece());

		return true;
	}

	@Override
	/**
	 * Removes a hint from the board.
	 */
	public boolean doMove(IMovable lb) {
		if(!isValid(lb)) {
			return false;
		}

		board.removeHint(hintPiece);
//		app.getModel().getCurrentLevel().getBullpen().removePiece(hintPiece.getPiece());

		return true;
	}

	@Override
	public boolean isValid(IMovable lb) {
		if(hintPiece != null) {
			return true;
		}
		return false;
	}

}

