package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.SelectVariationView;

/**
 * Controller for creating a new level.
 *
 */
public class NewLevel implements ActionListener
{
	Application app;
	
	/**
	 * Constructor for new level
	 * @param app current application
	 */
	public NewLevel(Application app)
	{
		this.app = app;
	}
	
	@Override
	/**
	 * Sets the view for the new level
	 */
	public void actionPerformed(ActionEvent e)
	{
		app.setContentPanel(SelectVariationView.NAME);
	}

}
