package diomedes.levelbuilder.control;

import javax.swing.JTextField;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Lightning;

/**
 * 
 * @author Kenedi
 *
 */
public class SetMaxTime extends Move{
	
	int maxTime;
	JTextField text;
	/**
	 * Sets the maximum amount of time the Player has to complete the level.
	 * @param nMaxt maximum time (in seconds) the Player has
	 * @param jTextField 
	 */
	public SetMaxTime(int nMaxt, JTextField text){
		this.maxTime = nMaxt;
		this.text = text;
	}

	/**
	 * Resets the time to the previous max time.
	 */
	@Override
	public boolean undoMove(IMovable lb) {
		Level level = (Lightning)((LevelBuilderModel) lb).getCurrentLevel();
		if(maxTime != -1)
		{
			((Lightning) level).setMaxTime(maxTime);
			String oldT = maxTime + "";
			text.setText(oldT);
		}
		else
		{
			text.setText(null);
		}
		
		return true;
	}

	/**
	 * Sets the maximum number of time(in seconds).
	 */
	@Override
	public boolean doMove(IMovable lb) {
		
		Level level = (Lightning)((LevelBuilderModel) lb).getCurrentLevel();
		
		if(maxTime != -1)
		{
			((Lightning) level).setMaxTime(maxTime);
			text.setText(maxTime+"");
		}
		else
		{
			text.setText(null);
		}
		
		return true;
	}

	/**
	 * isValid is always true.
	 */
	@Override
	public boolean isValid(IMovable lb) {
		return true;
	}

}
