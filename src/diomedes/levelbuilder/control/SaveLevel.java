package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.LevelDirectorySorter;
import diomedes.shared.entity.LevelSaver;

/**
 * Controller to save the current level when a button is pressed
 * @author Jordan Burklund
 */
public class SaveLevel implements ActionListener {
	LevelBuilderModel lbModel;
	
	/**
	 * Constructor
	 * @param lbModel Top-Level model for the application
	 */
	public SaveLevel(LevelBuilderModel lbModel) {
		this.lbModel = lbModel;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Level currentLevel = lbModel.getCurrentLevel();
		currentLevel.recalculate();
		String dir = LevelDirectorySorter.getDirectoryForLevel(currentLevel);
		
		//Ensure that the first level is always unlocked
		if (currentLevel.getOrder() == 1) {
			currentLevel.unlockLevel();
		}
		
		//Save the level!
		Level[] levelsToSave = {currentLevel};
		LevelSaver saver = new LevelSaver(dir, levelsToSave);
		
		//Fail gracefully if the level could not be saved by not saving the level
		try {
			saver.saveLevels();
		} catch (IOException e) {
			System.err.println("Couldn't save level to: "+dir);
			e.printStackTrace();
		}
		//Update the model if the level isn't already in there
		if(!lbModel.getLevels().contains(currentLevel)) {
			lbModel.addLevel(currentLevel);
		}
	}

}
