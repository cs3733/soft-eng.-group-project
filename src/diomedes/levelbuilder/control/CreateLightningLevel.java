package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.Piece;

/**
 * Shows the create ligntning layout.
 * @author Kenedi
 *
 */
public class CreateLightningLevel implements ActionListener
{
	Application app;
	
	/**
	 * Constructor for creating lightning level
	 * @param app current application
	 */
	public CreateLightningLevel(Application app)
	{
		this.app = app;
	}

	@Override  
	/**
	 * Sets the view for the lightning level
	 */
	public void actionPerformed(ActionEvent e)
	{
		app.setContentPanel(EditLevelView.NAME);
		app.setEditorMenuItems(true);
		app.getEditLevelView().setRelease(false);
		app.getEditStatusView().setTextLabel("Time Allowed:");
		app.getEditStatusView().setTextField(true);
		app.getEditStatusView().setActionListener(new ChooseTimeout(app, app.getModel()));
		
		
		//Create new Lightning Level
		ArrayList<Piece> pieces = new ArrayList<Piece>();
		Bullpen bullpen = new Bullpen(pieces);
		Board board = new Board();
		Lightning level = new Lightning(board, bullpen,  app.getModel().getNumLightning()+1);
		app.getModel().setCurrentLevel(level);
		app.getEditLevelView().setLevelToEdit(level);
		Move m = new SetMaxTime(-1, app.getEditLevelView().getEditStatusView().getTextField());
		if(m.doMove(app.getModel()))
		{
			app.getModel().pushMove(m);
		}
	}

}
