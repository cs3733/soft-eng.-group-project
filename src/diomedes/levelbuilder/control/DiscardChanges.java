package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.popup.DiscardChangesAssuranceView;

/**
 * Discard changes controller
 * @author Kenedi
 *
 */
public class DiscardChanges implements ActionListener
{
	Application app;
	/**
	 * Constructor for discarding changes
	 * @param app current application
	 */
	public DiscardChanges(Application app)
	{
		this.app = app;
	}
	
	@Override
	/**
	 * Shows an assurance that they want to discharge 
	 */
	public void actionPerformed(ActionEvent e)
	{
		DiscardChangesAssuranceView sa = new DiscardChangesAssuranceView(app);
	}

}
