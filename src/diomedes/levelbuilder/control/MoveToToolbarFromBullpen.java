package diomedes.levelbuilder.control;

import java.security.InvalidParameterException;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Piece;

/**
 * Class to move a piece from toolbar from the bullpen
 */
public class MoveToToolbarFromBullpen extends Move {

	Application app;
	Piece piece;
	Bullpen bullpen;
	
	/**
	 * Constructor for the move
	 * @param app current application
	 * @param piece the current piece being moved
	 * @param bullpen the bullpen
	 */
	public MoveToToolbarFromBullpen(Application app, Piece piece, Bullpen bullpen) {
		//Can't do anything if the bullpen isn't set
		if(bullpen == null) {
			throw new InvalidParameterException("bullpen is not set");
		}
		//Can't do anything if the piece to move isn't set
		if(piece == null) {
			throw new InvalidParameterException("Piece to move not set");
		}
		
		this.app = app;
		this.bullpen = bullpen;
		this.piece = piece;
	}

	@Override
	/**
	 * Undo the move
	 */
	public boolean undoMove(IMovable lb) {
		//Add the piece to the bullpen
		bullpen.addPiece(piece);

		return true;
	}

	@Override
	/**
	 * Removes the piece from the bullpen
	 */
	public boolean doMove(IMovable lb) {
		//Check if the move is valid
		if(!isValid(lb)) {
			return false;
		}
		
		bullpen.removePiece(piece);
		bullpen.clearSelectedPiece();
		
		return true;
	}

	@Override
	public boolean isValid(IMovable lb) {
		//is the piece in the move a selected piece

		return true;
	}

}
