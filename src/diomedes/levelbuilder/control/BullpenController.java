package diomedes.levelbuilder.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;

/**
 * Controller for the bullpen.
 * @author Kyle Carrero
 * @author Myles Spencer
 */
public class BullpenController implements MouseMotionListener, MouseListener {
	
	Application app;
	
	/**
	 * Constructor for the bullpen controller.
	 * @param app
	 */
	public BullpenController(Application app) {
		this.app = app;
	}

	@Override
	/**
	 * Selects the piece from the bullpen, or adds a piece to the bullpen
	 */
	public void mouseClicked(MouseEvent me) {
		int index = (me.getX() / app.getEditLevelView().getBullpenView().getPieceViews().getHeight());
		if(index < app.getModel().getCurrentLevel().getBullpen().getPieces().size())
		{
			if(app.isPieceSelected()) {
				if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
					Piece s = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
					app.getModel().getCurrentLevel().getBullpen().clearSelectedPiece();
					Piece p = app.getModel().getCurrentLevel().getBullpen().getPieces().get(index);
					if(s != p)
					{
						app.getModel().getCurrentLevel().getBullpen().setSelectedPiece(p);
					}
				}
				else {
					if(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() != null) {
						PlacedPiece s = app.getModel().getCurrentLevel().getBoard().getSelectedPiece();
						MoveToBullpenFromBoard mptb = new MoveToBullpenFromBoard(s, app.getModel().getCurrentLevel().getBullpen());
						if(mptb.doMove(app.getModel())) {
							app.getModel().pushMove(mptb);
							app.getEditLevelView().getBullpenView().update();
							app.getEditLevelView().getBoardView().repaint();
						}
					}
					else {
						Piece s = app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece();
						MoveToBullpenFromToolbar mpft = new MoveToBullpenFromToolbar(app, s, app.getModel().getCurrentLevel().getBullpen());
						if(mpft.doMove(app.getModel())) {
							app.getModel().pushMove(mpft);
							app.getEditLevelView().getBullpenView().update();
							app.getEditLevelView().getPieceToolbarView().repaint();
						}
					}
				}
			}
			else {
				Piece p = app.getModel().getCurrentLevel().getBullpen().getPieces().get(index);
				app.getModel().getCurrentLevel().getBullpen().setSelectedPiece(p);
			}
			app.getEditLevelView().getBullpenView().update();
		}
		else {
			if(app.isPieceSelected() && app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() == null) {
				if(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() != null) {
					PlacedPiece s = app.getModel().getCurrentLevel().getBoard().getSelectedPiece();
					MoveToBullpenFromBoard mptb = new MoveToBullpenFromBoard(s, app.getModel().getCurrentLevel().getBullpen());
					if(mptb.doMove(app.getModel())) {
						app.getModel().pushMove(mptb);
						app.getEditLevelView().getBullpenView().update();
						app.getEditLevelView().getBoardView().repaint();
					}
				}
				else {
					Piece s = app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece();
					MoveToBullpenFromToolbar mpft = new MoveToBullpenFromToolbar(app, s, app.getModel().getCurrentLevel().getBullpen());
					if(mpft.doMove(app.getModel())) {
						app.getModel().pushMove(mpft);
						app.getEditLevelView().getBullpenView().update();
						app.getEditLevelView().getPieceToolbarView().repaint();
					}
				}
			}
		}
		app.getEditLevelView().getBullpenView().getPieceViews().updateSize();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
