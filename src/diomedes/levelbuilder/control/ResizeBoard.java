package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;

/**
 * Controller for resizing the board.
 * @author Kyle
 *
 */
public class ResizeBoard implements ActionListener {

	Application app;
	LevelBuilderModel lb;
	EditLevelView lv;
	boolean isXDirection;
	boolean isPositive;
	
	/**
	 * Resizes the board in the level editor
	 * @param app The top-level application for LevelBuilder
	 * @param lb The top-level model for LevelBuilder
	 * @param isXDirection Whether or not resizing the X or Y dimensions of the board (true for X, false for Y)
	 * @param isPositive Whether or not it is a change in the positive or negative direction (true for positive, false for negative)
	 */
	public ResizeBoard(Application app, EditLevelView lv, boolean isXDirection, boolean isPositive) {
		this.app = app;
		this.lb = app.getModel();
		this.lv = lv;
		this.isXDirection = isXDirection;
		this.isPositive = isPositive;
	}

	@Override
	/**
	 * Performs the Resize Board Move when clicked.
	 */
	public void actionPerformed(ActionEvent e) {
		ResizeBoardMove rbm = new ResizeBoardMove(app, lv, isXDirection, isPositive);
		
		if(rbm.doMove(lb)) {
			//push move onto stack
			lb.pushMove(rbm);
			
			lv.getBoardView().repaint();
		}
	}
	
}
