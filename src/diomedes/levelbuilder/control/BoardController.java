package diomedes.levelbuilder.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.BoardView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.ReleaseData;
import diomedes.shared.entity.Tile;

public class BoardController implements MouseMotionListener, MouseListener
{
	Application app;

	/**
	 * 
	 * @param app The Application
	 */
	public BoardController(Application app)
	{
		this.app = app;
	}

	@Override
	public void mouseClicked(MouseEvent me)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent me)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent me)
	{
		app.getEditLevelView().getBoardView().setHoverPosition(-1000, -1000);
		app.getEditLevelView().getBoardView().repaint();
	}

	@Override
	public void mousePressed(MouseEvent me)
	{		
		BoardView bv = app.getEditLevelView().getBoardView();
		Board b = app.getModel().getCurrentLevel().getBoard();
		int col = bv.getColFromX(me.getX());
		int row = bv.getRowFromY(me.getY());
		if(app.getEditLevelView().getActionToolbarView().getAddNumberButton().isSelected()) {
			ReleaseData rd = app.getEditLevelView().getActionToolbarView().getReleaseDataFromRadioButtons();
			if(rd != null) {
				MoveAddNumberToBoard mantb = new MoveAddNumberToBoard(app, rd, me);
				if(mantb.doMove(app.getModel())) {
					app.getModel().pushMove(mantb);
				}
				bv.repaint();
			}
		}
		else if(app.getEditLevelView().getActionToolbarView().getRemoveHintsButton().isSelected())
		{
			for(PlacedPiece pp: b.getHints())
			{
				Position rel = pp.getPosition();
				for(Position pos: pp.getPiece().getSquares())
				{
					if(pos.derelatize(rel).getCol() == col && pos.derelatize(rel).getRow() == row)
					{
						Move m = new RemoveHintMove(app, pp);
						if(m.doMove(app.getModel()))
						{
							app.getModel().pushMove(m);
							bv.repaint();
						}
					}
				}
			}
		}
		else if(app.isPieceSelected() && b.getSelectedPiece() == null)
		{
			Piece p = null;
			if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null)
			{
				p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece().copy();
				app.getModel().getCurrentLevel().getBullpen().removePiece(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece());
			}
			else if(app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null)
			{
				p = app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece().copy();
			}

			if(p != null)
			{
				//Is it coming from the bullpen
				if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null)
				{
					Move m = new MovePieceToBoard(new Position(col, row), p);
					if(m.doMove(app.getModel()))
					{
						app.deselectSelectedPiece();
						bv.repaint();
						app.getEditLevelView().getPieceToolbarView().update();
						app.getEditLevelView().getBullpenView().update();
						app.getModel().pushMove(m);
					}
				}

				//otherwise it must be coming from the Action Toolbar
				
				else
				{
					Move m = new MovePieceFromPieceToolbar(new Position(col, row), p.copy(), app.getEditLevelView().getPieceToolbarView().getToolbar());
					if(m.doMove(app.getModel()))
					{
						app.deselectSelectedPiece();
						bv.repaint();
						app.getEditLevelView().getPieceToolbarView().update();
						app.getEditLevelView().getBullpenView().update();
						app.getModel().pushMove(m);
					}
				}

			}
		}
		else if(b.getSelectedPiece() != null)
		{
			Move m = new MovePieceInBoard(new Position(col, row), b.getSelectedPiece());
			if(m.doMove(app.getModel()))
			{
				app.deselectSelectedPiece();
				bv.repaint();
				app.getModel().pushMove(m);
			}
		}
		else
		{
			Position pos = new Position(col, row);
			if(!b.selectPiece(pos)) {
				Tile t = b.getTileAtPosition(new Position(col, row));
				if(t != null) {
					if(t.isActive()) {
						t.deactivateTile();
					}
					else {
						t.activateTile();
					}
				}
				bv.repaint();
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseDragged(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
	}

	/**
	 * Paints the selected piece on the board at the mouse position
	 * @param MouseEvent The mouse event that is automatically given when this method is called
	 */
	@Override
	public void mouseMoved(MouseEvent me)
	{
		app.getEditLevelView().getBoardView().setHoverPosition(me.getX(), me.getY());
		app.getEditLevelView().getBoardView().repaint();
	}
}
