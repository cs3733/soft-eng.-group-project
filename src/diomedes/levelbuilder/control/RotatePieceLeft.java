package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Piece;

/**
 * Controller for rotating the selected Piece 90 degrees to the left.
 * @author Kyle Carrero
 *
 */
public class RotatePieceLeft implements ActionListener {

	Application app;
	LevelBuilderModel lb;
	/**
	 * Constructor for rotating piece to the left.
	 * @param app current application
	 * @param lb current model
	 */
	public RotatePieceLeft(Application app, LevelBuilderModel lb) {
		this.app = app;
		this.lb = lb;
	}

	@Override
	/**
	 * Once clicked, we turn the piece to the left.
	 */
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
			Piece p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
			p.rotateLeft();
			app.getEditLevelView().repaint();
		}
		else if(app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null) {
			Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece();
			p.rotateLeft();
			app.getEditLevelView().repaint();
		}
	}
}