package diomedes.levelbuilder.control;

import java.security.InvalidParameterException;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Piece;

/**
 * Class that handles moving a piece from the piece toolbar to the bullpen.
 * Has doMove, undoMove and isValid methods 
 * @author Aura
 *
 */
public class MoveToBullpenFromToolbar extends Move {

	Application app;
	Piece piece;
	Bullpen bullpen;
	
	/**
	 * constructor specifying the piece to move an the bullpen
	 * @param app the current application
	 * @param piece the piece being moved
	 * @param bullpen the bullpen to be moved to
	 */
	public MoveToBullpenFromToolbar(Application app, Piece piece, Bullpen bullpen) {
		//Can't do anything if the bullpen isn't set
		if(bullpen == null) {
			throw new InvalidParameterException("bullpen is not set");
		}
		//Can't do anything if the piece to move isn't set
		if(piece == null) {
			throw new InvalidParameterException("Piece to move not set");
		}
		
		this.app = app;
		this.piece = piece;
		this.bullpen = bullpen;
	}

	@Override
	/**
	 * undoes the move
	 */
	public boolean undoMove(IMovable lb) {
		//Add the piece to the bullpen
		if(bullpen.removePiece(piece)) {
			bullpen.clearSelectedPiece();
			app.getEditLevelView().getPieceToolbarView().getToolbar().clearSelectedPiece();
			
			return true;
		}

		return false;
	}

	@Override
	/**
	 * Moves the piece to the bullpen
	 */
	public boolean doMove(IMovable lb) {
		//Check if the move is valid
		if(!isValid(lb)) {
			return false;
		}
		
		//Add the piece to the bullpen
		bullpen.addPiece(piece.copy());
		bullpen.clearSelectedPiece();
		app.getEditLevelView().getPieceToolbarView().getToolbar().clearSelectedPiece();
		
		return true;
	}

	@Override
	/**
	 * Checks if there's a selected piece
	 */
	public boolean isValid(IMovable lb) {
		//is the piece in the move a selected piece
		if(app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != piece) {
			return false;
		}
		
		return true;
	}

}
