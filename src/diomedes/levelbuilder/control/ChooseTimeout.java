package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;

/**
 * Controller for choosing the maximum amount of time for a lightning Level.
 * @author Kenedi
 *
 */
public class ChooseTimeout implements ActionListener{

	Application app; 
	LevelBuilderModel lb;
	String oldTime;
	
	public ChooseTimeout(Application app, LevelBuilderModel lb){
		this.app = app;
		this.lb = lb;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int maxTime;
		if(!app.getEditStatusView().getTextField().getText().isEmpty()){
			maxTime = Integer.parseInt(app.getEditStatusView().getTextField().getText());
		}
		else{
			maxTime = 0;
		}
		Move m = new SetMaxTime(maxTime, app.getEditLevelView().getEditStatusView().getTextField());
		if(m.doMove(lb)){
				app.getEditStatusView().setOldStatus(maxTime+"");
				app.getEditStatusView().repaint();
				lb.pushMove(m);

			}
		}
	
}
