package diomedes.levelbuilder.control;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.NumberedTile;
import diomedes.shared.entity.NumberedTileGroup;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Release;
import diomedes.shared.entity.ReleaseData;
import diomedes.shared.entity.Tile;

/**
 * Adds a number to the board.
 * @author Kyle Carrero */
public class MoveAddNumberToBoard extends Move {

	Application app;
	ReleaseData rd;
	MouseEvent me;
	int row;
	int col;

	/**
	 * Constructor for adding a number to the board
	 * @param app constant application
	 * @param rd Release data
	 * @param me
	 */
	public MoveAddNumberToBoard(Application app, ReleaseData rd, MouseEvent me) {
		this.app = app;
		this.rd = rd;
		this.me = me;
		this.col = app.getEditLevelView().getBoardView().getColFromX(me.getX());
		this.row = app.getEditLevelView().getBoardView().getRowFromY(me.getY());
	}

	@Override
	/**
	 * Gets rid of the move added to the board recently.
	 */
	public boolean undoMove(IMovable lb) {
		if(!isValid(lb)) {
			return false;
		}
		Position pos = new Position(col, row);
		if(!app.getModel().getCurrentLevel().getBoard().selectPiece(pos)) {
			Tile t = app.getModel().getCurrentLevel().getBoard().getTileAtPosition(new Position(col, row));
			if(t != null) {
				if(app.getModel().getCurrentLevel() instanceof Release) {
					Release lvl = (Release) app.getModel().getCurrentLevel();
					ArrayList<NumberedTileGroup> tileGroups = lvl.getTileGroups();
					NumberedTileGroup ntg;
					//					System.out.println("ReleaseData color is: R=" + rd.getColor().getRed() + " G=" + rd.getColor().getGreen() + " B=" + rd.getColor().getBlue());
					if(rd.isRed()) {
						ntg = tileGroups.get(0);
						//						System.out.println("Going into RED group");
					}
					else if(rd.isGreen()) {
						ntg = tileGroups.get(1);
						//						System.out.println("Going into GREEN group");
					}
					else if(rd.isBlue()){
						ntg = tileGroups.get(2);
						//						System.out.println("Going into BLUE group");
					}
					else {
						throw new IllegalArgumentException("There can be no other color groups than RED, GREEN, or BLUE");
					}

					if(SwingUtilities.isRightMouseButton(me)) {
						((NumberedTile) t).addTileGroup(ntg);
						((NumberedTile) t).setReleaseData(rd);
						ntg.addNumberedTile((NumberedTile) t);
					}
					else if(SwingUtilities.isLeftMouseButton(me)) {
						ntg.removeNumberedTile(t.getPosition());
						((NumberedTile) t).clearTileGroup();
						((NumberedTile) t).clearReleaseData();
					}
				}
			}
			return true;
		}
		else {
			app.getModel().getCurrentLevel().getBoard().clearSelectedPiece();
			return false;
		}
	}

	@Override
	/**
	 * Adds a number to the board for Release.
	 */
	public boolean doMove(IMovable lb) {
		Position pos = new Position(col, row);
		if(!app.getModel().getCurrentLevel().getBoard().selectPiece(pos)) {
			Tile t = app.getModel().getCurrentLevel().getBoard().getTileAtPosition(new Position(col, row));
			if(t != null) {
				if(app.getModel().getCurrentLevel() instanceof Release) {
					Release lvl = (Release) app.getModel().getCurrentLevel();
					ArrayList<NumberedTileGroup> tileGroups = lvl.getTileGroups();
					NumberedTileGroup ntg;
					//					System.out.println("ReleaseData color is: R=" + rd.getColor().getRed() + " G=" + rd.getColor().getGreen() + " B=" + rd.getColor().getBlue());
					if(rd.isRed()) {
						ntg = tileGroups.get(0);
						//						System.out.println("Going into RED group");
					}
					else if(rd.isGreen()) {
						ntg = tileGroups.get(1);
						//						System.out.println("Going into GREEN group");
					}
					else if(rd.isBlue()){
						ntg = tileGroups.get(2);
						//						System.out.println("Going into BLUE group");
					}
					else {
						throw new IllegalArgumentException("There can be no other color groups than RED, GREEN, or BLUE");
					}

					if(SwingUtilities.isLeftMouseButton(me)) {
						((NumberedTile) t).clearTileGroup();
						((NumberedTile) t).clearReleaseData();

						((NumberedTile) t).setReleaseData(rd);
						((NumberedTile) t).addTileGroup(ntg);
						//ntg.addNumberedTile((NumberedTile) t);
					}
					else if(SwingUtilities.isRightMouseButton(me)) {
						//ntg.removeNumberedTile(t.getPosition());
						((NumberedTile) t).clearTileGroup();
						((NumberedTile) t).clearReleaseData();
					}
				}
			}
			return true;
		}
		else {
			app.getModel().getCurrentLevel().getBoard().clearSelectedPiece();
			return false;
		}
	}

	@Override
	/**
	 * Checks that a piece is not selected.
	 */
	public boolean isValid(IMovable lb) {
		Position pos = new Position(col, row);
		if(!app.getModel().getCurrentLevel().getBoard().selectPiece(pos)) {
			return true;
		}
		app.getModel().getCurrentLevel().getBoard().clearSelectedPiece();
		return false;
	}

}
