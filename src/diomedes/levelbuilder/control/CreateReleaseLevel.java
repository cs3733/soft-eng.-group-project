package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.NumberedTileGroup;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Release;

/**
 * Sets the correct view for creating a release level.
 *
 */
public class CreateReleaseLevel implements ActionListener
{
	Application app;
	/**
	 * Constructor for the release level.
	 * @param app
	 */
	public CreateReleaseLevel(Application app)
	{
		this.app = app;
	}

	@Override
	/**
	 * Shows the correct view for creating a release level
	 */
	public void actionPerformed(ActionEvent e)
	{
		app.setContentPanel(EditLevelView.NAME);
		app.setEditorMenuItems(true);
		app.getEditStatusView().setTextLabel(false);
		app.getEditStatusView().setTextField(false);
		
		
		
		//Create new Release Level
		ArrayList<Piece> pieces = new ArrayList<Piece>();
		Bullpen bullpen = new Bullpen(pieces);
		Board board = new Board();
		ArrayList<NumberedTileGroup> numbers = new ArrayList<NumberedTileGroup>();
		numbers.add(new NumberedTileGroup());
		numbers.add(new NumberedTileGroup());
		numbers.add(new NumberedTileGroup());
		Release level = new Release(board, bullpen,  app.getModel().getNumRelease()+1, numbers);
		app.getModel().setCurrentLevel(level);
		app.getEditLevelView().setLevelToEdit(level);
	}

}
