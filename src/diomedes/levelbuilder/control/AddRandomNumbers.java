package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Release;

public class AddRandomNumbers implements ActionListener {

	Application app;
	
	public AddRandomNumbers(Application app) {
		this.app = app;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel() instanceof Release) {
			MoveAddRandomNumbers marn = new MoveAddRandomNumbers(app);
			if(marn.doMove(app.getModel())) {
				app.getModel().pushMove(marn);
				app.getEditLevelView().getBoardView().repaint();
			}
		}
	}
	
}
