package diomedes.levelbuilder.control;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import diomedes.levelbuilder.view.Application;

/**
 * A scrollbar listener to help the rendering of pieces in the bullpen when scrolling through.
 * @author Kyle Carrero
 *
 */
public class PieceToolbarScrollbarListener implements AdjustmentListener {

	/** Top-Level Application for LevelBuilder **/
	Application app;
	
	/** Constructor **/
	public PieceToolbarScrollbarListener(Application app) {
		this.app = app;
	}
	
	@Override
	/** Refreshes the bullpenview upon change event **/
	public void adjustmentValueChanged(AdjustmentEvent ae) {
		app.getEditLevelView().getPieceToolbarView().update();
	}

}
