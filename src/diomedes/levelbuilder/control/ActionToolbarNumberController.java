package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import diomedes.levelbuilder.view.Application;

/**
 * controller that manages the buttons for adding a number in release
 * @author Aura
 *
 */
public class ActionToolbarNumberController implements ActionListener {
	
	/**
	 * current application
	 */
	Application app;
	/**
	 * current button
	 */
	JRadioButton btn;
	
	/**
	 * ActionToolbarNumberController level builder needs to add a number
	 * @param app current application
	 * @param btn current button
	 */
	public ActionToolbarNumberController(Application app, JRadioButton btn) {
		this.app = app;
		this.btn = btn;
	}
	
	@Override
	/**
	 * Selects a number
	 */
	public void actionPerformed(ActionEvent arg0) {
		Integer i = null;
		ButtonGroup bg = app.getEditLevelView().getActionToolbarView().getNumberButtonGroup();
		if(bg.isSelected(this.btn.getModel())) {
			switch(this.btn.getText()) {
			case "1": i = 1;
				break;
			case "2": i = 2;
				break;
			case "3": i = 3;
				break;
			case "4": i = 4;
				break;
			case "5": i = 5;
				break;
			case "6": i = 6;
				break;
			}
			
			app.getEditLevelView().getActionToolbarView().setReleaseNumber(i);
		}
	}

}