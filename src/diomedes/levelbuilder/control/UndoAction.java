package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;

/**
 * The controller to handle undo moves in LevelBuilder.
 * @author Kyle Carrero
 *
 */
public class UndoAction implements ActionListener {
	
	Application app;
	LevelBuilderModel lb;
	
	public UndoAction(Application app, LevelBuilderModel lb) {
		this.app = app;
		this.lb = lb;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Move m = lb.popMove();
		if(m != null) {
			if(m.undoMove(lb)) {
				lb.pushRedoMove(m);
				app.repaint();
			}
			app.repaint();
		}
	}
	
}
