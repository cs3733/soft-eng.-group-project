package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

import javax.swing.JDialog;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.MainMenuView;

/**
 * Discards the changes. 
 * @author Kenedi
 *
 */
public class DiscardChangesController implements ActionListener{

	Application app;
	JDialog w;
	Stack<Move> undoStack = new Stack<Move>();
	
	/**
	 * Constructor for discarding the changes in the board
	 * @param app current appliation
	 * @param w the dialog 
	 */
	public DiscardChangesController(Application app, JDialog w)
	{
		this.app = app;
		this.w = w;
	}
	@Override
	/**
	 * Discards the changes made.
	 */
	public void actionPerformed(ActionEvent arg0) {
		undoStack = app.getModel().getUndoStack();
		while(!undoStack.isEmpty()){
			undoStack.pop().undoMove(app.getModel());
		}
		app.setContentPanel(MainMenuView.NAME);
		app.setEditorMenuItems(false);
		w.dispose();
	}

}
