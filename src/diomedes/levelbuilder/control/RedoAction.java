package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;

/**
 * The controller to handle redo moves in LevelBuilder.
 * @author Kyle Carrero
 *
 */
public class RedoAction implements ActionListener {
	
	Application app;
	LevelBuilderModel lb;
	
	/**
	 * Takes constructor to redo an action.
	 * @param app takes the current application
	 * @param lb current level builder model.
	 */
	public RedoAction(Application app, LevelBuilderModel lb) {
		this.app = app;
		this.lb = lb;
	}

	@Override
	/**
	 * Performs an undo once clicked.
	 */
	public void actionPerformed(ActionEvent arg0) {
		Move m = lb.popRedoMove();
		if(m != null) {
			if(m.doMove(lb)) {
				lb.pushMove(m);
				app.repaint();
			}
		}
	}
	
}
