package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import diomedes.levelbuilder.view.Application;
import diomedes.levelbuilder.view.panel.EditLevelView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Puzzle;

/**
 * Creates a puzzle level.
 * @author Kenedi
 *
 */
public class CreatePuzzleLevel implements ActionListener
{
	Application app;
	/**
	 * Constructor for creating a puzzle level.
	 * @param app current application
	 */
	public CreatePuzzleLevel(Application app)
	{
		this.app = app;
	}

	@Override
	/**
	 * Sets the view when the create puzzle level is called.
	 */
	public void actionPerformed(ActionEvent e)
	{
		app.setContentPanel(EditLevelView.NAME);
		app.setEditorMenuItems(true);
		app.getEditLevelView().setRelease(false);
		app.getEditStatusView().setTextLabel("Number of Moves Allowed:");
		app.getEditStatusView().setTextField(true);
		app.getEditStatusView().setActionListener(new ChooseMaxMoves(app, app.getModel()));
		
		
		//Create new Lightning Level
		ArrayList<Piece> pieces = new ArrayList<Piece>();
		Bullpen bullpen = new Bullpen(pieces);
		Board board = new Board();
		Puzzle level = new Puzzle(board, bullpen, app.getModel().getNumPuzzle()+1, 10);
		app.getModel().setCurrentLevel(level);
		app.getEditLevelView().setLevelToEdit(level);
		Move m = new SetNumMaxMoves(-1, app.getEditLevelView().getEditStatusView().getTextField());
		if(m.doMove(app.getModel()))
		{
			app.getModel().pushMove(m);
		}
	}

}
