package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Position;
/**
 * Controller to remove a hint from the board.
 * @author Aura
 *
 */
public class RemoveHint implements ActionListener
{
	Application app;
	LevelBuilderModel lb;
	
	/**
	 * Default constructor to remove a hint from the board.
	 * @param app current application
	 * @param lb current model
	 */
	public RemoveHint(Application app, LevelBuilderModel lb)
	{
		this.app = app;
		this.lb = lb;
	}
	
	@Override
	/**
	 * Removes hint from the board once button is pressed.
	 */
	public void actionPerformed(ActionEvent arg0)
	{
		if(app.getEditLevelView().getActionToolbarView().getRemoveHintsButton().isSelected()) {
			app.getModel().getCurrentLevel().getBoard().clearSelectedPiece();
			app.getModel().getCurrentLevel().getBullpen().clearSelectedPiece();
			app.getEditLevelView().getPieceToolbarView().getToolbar().clearSelectedPiece();
		}
	}
	
}
