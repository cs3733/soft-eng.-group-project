package diomedes.levelbuilder.control;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidParameterException;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.LevelDirectorySorter;
import diomedes.shared.entity.LevelSaver;

/**
 * Controller to delete a selected level in the LevelSelectView of Level Builder
 * @author Jordan Burklund
 */
public class DeleteLevel extends MouseAdapter {
	/** Reference to the top-level model **/
	LevelBuilderModel lbModel;
	Application app;
	
	/**
	 * Constructs a DeleteLevel controller
	 * @param lbModel Top-Level model for the level builder
	 * @param app Top-Level application for the level builder
	 * @throws InvalidParameterException if the levelBuilder model is null
	 */
	public DeleteLevel(LevelBuilderModel lbModel, Application app) {
		if(lbModel == null) {
			throw new InvalidParameterException("Level Builder model cannot be null");
		}
		this.lbModel = lbModel;
		this.app = app;
	}
	
	/**
	 * Delete the selected level when the user clicks on the button that this
	 * controller is subscribed to
	 */
	public void mouseClicked(MouseEvent me) {
		System.out.println("Attempting to delete the selected level");
		Level selectedLevel = app.getLevelSelectView().getTableSelectedLevel();
		if(selectedLevel == null) {
			System.out.println("No level selected");
			//No level was selected, do nothing
			return;
		}
		//Find the correct directory and filename based on the level
		String filename = LevelSaver.levelFilename(selectedLevel);
		String directory = LevelDirectorySorter.getDirectoryForLevel(selectedLevel);
		File fileToDelete = new File(directory+filename);
		try {
			Files.deleteIfExists(fileToDelete.toPath());
		} catch(IOException e) {
			//Could not delete the file for some reason, just ignore it
			System.err.println("Could not delete: "+fileToDelete.getPath());
		}
		//Remove the level from the model, and update the table
		lbModel.removeLevel(selectedLevel);
		app.getLevelSelectView().setUpTable();
	}
}
