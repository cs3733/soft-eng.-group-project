package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;

/**
 * Controller for setting the maximum number of moves for a puzzle level.
 * @author Kenedi
 *
 */
public class ChooseMaxMoves implements ActionListener{

	//EditStatusView statusView;
 
	Application app;
	LevelBuilderModel lb;
	String oldMove;
	/**
	 * Chooses the max moves 
	 * @param app current application
	 * @param lb current model
	 */
	public ChooseMaxMoves(Application app, LevelBuilderModel lb){
		this.app = app;
		this.lb = lb;
	}
	
	@Override
	/**
	 * Gets the new max moves and sets it. Does the move.
	 */
	public void actionPerformed(ActionEvent e) {
		int maxMoves;
		if(!app.getEditStatusView().getTextField().getText().isEmpty()){
			maxMoves = Integer.parseInt(app.getEditStatusView().getTextField().getText());
		}
		else{
			maxMoves = 0;
		}
		Move m = new SetNumMaxMoves(maxMoves, app.getEditLevelView().getEditStatusView().getTextField());
		if(m.doMove(lb)){
				app.getEditStatusView().setOldStatus(maxMoves+"");
				app.getEditStatusView().repaint();
				lb.pushMove(m);

			}
	}

}
