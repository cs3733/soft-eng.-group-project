package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Board;

public class AddHint implements ActionListener
{
	Application app;
	LevelBuilderModel lb;
	
	public AddHint(Application app, LevelBuilderModel lb)
	{
		this.app = app;
		this.lb = lb;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		Board b = app.getModel().getCurrentLevel().getBoard();
		
		if(app.isPieceSelected() && b.getSelectedPiece() != null) {
			Move m = new AddHintMove(app, lb);
			if(m.doMove(app.getModel()))
			{
				app.deselectSelectedPiece();
				app.repaint();
				app.getModel().pushMove(m);
			}
		}
	}
	
}
