package diomedes.levelbuilder.control;

import javax.swing.JTextField;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.Puzzle;

/**
 * Sets the number of maximum moves for the Puzzle level.
 * @author Kenedi
 *
 */
public class SetNumMaxMoves extends Move{
	int maxMoves;
	JTextField text;
	/**
	 * Sets the maximum number of moves the player has to complete the level.
	 * @param maxMove maximum number of moves the user has
	 */
	public SetNumMaxMoves(int maxMove, JTextField text){
		this.maxMoves = maxMove;
		this.text = text;
	}
	
	/**
	 * Sets the maximum number of moves back to the previous number.
	 */
	@Override
	public boolean undoMove(IMovable lb) {
		Level level = (Puzzle)((LevelBuilderModel) lb).getCurrentLevel();
		if(maxMoves != -1)
		{
			((Puzzle) level).setNumMaxMoves(maxMoves);
			String oldM = maxMoves + "";
			text.setText(oldM);
		}
		else
		{
			text.setText(null);
		}
		return true;
	}
	
	/**
	 * Sets the maximum number of moves the player has to complete the level.
	 */
	@Override
	public boolean doMove(IMovable lb) {
		Level level = (Puzzle)((LevelBuilderModel) lb).getCurrentLevel();
		
		if(maxMoves != -1)
		{
			((Puzzle) level).setNumMaxMoves(maxMoves);
			text.setText(maxMoves+"");
		}
		else
		{
			text.setText(null);
		}
		
		return true;
	}
	
	/**
	 * Move is always valid.
	 */
	@Override
	public boolean isValid(IMovable lb) {
		return true;
	}
}
