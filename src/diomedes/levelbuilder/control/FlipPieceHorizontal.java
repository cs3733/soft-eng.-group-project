package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Piece;

public class FlipPieceHorizontal implements ActionListener {

	Application app;
	LevelBuilderModel lb;

	/**
	 * Flips the piece horizontally.
	 * @param app current application
	 * @param lb current model
	 */
	public FlipPieceHorizontal(Application app, LevelBuilderModel lb) {
		this.app = app;
		this.lb = lb;
	}

	@Override
	/**
	 * Draws and rotates the piece horizontally.
	 */
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
			Piece p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
			p.flipHorizontal();
			app.getEditLevelView().repaint();
		}
		else if(app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null) {
			Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece();
			p.flipHorizontal();
			app.getEditLevelView().repaint();
		}
	}
	
}
