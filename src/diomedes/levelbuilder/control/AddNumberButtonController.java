package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.view.Application;

/**
 * Controller for the add a number button.
 *
 */
public class AddNumberButtonController implements ActionListener {

	Application app;
	
	/**
	 * Constructor for add number controller.
	 * @param app takes the current application
	 */
	public AddNumberButtonController(Application app) {
		this.app = app;
	}

	@Override
	/**
	 * Adds the number to the button if the add number button is selected. 
	 * Makes sure a piece is not selected.
	 */
	public void actionPerformed(ActionEvent e) {
		if(app.getEditLevelView().getActionToolbarView().getAddNumberButton().isSelected()) {
			app.getModel().getCurrentLevel().getBoard().clearSelectedPiece();
			app.getModel().getCurrentLevel().getBullpen().clearSelectedPiece();
			app.getEditLevelView().getPieceToolbarView().getToolbar().clearSelectedPiece();
		}
	}

}
