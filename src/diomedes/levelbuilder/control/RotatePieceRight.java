package diomedes.levelbuilder.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.levelbuilder.entity.LevelBuilderModel;
import diomedes.levelbuilder.view.Application;
import diomedes.shared.entity.Piece;

/**
 * Controller for rotating the selected Piece 90 degrees to the right.
 * @author Kyle Carrero
 *
 */
public class RotatePieceRight implements ActionListener {

	Application app;
	LevelBuilderModel lb;

	/**
	 * Constructor for rotating a piece
	 * @param Application app current application
	 * @param LevelBuilderModel lb current model
	 */
	public RotatePieceRight(Application app, LevelBuilderModel lb) {
		this.app = app;
		this.lb = lb;
	}

	/**
	 * Once clicked we rotate the piece in the bullpen.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
			Piece p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
			p.rotateRight();
			app.getEditLevelView().repaint();
		}
		else if(app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece() != null) {
			Piece p = app.getEditLevelView().getPieceToolbarView().getToolbar().getSelectedPiece();
			p.rotateRight();
			app.getEditLevelView().repaint();
		}
	}
}