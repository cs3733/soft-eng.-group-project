package diomedes.kabasuji;

/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 


/*
 * SplashDemo.java
 *
 */
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.SplashScreen;

/**
 * Splash Screen for Kabasuji
 *
 */
public class KabasujiSplashScreen {
	//Constants for the fade in and fade out animations, and the static image
	final static String FADEIN_PATH = "/diomedes/assets/KabasujiFadeIn.gif";
	final static String STATICLOGO_PATH = "/diomedes/assets/KabasujiStatic.gif";
	final static String FADEOUT_PATH = "/diomedes/assets/KabasujiFadeOut.gif";


	//Time constants for how long the fade in and fade out animations take (in milliseconds)
	final static int FADEOUT_TIME = 1250;

	//References to the splash screen and the graphics object
	final SplashScreen splash;
	final Graphics2D g;
	/**
	 * Gets references to the SplashScreen object,
	 * and the graphics object for drawing
	 */
	public KabasujiSplashScreen() {
		//Get the reference to the Java Splash Screen
		splash = SplashScreen.getSplashScreen();
		//Check that the reference was retrieved properly
		if (splash == null) {
			//Splash Screen reference could not be retrieved
			System.out.println("SplashScreen.getSplashScreen() returned null");
			g = null;
			return;
		}

		//Get a reference to the graphics area to paint over
		g = splash.createGraphics();

		//Check that the reference was retrieved properly
		if (g == null) {
			System.out.println("g is null");
			return;
		}
	}

	/**
	 * Sets the text to display on the splash screen
	 */
	public void setText(String text) {
		//Exit early if the reference to the splash screen was not obtained
		if(splash == null) {
			return;
		}
		g.setComposite(AlphaComposite.Clear);
		g.fillRect(120,140,200,40);
		g.setPaintMode();
		g.setColor(Color.BLACK);
		g.drawString(text, 120, 150);
		splash.update();
	}

	/**
	 * Closes the splash screen
	 */
	public void close() {
		if(splash == null) {
			return;
		}
		splash.close();
	}
}