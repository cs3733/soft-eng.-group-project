package diomedes.kabasuji.view.popup;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.LevelSelectView;

/**
 * Pop up when a player loses a level.
 * @author Myles Spencer
 *
 */
public class FailedLevelPopupView extends JDialog implements ActionListener
{
	Application app;
	private JOptionPane optionPane;
	JButton replayButton;
	JButton returnButton;
	
	/**
	 * Constructor for a failed level.
	 * @param app current application.
	 */
	public FailedLevelPopupView(Application app)
	{
		this.app = app;
		
		replayButton = new JButton();
		replayButton.setText("Replay Game?");
		replayButton.addActionListener(this);
		
		returnButton = new JButton();
		returnButton.setText("Return to Level Select");
		returnButton.addActionListener(this);
		
		Object[] options = {replayButton, returnButton};
		optionPane = new JOptionPane("You lost!",
									JOptionPane.PLAIN_MESSAGE,
									JOptionPane.YES_NO_OPTION,
									null,
									options,
									options[1]);
		optionPane.setAlignmentY(CENTER_ALIGNMENT);
		add(optionPane);
		setLocationRelativeTo(app);
		setVisible(true);
		setModal(true);
		setTitle("Level Failed!");
		setSize(new Dimension(325, 175));
	}

	/**
	 * Determines whether the user wants to replay the game or return to the main menu. 
	 * Once updated, it updates the view.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource().equals(replayButton)) {
			app.getModel().getCurrentLevel().reset();
			app.getModel().getCurrentLevel().recalculate();
			app.getLevelView().setLevel(app.getModel().getCurrentLevel());
			app.getLevelView().updateView();
			app.getLevelView().repaint();
			dispose();
		}
		else if(e.getSource().equals(returnButton)) {
			app.setContentPanel(LevelSelectView.NAME);
			dispose();
		}
	}

}
