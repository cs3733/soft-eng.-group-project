package diomedes.kabasuji.view.popup;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.AchievementView;
import diomedes.kabasuji.view.panel.LevelSelectView;
import diomedes.shared.entity.Level;

/**
 * Popup dialog for presenting winner with thier achievement and asking if they want to replay
 * or go to the level select.
 * @author Kyle Carrero
 * @author Myles Spencer
 */
@SuppressWarnings("serial")
public class LevelCompletePopupView extends JDialog implements ActionListener {

	private JOptionPane optionPane;
	JButton replayButton;
	JButton returnButton;
	Application app;

	/**
	 * Creates the popup dialog
	 * @param app The high-level application
	 */
	public LevelCompletePopupView(Application app) {
		this.app = app;
		
		replayButton = new JButton();
		replayButton.setText("Replay Game?");
		replayButton.addActionListener(this);
		
		returnButton = new JButton();
		returnButton.setText("Return to Level Select");
		returnButton.addActionListener(this);
		
		Object[] options = {replayButton, returnButton};
		optionPane = new JOptionPane(new AchievementView(app),
									JOptionPane.PLAIN_MESSAGE,
									JOptionPane.YES_NO_OPTION,
									null,
									options,
									options[1]);
		optionPane.setAlignmentY(CENTER_ALIGNMENT);
		add(optionPane);
		setLocationRelativeTo(app);
		setVisible(true);
		setModal(true);
		setTitle("Level Complete!");
		setSize(new Dimension(325, 175));
		
		//Unlock next level
		Level current = app.getModel().getCurrentLevel();
		for(Level lvl: app.getModel().getLevels())
		{
			if(lvl.getVariationName() == current.getVariationName() && lvl.getOrder() == current.getOrder() + 1)
			{
				lvl.unlockLevel();
			}
		}
		
		//Record achievement
		current.recordAchievement();
	}

	/**
	 * Allow the player to replay the window, or to return to the main menu
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//Replay the Level by reseting all of the information
		if(e.getSource().equals(replayButton)) {
			app.getModel().getCurrentLevel().reset();
			app.getModel().getCurrentLevel().recalculate();
			app.getLevelView().setLevel(app.getModel().getCurrentLevel());
			app.getLevelView().updateView();
			app.getLevelView().repaint();
			dispose();
		}
		//Return to the main menu
		else if(e.getSource().equals(returnButton)) {
			app.setContentPanel(LevelSelectView.NAME);
			dispose();
		}
	}
}
