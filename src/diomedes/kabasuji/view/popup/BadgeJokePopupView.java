package diomedes.kabasuji.view.popup;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.kabasuji.view.Application;

/**
 * View for the badge joke.
 * @author Kyle Carrero
 */
public class BadgeJokePopupView extends JDialog implements ActionListener {

	private JOptionPane optionPane;
	private JDialog popupWindow;
	JButton okButton;
	Application application;

	/**
	 * Create the frame.
	 */
	public BadgeJokePopupView(Application application) {
		this.application = application;
		
		okButton = new JButton();
		okButton.setText("Ok");
		okButton.addActionListener(this);
		
		Object[] options = {okButton};
		optionPane = new JOptionPane("Yeah... We wish we had them too...",
									JOptionPane.INFORMATION_MESSAGE,
									JOptionPane.OK_OPTION,
									null,
									options,
									options[0]);
		
		popupWindow = new JDialog(application);
		popupWindow.add(optionPane);
		popupWindow.setLocationRelativeTo(application);
		popupWindow.setVisible(true);
		popupWindow.setTitle("Badges? What badges?");
		popupWindow.setModal(true);
		popupWindow.setSize(new Dimension(300, 125));
	}

	@Override
	/**
	 * Closes the window
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(okButton)) {
			popupWindow.dispose();
		}	
	}
	
}
