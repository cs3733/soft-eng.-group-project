package diomedes.kabasuji.view.popup;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.kabasuji.view.Application;

/**
 * Ensures the plater wants to quit.
 * @author Kyle Carrero
 */
@SuppressWarnings("serial")
public class QuitAssurancePopupView extends JDialog implements ActionListener {

	private JOptionPane optionPane;
	JButton exitButton;
	JButton returnButton;
	Application application;

	/**
	 * Create the frame.
	 * @param application
	 */
	public QuitAssurancePopupView(Application application) {
		this.application = application;
		
		exitButton = new JButton();
		exitButton.setText("Exit Game");
		exitButton.addActionListener(this);
		
		returnButton = new JButton();
		returnButton.setText("Return to Game");
		returnButton.addActionListener(this);
		
		Object[] options = {exitButton, returnButton};
		optionPane = new JOptionPane("Are you sure you want to EXIT?",
									JOptionPane.QUESTION_MESSAGE,
									JOptionPane.YES_NO_OPTION,
									null,
									options,
									options[1]);
		add(optionPane);
		setLocationRelativeTo(application);
		setVisible(true);
		setModal(true);
		setTitle("Exit Game?");
		setSize(new Dimension(300, 125));
		
	}

	/**
	 * Determines if the user wants to exit the application.
	 */
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(returnButton)) {
			dispose();
		}
		else if(e.getSource().equals(exitButton)) {
			application.dispose();
			dispose();
		}
		
	}
}
