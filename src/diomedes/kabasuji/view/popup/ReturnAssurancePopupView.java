
package diomedes.kabasuji.view.popup;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import diomedes.kabasuji.control.ExitButtonController;
import diomedes.kabasuji.view.Application;

/**
 * Ensures that the user wants to exit the level.
 * @author Kyle Carrero
 */
@SuppressWarnings("serial")
public class ReturnAssurancePopupView extends JDialog implements ActionListener {

	private JOptionPane optionPane;
	private JDialog popupWindow;
	JButton exitButton;
	JButton returnButton;
	Application application;

	/**
	 * Create the frame.
	 */
	public ReturnAssurancePopupView(Application application) {
		this.application = application;
		
		popupWindow = new JDialog(application);
		
		exitButton = new JButton();
		exitButton.setText("Exit Game");
		exitButton.addActionListener(new ExitButtonController(application, popupWindow));
		
		
		returnButton = new JButton();
		returnButton.setText("Return to Game");
		returnButton.addActionListener(this);
		
		Object[] options = {exitButton, returnButton};
		optionPane = new JOptionPane("Are you sure you want to EXIT?",
									JOptionPane.QUESTION_MESSAGE,
									JOptionPane.YES_NO_OPTION,
									null,
									options,
									options[1]);
		
		popupWindow.add(optionPane);
		popupWindow.setLocationRelativeTo(application);
		popupWindow.setVisible(true);
		popupWindow.setModal(true);
		popupWindow.setTitle("Exit Game?");
		popupWindow.setSize(new Dimension(300, 125));
		
		
	}

	/**
	 * Exits the window if the user wants to return.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(returnButton)) {
			popupWindow.dispose();
		}
	}
}
