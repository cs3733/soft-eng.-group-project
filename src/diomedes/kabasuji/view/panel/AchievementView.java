package diomedes.kabasuji.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import diomedes.kabasuji.view.Application;

/**
 * View that handles the drawing of the Achievements.
 * @author Myles Spencer
 */
public class AchievementView extends JPanel 
{
	Application app;
	BufferedImage filledStar;
	BufferedImage emptyStar;
	int padding;
	int spacing;
	int imageSize;
	
	/**
	 * Default constructor for the achievement view.
	 * @param application takes the current app.
	 */
	public AchievementView(Application application)
	{
		this.app = application;
		this.padding = 5;
		this.spacing = 5;
		this.imageSize = 50;
		int width = (imageSize + spacing) * 3;
		int height = imageSize + (2 * padding);
		setSize(width, height);
		setPreferredSize(new Dimension(width, height));
		try
		{
			this.filledStar = ImageIO.read(AchievementView.class.getResource("/diomedes/assets/filledStar.png"));
			this.emptyStar = ImageIO.read(AchievementView.class.getResource("/diomedes/assets/emptyStar.png"));
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	/**
	 * Draws the achievements.
	 * @param Graphics g ensures a white background and the container for the stars
	 */
	public void paintComponent(Graphics g) 
	{
		int numFilled = app.getModel().getCurrentLevel().getCurrentAchievement();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, (imageSize + spacing) * 3 + 30, imageSize + (2 * padding) + 10);
		for(int i = 0; i < 3; i++)
		{
			if(i < numFilled)
			{
				g.drawImage(filledStar, (i * imageSize) + spacing + 5, padding, null);
			}
			else
			{
				g.drawImage(emptyStar, (i * imageSize) + spacing + 5, padding, null);
			}
		}
	}
}
