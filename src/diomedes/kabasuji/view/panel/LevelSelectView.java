package diomedes.kabasuji.view.panel;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import diomedes.kabasuji.control.GoToSelectVariation;
import diomedes.kabasuji.control.PlayLevel;
import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Level;
import diomedes.shared.view.LevelAchievmentRenderer;
import diomedes.shared.view.LevelNameRenderer;

/**
 * The view for selecting a level.
 * @author Kyle Carrero
 */
@SuppressWarnings("serial")
public class LevelSelectView extends JPanel {

	public static final String NAME = "LevelSelectView";
	JButton levelButton;
	JButton exitButton;
	private JTable table;
	private JLabel lblVariationName;
	
	private ArrayList<Level> puzzleLevels = new ArrayList<Level>();
	private ArrayList<Level> lightningLevels = new ArrayList<Level>();
	private ArrayList<Level> releaseLevels = new ArrayList<Level>();
	
	/**
	 * Constructor for the level select view.
	 * @param application Current application.
	 * @param model	Current model.
	 */
	public LevelSelectView(Application application, KabasujiModel model) {
		setBorder(new EmptyBorder(5, 5, 5, 5));
		
		sortLevels(model);
		
		lblVariationName = new JLabel("Variation Name");
		lblVariationName.setHorizontalAlignment(SwingConstants.CENTER);
		lblVariationName.setFont(new Font("Times New Roman", Font.BOLD, 20));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportBorder(null);
		
		JPanel panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(this);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(67)
					.addComponent(lblVariationName, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
					.addGap(73))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(212)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(226, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblVariationName, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addGap(48))
		);
		
		table = new JTable();
		
		levelButton = new JButton("Play Selected Level");
		panel.add(levelButton);
		levelButton.addActionListener(new PlayLevel(application, model, table));
		
		exitButton = new JButton("Variation Select");
		panel.add(exitButton);
		exitButton.addActionListener(new GoToSelectVariation(application));
		
		
		scrollPane.setViewportView(table);
		setLayout(gl_contentPane);
	}
	
	/**
	 * Sorts the levels for the table in different lists.
	 * @param model current model
	 */
	private void sortLevels(KabasujiModel model) {
		ArrayList<Level> kabasujiLevels = model.getLevels();
		Level temp;
		for(int i = 0; i < kabasujiLevels.size(); i++) {
			temp = kabasujiLevels.get(i);
			if(temp != null) {
				if(temp.getVariationName() == "Release") {
					releaseLevels.add(temp);
				}
				else if(temp.getVariationName() == "Puzzle") {
						puzzleLevels.add(temp);
				}
				else if(temp.getVariationName() == "Lightning") {
					lightningLevels.add(temp);
				}
			}
		}
	}

//	/**
//	 * Gets the Button associated with a certain string
//	 * @param str
//	 * @return The JButton aquainted with that action. If no such button exists, return null
//	 */
//	public JButton getButton(String str) {
//		switch(str) {
//		case "Level": return levelButton;
//		case "Exit": return exitButton;
//		}
//		
//		return null;
//	}
	
	/**
	 * Sets the variation name for the level and shows it.
	 * @param str
	 */
	
	public void setVariantName(String str) {
		lblVariationName.setText(str);
	}
	
	/**
	 * Method to set up the table with the given list of levels
	 * Sets the levels that are available in the list, and how they draw
	 * @param levels
	 */
	public void setUpTable(ArrayList<Level> levels) {
		Object[][] obj = new Object[levels.size()][2];
		
		//Add the levels in the list to the table
		for(int i = 0; i < levels.size(); i++) {
			obj[i][0] = levels.get(i);
			obj[i][1] = levels.get(i);
		}
		
		table.setModel(new DefaultTableModel(
			obj,
			new String[] {
				"Level Name", "Achievements"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(150);
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(0).setCellRenderer(new LevelNameRenderer());
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setMinWidth(50);
		table.getColumnModel().getColumn(1).setCellRenderer(new LevelAchievmentRenderer());
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoResizeMode(table.AUTO_RESIZE_ALL_COLUMNS);
	}

	/**
	 * Gets the correct level list depending on the variation it's in.
	 * @param variantName the name of the variation list we are looking for
	 * @return ArrayList<Level> the list of levels for that variation
	 */
	public ArrayList<Level> getVariantLevels(String variantName) {
		switch(variantName) {
		case "Puzzle": return this.puzzleLevels;
		case "Lightning": return this.lightningLevels;
		case "Release": return this.releaseLevels;
		}
		return new ArrayList<Level>();
	}
}
