package diomedes.kabasuji.view.panel;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import diomedes.kabasuji.control.BoardControllerLightning;
import diomedes.kabasuji.control.BoardControllerPuzzle;
import diomedes.kabasuji.control.BoardControllerRelease;
import diomedes.kabasuji.control.ExitLevel;
import diomedes.kabasuji.control.LightningTimer;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.NumberedTileGroup;
import diomedes.shared.entity.Release;

/**
 * Sets the current view for the corresponding level.
 * @author Kyle Carrero
 * @author Myles Spencer
 */
public class LevelView extends JPanel {

	public static final String NAME = "LevelView";

	BoardView boardView;
	StatusView statusView;
	BullpenView bullpenView;
	ActionToolbarView actionToolbar;
	AchievementView achievementView;
	Level level;
	JPanel releaseInfo;
	Application app;
	JTextArea labelBlue;
	JTextArea labelRed;
	JTextArea labelGreen;
	LightningTimer timer;

	/**
	 * Constructor for the level view.
	 * @param application current application
	 * @param level current level
	 */
	public LevelView(Application application, Level level) {
		setBorder(new EmptyBorder(5, 5, 5, 5));

		this.app = application;		
	}

	/**
	 * Sets up all of the different options for the levelview.
	 */
	private void setup()
	{
		bullpenView = new BullpenView(app, level.getBullpen());
		boardView = new BoardView(app, level.getBoard());

		actionToolbar = new ActionToolbarView(app);
		statusView = new StatusView(app);
		achievementView = new AchievementView(app);

		//If it's in the Variation of Puzzle add the new Controller
		if(level.getVariationName().equals("Puzzle")){
			boardView.addMouseListener(new BoardControllerPuzzle(app));
			boardView.addMouseMotionListener(new BoardControllerPuzzle(app));
		}
		else if(level.getVariationName().equals("Lightning")) {
			boardView.addMouseListener(new BoardControllerLightning(app));
			boardView.addMouseMotionListener(new BoardControllerLightning(app));
			if(timer != null)
			{
				timer.stop();
			}
			timer = new LightningTimer(app, ((Lightning)level).getMaxTime());
			timer.start();
		}
		else if(level.getVariationName().equals("Release")){
			boardView.addMouseListener(new BoardControllerRelease(app));
			boardView.addMouseMotionListener(new BoardControllerRelease(app));
		}

		JButton exitButton = new JButton("Return");

		releaseInfo = new JPanel();
		releaseInfo.setBorder(new LineBorder(new Color(0, 0, 0)));

		GroupLayout gl_contentPane = new GroupLayout(this);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addGap(71)
										.addComponent(bullpenView, GroupLayout.PREFERRED_SIZE, 566, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup()
										.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
												.addComponent(exitButton)
												.addComponent(releaseInfo, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
												.addGroup(gl_contentPane.createSequentialGroup()
														.addGap(13)
														.addComponent(statusView, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addGap(18)
														.addComponent(achievementView, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_contentPane.createSequentialGroup()
														.addComponent(boardView, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addGap(26)
														.addComponent(actionToolbar, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))))
						.addGap(22))
				);
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addComponent(bullpenView, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(actionToolbar, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
								.addComponent(releaseInfo, GroupLayout.PREFERRED_SIZE, 379, GroupLayout.PREFERRED_SIZE)
								.addComponent(boardView, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
										.addComponent(exitButton))
								.addGroup(gl_contentPane.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
												.addComponent(statusView, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(achievementView, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
						.addGap(11))
				);
		releaseInfo.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel coveredRedPanel = new JPanel();
		coveredRedPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		releaseInfo.add(coveredRedPanel);
		JTextArea lblCoveredRed = new JTextArea("Covered Red:");
		lblCoveredRed.setBackground(UIManager.getColor("InternalFrame.borderLight"));
		lblCoveredRed.setEditable(false);
		lblCoveredRed.setLineWrap(true);
		coveredRedPanel.add(lblCoveredRed);
		labelRed = new JTextArea(" ");
		labelRed.setBackground(UIManager.getColor("InternalFrame.borderLight"));
		labelRed.setEditable(false);
		labelRed.setWrapStyleWord(true);
		labelRed.setLineWrap(true);
		coveredRedPanel.add(labelRed);

		JPanel coveredBluePanel = new JPanel();
		coveredBluePanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		releaseInfo.add(coveredBluePanel);
		JTextArea lblCoveredBlue = new JTextArea("Covered Blue:");
		lblCoveredBlue.setBackground(UIManager.getColor("InternalFrame.borderLight"));
		lblCoveredBlue.setEditable(false);
		lblCoveredBlue.setLineWrap(true);
		coveredBluePanel.add(lblCoveredBlue);
		labelBlue = new JTextArea(" ");
		labelBlue.setBackground(UIManager.getColor("InternalFrame.borderLight"));
		labelBlue.setEditable(false);
		labelBlue.setLineWrap(true);
		coveredBluePanel.add(labelBlue);

		JPanel coveredGreenPanel = new JPanel();
		coveredGreenPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		releaseInfo.add(coveredGreenPanel);
		JTextArea lblCoveredGreen = new JTextArea("Covered Green:");
		lblCoveredGreen.setBackground(UIManager.getColor("InternalFrame.borderLight"));
		lblCoveredGreen.setEditable(false);
		lblCoveredGreen.setLineWrap(true);
		coveredGreenPanel.add(lblCoveredGreen);
		labelGreen = new JTextArea(" ");
		labelGreen.setBackground(UIManager.getColor("InternalFrame.borderLight"));
		labelGreen.setEditable(false);
		labelGreen.setLineWrap(true);
		coveredGreenPanel.add(labelGreen);

		releaseInfo.setVisible(false);

		setLayout(gl_contentPane);

		exitButton.addActionListener(new ExitLevel(app));
	}

	/**
	 * Sets the current level; clears the previous.
	 * @param level current level.
	 */
	public void setLevel(Level level) {
		this.level = level;
		this.removeAll();
		setup();
	}


	/**
	 * hides the release info if not in a release level.
	 */
	public void updateView() {
		((StatusView) statusView).setContentPanel(level.getVariationName());
		if(level.getVariationName() == "Release") {
			releaseInfo.setVisible(true);
		}
		else {
			releaseInfo.setVisible(false);
		}
	}

	/**
	 * Getter for the board view
	 * @return this instance of board view
	 */
	public BoardView getBoardView(){
		return this.boardView;
	}

	/**
	 * Getter for the level set.
	 * @return level current level
	 */

	public Level getLevel() {
		return level;
	}

	/**
	 * Getter for the status view.
	 * @return StatusView this status view.
	 */
	public StatusView getStatusView(){
		return this.statusView;
	}

	/**
	 * Returns the bullpen view
	 * @return
	 */
	public BullpenView getBullpenView(){
		return this.bullpenView;
	}

	/**
	 * Updates the labels to show the view of numbers covered.
	 * 0 is assumed to be Red in level builder; thus, this should show all the covered in Red
	 */
	public void refreshLabelRed(){
		boolean changed = false;
		if(this.app.getModel().getCurrentLevel().getVariationName().equalsIgnoreCase("Release")){
			Release level = ((Release) app.getModel().getCurrentLevel());
			if(level.getTileGroups().size() > 0){
				NumberedTileGroup ntg = level.getTileGroups().get(0);
				boolean[] cov = ntg.getCovered();
				String numbers = "";
				for(int i = 0; i< cov.length; i++){
					if(cov[i]){
						int c = i+1;
						numbers = numbers + (" " + c + " ");
						changed = true;
					}
				}
				if(changed){
					labelRed.setText(numbers);
				}
			}

		}

	}

	/**
	 * Updates the labels to show the view of numbers covered.
	 * 2 is assumed to be Blue in level builder; thus, this should show all the covered in Red
	 */
	public void refreshLabelBlue(){
		boolean changed = false;
		if(this.app.getModel().getCurrentLevel().getVariationName().equalsIgnoreCase("Release")){
			Release level = ((Release) app.getModel().getCurrentLevel());
			if(level.getTileGroups().size() > 2){
				NumberedTileGroup ntg = level.getTileGroups().get(2);
				boolean[] cov = ntg.getCovered();
				String numbers = "";
				for(int i = 0; i< cov.length; i++){
					if(cov[i]){
						int c = i+1;
						numbers = numbers + (" " + c + " ");
						changed = true;
					}
				}
				if(changed){
					labelBlue.setText(numbers);
				}
			}

		}

	}

	/**
	 * Updates the labels to show the view of numbers covered.
	 * 1 is assumed to be Green in level builder; thus, this should show all the covered in Green
	 */

	public void refreshLabelGreen(){
		boolean changed = false;
		if(this.app.getModel().getCurrentLevel().getVariationName().equalsIgnoreCase("Release")){
			Release level = ((Release) app.getModel().getCurrentLevel());
			if(level.getTileGroups().size() > 1){
				NumberedTileGroup ntg = level.getTileGroups().get(1);
				boolean[] cov = ntg.getCovered();
				String numbers = "";
				for(int i = 0; i < cov.length; i++){
					if(cov[i]){
						int c = i+1;
						numbers = numbers + (" " + c + " ");
						changed = true;
					}
				}
				if(changed){
					labelGreen.setText(numbers);
				}
			}

		}

	}

	/**
	 * Getter for achievement view
	 * @return AchievementView current achievement view
	 */
	public AchievementView getAchievementView()
	{
		return this.achievementView;
	}

	/**
	 * Returns the timer for Kabasuji
	 * @return LightningTimer the current timer
	 */
	public LightningTimer getTimer(){
		return this.timer;
	}

	//	/**
	//	 * Resets the label for the covered numbers and higes all of the release info.
	//	 */
	//	public void resetCoveredLabel(){
	//		labelGreen.setText("");
	//		labelRed.setText("");
	//		labelBlue.setText("");
	//		releaseInfo.setVisible(false);
	//	}
}
