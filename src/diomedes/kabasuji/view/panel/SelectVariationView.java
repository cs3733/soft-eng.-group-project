package diomedes.kabasuji.view.panel;

import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import diomedes.kabasuji.control.GoToLevelSelect;
import diomedes.kabasuji.control.ReturnToMainMenu;
import diomedes.kabasuji.view.Application;

/**
 * View for selecting a variation of level.
 *
 */
public class SelectVariationView extends JPanel
{
	public static final String NAME = "SelectVariationView";
	JTable table;
	JButton mainMenuButton;
	JButton puzzleButton; //button2
	JButton releaseButton; //button3
	JButton lightningButton; //button4
	
	/**
	 * Constructor for select variation view.
	 * @param application current application view.
	 */
	public SelectVariationView(Application application)
	{
		setBorder(new EmptyBorder(5, 5, 5, 5));
		
		table = new JTable();
		
		JLabel lblSelectVariation = new JLabel("Select Variation");
		lblSelectVariation.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblSelectVariation.setHorizontalAlignment(SwingConstants.CENTER);
		
		mainMenuButton = new JButton("MAIN MENU");
		mainMenuButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		puzzleButton = new JButton("PUZZLE");
		puzzleButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		releaseButton = new JButton("RELEASE");
		releaseButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		lightningButton = new JButton("LIGHTNING");
		lightningButton.setBackground(UIManager.getColor("Button.background"));
		lightningButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		GroupLayout gl_contentPane = new GroupLayout(this);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSelectVariation, GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
					.addGap(10))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(133)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(table, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)
							.addGap(324))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(releaseButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
								.addComponent(puzzleButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
								.addComponent(lightningButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE))
							.addGap(128))))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addGap(155)
					.addComponent(mainMenuButton, GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
					.addGap(152))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSelectVariation, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)
					.addGap(74)
					.addComponent(releaseButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(puzzleButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lightningButton)
					.addPreferredGap(ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
					.addComponent(mainMenuButton)
					.addGap(65))
		);
		setLayout(gl_contentPane);
		releaseButton.addActionListener(new GoToLevelSelect(application, "Release"));
		puzzleButton.addActionListener(new GoToLevelSelect(application, "Puzzle"));
		lightningButton.addActionListener(new GoToLevelSelect(application, "Lightning"));
		mainMenuButton.addActionListener(new ReturnToMainMenu(application));
	}
	
	
	
}