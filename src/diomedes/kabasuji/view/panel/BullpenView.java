package diomedes.kabasuji.view.panel;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import diomedes.kabasuji.control.BullpenController;
import diomedes.kabasuji.control.BullpenScrollbarListener;
import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Bullpen;

/**
 * In charge of the scrolling of the bullppen.
 * @author Kyle Carrero
 * @author Aura Velarde
 * @author Myles Spencer
 */
@SuppressWarnings("serial")
public class BullpenView extends JScrollPane {

	Bullpen bullpen;
	Application application;
	KabasujiModel model;
	BullpenPieceView bullpenPieces;

	/**
	 * Constructor for the bullpen view
	 * @param application current application
	 * @param bullpen entity for the bullpen
	 */
	public BullpenView(Application application, Bullpen bullpen) {
		this.bullpen = bullpen;
		this.application = application;
		this.model = application.getModel();
		this.getHorizontalScrollBar().addAdjustmentListener(new BullpenScrollbarListener(application));

		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.bullpenPieces = new BullpenPieceView(application);
		this.bullpenPieces.addMouseListener(new BullpenController(application));

		setViewportView(bullpenPieces);
	}
	
	/**
	 * Updates the view, and repaints.
	 */
	public void update()
	{
		updateUI();
		this.bullpenPieces.repaint();
	}

	/**
	 * Gets the list of piece views in the application.
	 * @return list of piece views.
	 */
	public BullpenPieceView getPieceViews() {
		return bullpenPieces;
	}

	/**
	 * Gives the current bullpen.
	 * @return bullpen the bullpen entity.
	 */
	public Bullpen getBullpen() {
		return bullpen;
	}

}
