package diomedes.kabasuji.view.panel;

import java.awt.CardLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.Release;

/**
 * Sets the views for the different levels on depending on the variation.
 * @author Kyle Carrero
 * @author Aura Velarde
 */
public class StatusView extends JPanel {

	public static final String PUZZLE = "Puzzle";
	public static final String LIGHTNING = "Lightning";
	public static final String RELEASE = "Release";

	//Added that it takes an application
	Application app;
	JLabel numMoves;
	JLabel timer;
	JPanel lightningStatus;
	JPanel puzzleStatus;
	JPanel releaseStatus;
	JLabel groupsCovered;
	JLabel lblNumGroupsCovered;
	
	/**
	 * Initializes the card layouts.
	 * @param app current application
	 */
	public StatusView(Application app) {
		setLayout(new CardLayout());

		JPanel lightningStatus = new JPanel();
		JPanel puzzleStatus = new JPanel();
		JPanel releaseStatus = new JPanel();
		this.app = app; 


		//Puzzle Status
		JLabel lblNumMovesLeft = new JLabel("Number of Moves Left: ");
		numMoves = new JLabel("");
		lblNumMovesLeft.setFont(new Font("Tahoma", Font.PLAIN, 14));
		numMoves.setFont(new Font("Tahoma", Font.PLAIN, 14));
		puzzleStatus.add(lblNumMovesLeft);
		puzzleStatus.add(numMoves);
		add(puzzleStatus, PUZZLE);
		

		//Lightning Status
		JLabel lblTimeLeft = new JLabel("Time Remaining: ");
		timer = new JLabel("");
		lblTimeLeft.setFont(new Font("Tahoma", Font.PLAIN, 14));
		timer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lightningStatus.add(lblTimeLeft);
		lightningStatus.add(timer);
		add(lightningStatus, LIGHTNING);

		//Release Status
		lblNumGroupsCovered = new JLabel("Number Groups Covered: ");
		groupsCovered = new JLabel("");
		lblNumGroupsCovered.setFont(new Font("Tahoma", Font.PLAIN, 14));
		groupsCovered.setFont(new Font("Tahoma", Font.PLAIN, 14));
		releaseStatus.add(lblNumGroupsCovered);
		releaseStatus.add(groupsCovered);
		add(releaseStatus, RELEASE);
	}

	/**
	 * Sets the current level card layour onto the top.
	 * @param name the variation you're tring to retrieve.
	 */
	public void setContentPanel(String name)
	{
		CardLayout cl = (CardLayout) this.getLayout();
		try
		{
			cl.show(this, name);
		}
		catch(Exception e)
		{
			System.err.println("Error setting content panel.\n");
		}
	}
	
	/**
	 * Refreshes the number of moves left!
	 */
	public void refreshLabel(){
		if(this.app.getModel().getCurrentLevel().getVariationName().equalsIgnoreCase("Puzzle")){
			Puzzle level = ((Puzzle) app.getModel().getCurrentLevel());
			numMoves.setText(level.getNumMovesLeft()+"");
			
		}
	}
	
	/**
	 * Refreshes the time for lightning
	 * @param time
	 */
	public void setLightningTime(String time)
	{
		timer.setText(time);
	}
	
	/**
	 * Refreshes the number of tile groups covered for release.
	 */
	public void refreshReleaseLabel(){
		//if(this.app.getModel().getCurrentLevel().getVariationName().equalsIgnoreCase("Release")){
			Release level = ((Release) app.getModel().getCurrentLevel());
			groupsCovered.setText(level.getNumTilesGroupCovered()+"");
			
		//}
	}

}
