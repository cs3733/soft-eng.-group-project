package diomedes.kabasuji.view.panel;

import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import diomedes.kabasuji.control.BadgeJokeController;
import diomedes.kabasuji.control.CloseApplication;
import diomedes.kabasuji.control.GoToSelectVariation;
import diomedes.kabasuji.view.Application;

/**
 * View for the Main Menu
 * @author Kyle Carrero
 */
@SuppressWarnings("serial")
public class MainMenuView extends JPanel {

	public static final String NAME = "MainMenuView";
	JButton playButton;
	JButton exitButton;
	JButton badgeButton;
	
	/**
	 * Constructor for the main menu
	 * @param application current application
	 */
	public MainMenuView(Application application) {
		setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JLabel lblKabasuji = new JLabel("Kabasuji");
		lblKabasuji.setHorizontalAlignment(SwingConstants.CENTER);
		lblKabasuji.setFont(new Font("Times New Roman", Font.BOLD, 32));
		
		playButton = new JButton("Play Kabasuji");
		playButton.setFont(new Font("Times New Roman", Font.BOLD, 20));
		
		exitButton = new JButton("Exit");
		exitButton.setFont(new Font("Times New Roman", Font.BOLD, 20));
		
		badgeButton = new JButton("Badges");
		badgeButton.setFont(new Font("Times New Roman", Font.BOLD, 20));
		
		GroupLayout gl_panel = new GroupLayout(this);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(194)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblKabasuji, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
						.addComponent(badgeButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
						.addComponent(exitButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
						.addComponent(playButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE))
					.addGap(221))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(97)
					.addComponent(lblKabasuji, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(112)
					.addComponent(playButton, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(exitButton, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(badgeButton, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		setLayout(gl_panel);
		playButton.addActionListener(new GoToSelectVariation(application));
		exitButton.addActionListener(new CloseApplication(application));
		badgeButton.addActionListener(new BadgeJokeController(application));
	}
	
	
}
