package diomedes.kabasuji.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Position;

/**
 * Draws the bullpen.
 * @author Myles Spencer
 *
 */

public class BullpenPieceView extends JPanel
{
	Application app;
	Bullpen bullpen;
	int squareSize;
	int padding;
	int height;
	int width;
	int squareRadius;
	
	/**
	 * Constructor for the bullpen view. Calculate the size. 
	 * @param app current application.
	 */
	public BullpenPieceView(Application app)
	{
		this.app = app;
		this.bullpen = app.getModel().getCurrentLevel().getBullpen();
		this.height = 100;
		this.squareRadius = 5;
		this.padding = 0;
		this.width = (height * this.bullpen.getPieces().size());
		this.squareSize = (height - (2 * padding)) / 8;
		setSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
		setBounds(0, 0, width, height);
	}
	
	@Override
	public void paintComponent(Graphics g) 
	{	
		for(int i = 0; i < bullpen.getPieces().size(); i++)
		{
			Piece p = bullpen.getPieces().get(i);
			boolean isSelected = false;
			if(p == bullpen.getSelectedPiece())
			{
				isSelected = true;
			}
			
			for(Position pos: p.getSquares())
			{
				int x = (this.height/2) + (i * height) + (pos.getCol() * squareSize);
				int y = (this.height/2) + (pos.getRow() * squareSize) - squareSize;
				if(isSelected)
				{
					g.setColor(Color.YELLOW);
				}
				else
				{
					g.setColor(Color.GREEN);
				}
				g.fillRoundRect(x, y, squareSize, squareSize, squareRadius, squareRadius);
				g.setColor(Color.BLACK);
				g.drawRoundRect(x, y, squareSize, squareSize, squareRadius, squareRadius);
			}
		}
	}
	
	/**
	 * Updates the current size of the bullpen.
	 */
	public void updateSize()
	{
		this.width = (height * this.bullpen.getPieces().size());
		setSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
	}
	
	/**
	 * Returns the current height of the bullpen.
	 * @return int height
	 */
	public int getHeight()
	{
		return this.height;
	}
}
