package diomedes.kabasuji.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;

import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.NumberedTile;
import diomedes.shared.entity.NumberedTileGroup;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Release;
import diomedes.shared.entity.ReleaseData;
import diomedes.shared.entity.Tile;

/**
 * Draws the board.
 * @author Kyle Carrero
 * @author Aura Velarde
 */
public class BoardView extends JPanel 
{
	Application app;
	Board board;
	Color tileColor = Color.GRAY;
	Color coveredTileColor = Color.BLUE;
	int squareSize;
	int cornerRadius = 10;
	int spacing = 2;
	//Hack to get where the mouse is
	int hoverX = 0;
	int hoverY = 0;
	
	public static final Color[] colors = {Color.CYAN, Color.GREEN, Color.MAGENTA, Color.RED, Color.ORANGE};
	
	/**
	 * Constructor for the board
	 * @param application current application
	 * @param board the entity of the board
	 */
	public BoardView(Application application, Board board) 
	{
		int width = 360;
		setPreferredSize(new Dimension(width, width));
		this.app = application;
		this.board = board;
		this.squareSize = ((width)/12) - spacing;
	}
	
	/**
	 * Draws the Board
	 */
	@Override
	public void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
		Dimension size = getSize();
		Insets insets = getInsets();
		
		//Draw tiles
		for(Tile tile: board.getTiles())
		{
			if(!tile.isActive()) {
				continue;
			}
			else if(tile.getIsCovered())
			{
				g.setColor(coveredTileColor);
			}
			else
			{
				g.setColor(tileColor);
			}
			
			g.fillRoundRect(tile.getPosition().getCol() * (squareSize + spacing), tile.getPosition().getRow() * (squareSize + spacing), squareSize, squareSize, cornerRadius, cornerRadius);
		}
		
		//Draw Hints
		for(PlacedPiece hint: board.getHints())
		{
			for(Position pos: hint.getPiece().getSquares())
			{
				g.setColor(new Color(Color.PINK.getRed(), Color.PINK.getGreen(), Color.PINK.getBlue(), 135));
				
				g.fillRoundRect(pos.derelatize(hint.getPosition()).getCol() * (squareSize + spacing), pos.derelatize(hint.getPosition()).getRow() * (squareSize + spacing), squareSize, squareSize, cornerRadius, cornerRadius);
			}
		}
		
		//Draw Release tiles
		if(app.getModel().getCurrentLevel() instanceof Release) {
			Release lvl = (Release) app.getModel().getCurrentLevel();
			ArrayList<NumberedTileGroup> tileGroups = lvl.getTileGroups();
			for(NumberedTileGroup ntg : tileGroups) {
				for(NumberedTile tile: ntg)
				{
					if(lvl.getBoard().getTileAtPosition(tile.getPosition()) != null && lvl.getBoard().getTileAtPosition(tile.getPosition()).isActive()) {
						ReleaseData rd = tile.getReleaseData();
						if(rd != null)
						{
							g.setColor(rd.getColor());
							Font f = new Font(Integer.toString(rd.getNumber()), Font.BOLD, 16);
							g.setFont(f);
							g.drawString(Integer.toString(rd.getNumber()), tile.getPosition().getCol() * (squareSize + spacing) + 10, tile.getPosition().getRow() * (squareSize + spacing) + 20);
						}
					}
				}
			}
		}
		
		//Draw Pieces
		for(PlacedPiece piece: board.getPieces())
		{
			for(Position pos: piece.getPiece().getSquares())
			{
				g.setColor(colors[piece.getPiece().getId() % 5]);
				
				g.fillRoundRect(pos.derelatize(piece.getPosition()).getCol() * (squareSize + spacing), pos.derelatize(piece.getPosition()).getRow() * (squareSize + spacing), squareSize, squareSize, cornerRadius, cornerRadius);
			}
		}
		
		
		//Draw selected Piece
		if(board.getSelectedPiece() != null)
		{
			for(Position pos: board.getSelectedPiece().getPiece().getSquares())
			{
				g.setColor(Color.YELLOW);
				
				g.fillRoundRect(pos.derelatize(board.getSelectedPiece().getPosition()).getCol() * (squareSize + spacing), pos.derelatize(board.getSelectedPiece().getPosition()).getRow() * (squareSize + spacing), squareSize, squareSize, cornerRadius, cornerRadius);
			}
		}
		
		//Draw hovered piece
		Piece piece = null;
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null)
		{
			piece = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
		}
		else if(app.getModel().getCurrentLevel().getBoard().getSelectedPiece() != null)
		{
			piece = app.getModel().getCurrentLevel().getBoard().getSelectedPiece().getPiece();
		}
		
		if(piece != null)
		{
			g.setColor(new Color(Color.BLACK.getRed(), Color.BLACK.getGreen(), Color.BLACK.getBlue(), 80));
			Position rel = new Position(getColFromX(hoverX), getRowFromY(hoverY));
			for(Position pos: piece.getSquares())
			{
				int x =  pos.derelatize(rel).getCol() * (squareSize + spacing);
				int y =  pos.derelatize(rel).getRow() * (squareSize + spacing);
				g.fillRoundRect(x, y, squareSize, squareSize, cornerRadius, cornerRadius);
			}
		}
		
	}


	/**
	 * Sets the hovering for the mouse.
	 * @param x the horizontal position
	 * @param y the vertical position
	 */
	public void setHoverPosition(int x, int y) {
		this.hoverX = x;
		this.hoverY = y;
		
	}

	/**
	 * Translates an x position to a board column.
	 * @param x position
	 * @return int the position in board column
	 */
	public int getColFromX(int x) {
		return (int) Math.floor((double)x/(squareSize + spacing));
	}

	/**
	 * Translates an y position to a board row.
	 * @param y position
	 * @return int the position in board row.
	 */
	public int getRowFromY(int y) {
		return (int) Math.floor((double)y/(squareSize + spacing));
	}
}
