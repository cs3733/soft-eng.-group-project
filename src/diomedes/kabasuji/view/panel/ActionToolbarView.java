package diomedes.kabasuji.view.panel;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import diomedes.kabasuji.control.FlipPieceHorizontal;
import diomedes.kabasuji.control.FlipPieceVertical;
import diomedes.kabasuji.control.RotatePieceLeft;
import diomedes.kabasuji.control.RotatePieceRight;
import diomedes.kabasuji.view.Application;

/**
 * View for the buttons that control a pieces orientation.
 * @author Kyle Carrero
 */
@SuppressWarnings("serial")
public class ActionToolbarView extends JPanel {

	Application app;
	
	/**
	 * Constructor for action toolbar view, initializes each buttons' controllers.
	 * @param app the current application
	 */
	public ActionToolbarView(Application app) {
		this.app = app;
		
		setBorder(new LineBorder(new Color(0, 0, 0), 2));
		JButton flipVertical = new JButton("Flip Vertical");
		flipVertical.addActionListener(new FlipPieceVertical(app, app.getModel()));
		add(flipVertical);
		
		JButton flipHorizontal = new JButton("Flip Horizontal");
		flipHorizontal.addActionListener(new FlipPieceHorizontal(app, app.getModel()));
		add(flipHorizontal);
		
		JButton rotateLeft = new JButton("Rotate Left");
		rotateLeft.addActionListener(new RotatePieceLeft(app, app.getModel()));
		add(rotateLeft);
		
		JButton rotateRight = new JButton("Rotate Right");
		rotateRight.addActionListener(new RotatePieceRight(app, app.getModel()));
		add(rotateRight);
	}
	
}
