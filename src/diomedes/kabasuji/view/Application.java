package diomedes.kabasuji.view;

import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import diomedes.kabasuji.control.CloseApplication;
import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.panel.LevelSelectView;
import diomedes.kabasuji.view.panel.LevelView;
import diomedes.kabasuji.view.panel.MainMenuView;
import diomedes.kabasuji.view.panel.SelectVariationView;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.Puzzle;

/**
 * High level application for Kabasuji 
 *
 */
@SuppressWarnings("serial")
public class Application extends JFrame {
	
	JPanel content;
	JPanel mainMenuPanel;
	JPanel selectVariationPanel;
	JPanel levelSelectPanel;
	JPanel levelPanel;
	
	KabasujiModel model;

	/**
	 * Constructor for the application
	 * @param model model with entity 
	 */
	public Application(KabasujiModel model) {
		this.model = model;
		
		//Create the content panel
		content = new JPanel(new CardLayout());
		setContentPane(content);
		
		//Add Main Menu
		mainMenuPanel = new MainMenuView(this);
		content.add(mainMenuPanel, MainMenuView.NAME);
		
		//Add Select Variation
		selectVariationPanel = new SelectVariationView(this);
		content.add(selectVariationPanel, SelectVariationView.NAME);
		
		//Add Level Select
		levelSelectPanel = new LevelSelectView(this, this.model);
		content.add(levelSelectPanel, LevelSelectView.NAME);
		
		//Add Level
		levelPanel = new LevelView(this, new Puzzle(model.board, new Bullpen(model.getPieces()), 1, 10));
		content.add(levelPanel, LevelView.NAME);		
		
		//Set some important settings
		setVisible(true);
		setTitle("Kabasuji");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds((1920/2) - 475, (1080/2) - 475, 750, 750); //We should probably change these numbers eventually
		addWindowListener(new CloseApplication(this));
		setLocation(0, 0);
	}
	
	/**
	 * Sets the current layout to the specific variation.
	 * @param name
	 */
	public void setContentPanel(String name)
	{
		CardLayout cl = (CardLayout) content.getLayout();
		try
		{
			cl.show(content, name);
		}
		catch(Exception e)
		{
			System.err.println("Error setting content panel.\n");
		}
	}
	
	/**
	 * Getter for level select view
	 * @return LevelSelectView current view
	 */
	public LevelSelectView getLevelSelectView() {
		return (LevelSelectView) this.levelSelectPanel;
	}
	
	/**
	 * Getter for level view
	 * @return LeveView current view for the level
	 */
	public LevelView getLevelView() {
		return (LevelView) this.levelPanel;
	}
	
	/**
	 * Getter for the current model
	 * @return LeveView current view for the level
	 */
	public KabasujiModel getModel() {
		return this.model;
	}

	/**
	 * Determines whether a piece is currently selected.
	 * @return True if a piece is selected, false if no piece is currently selected
	 */
	public boolean isPieceSelected() {
		if(this.getLevelView().getLevel().getBoard().getSelectedPiece() != null) {
			return true;
		}
		else if(this.getLevelView().getLevel().getBullpen().getSelectedPiece() != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * Deselects the currently selected piece if there is one.
	 * @return True if piece successfully deselected, false if no piece to deselect.
	 */
	public boolean deselectSelectedPiece() {
		if(this.getLevelView().getLevel().getBoard().getSelectedPiece() != null) {
			this.getLevelView().getLevel().getBoard().clearSelectedPiece();
			this.getLevelView().getBoardView().repaint();
			return true;
		}
		else if(this.getLevelView().getLevel().getBullpen().getSelectedPiece() != null) {
			this.getLevelView().getLevel().getBullpen().clearSelectedPiece();
			this.getLevelView().getBullpenView().update();
			return true;
		}
		return false;
	}

}
