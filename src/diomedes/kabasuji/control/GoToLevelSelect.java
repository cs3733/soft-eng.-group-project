package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.LevelSelectView;

/**
 * Controller to select a level variation.
 *
 */
public class GoToLevelSelect implements ActionListener
{

	Application app;
	String variantName;
	
	/**
	 * Constructor for selecting a levle.
	 * @param app Takes the current application.
	 * @param variant Takes the level variation name.
	 */
	public GoToLevelSelect(Application app, String variant){
		this.app = app;
		this.variantName = variant;
	}
	
	/**
	 * Selects the current display depending on the variation.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		app.setContentPanel(LevelSelectView.NAME);
		LevelSelectView levelSelectView = app.getLevelSelectView();
		levelSelectView.setVariantName(variantName + " Levels");
		levelSelectView.setUpTable(levelSelectView.getVariantLevels(variantName));
		
	}

}
