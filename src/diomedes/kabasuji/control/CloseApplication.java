package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.ArrayList;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.popup.QuitAssurancePopupView;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.LevelSaver;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.Puzzle;
import diomedes.shared.entity.Release;
import diomedes.shared.entity.Resources;

/**
 * Controller that closes the application.
 *
 */
public class CloseApplication implements ActionListener, WindowListener {


	Application app;
	
	/**
	 * Constructor for the controller
	 * @param app the current application
	 * 
	 */
	public CloseApplication(Application app){
		this.app = app;
	}
	
	/**
	 * Creates a new pop up to ensure the user wants to quit.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		QuitAssurancePopupView qa = new QuitAssurancePopupView(app);
	}

	@Override
	public void windowActivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0)
	{
		if(app.getModel().getCurrentLevel() != null)
		{
			app.getModel().getCurrentLevel().recordAchievement();
		}
		saveAllLevels();
		QuitAssurancePopupView qa = new QuitAssurancePopupView(app);		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Internal method to save all of the levels to memory
	 * @author Jordan Burklund
	 */
	void saveAllLevels() {
		System.out.println("Saving All Levels to Memory");
		ArrayList<Level> allLevels = app.getModel().getLevels();
		ArrayList<Level> lightningLevels = new ArrayList<Level>();
		ArrayList<Level> puzzleLevels = new ArrayList<Level>();
		ArrayList<Level> releaseLevels = new ArrayList<Level>();
		
		//Separate out the levels into their respective lists
		for(Level l : allLevels) {
			if(l instanceof Lightning) {
				lightningLevels.add(l);
			} else if(l instanceof Puzzle) {
				puzzleLevels.add(l);
			} else if(l instanceof Release) {
				releaseLevels.add(l);
			}
		}
		
		//Save each list to the appropriate location
		try {
			LevelSaver saver = new LevelSaver(Resources.lightningDir, lightningLevels.toArray(new Level[1]));
			saver.saveLevels();
			saver = new LevelSaver(Resources.releaseDir, releaseLevels.toArray(new Level[1]));
			saver.saveLevels();
			saver = new LevelSaver(Resources.puzzleDir, puzzleLevels.toArray(new Level[1]));
			saver.saveLevels();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
