package diomedes.kabasuji.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.BoardView;
import diomedes.kabasuji.view.popup.LevelCompletePopupView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Lightning;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Position;


/**
 * Controls the board in Kabasuji for lightning level.
 * @author Myles Spencer
 *
 */

public class BoardControllerLightning implements MouseMotionListener, MouseListener 
{


	Application app;

	/**
	 * Controller for the board needs the application.
	 * @param app
	 */
	public BoardControllerLightning(Application app) 
	{
		this.app = app;
	}


	/**
	 * Whenever the mouse is moved, if a piece is selected the Board draws it shadows
	 * to represent where it would be placed.
	 * @param MouseEvent me The mouse event of the mouse being moved.
	 */

	@Override
	public void mouseMoved(MouseEvent me) 
	{

		app.getLevelView().getBoardView().setHoverPosition(me.getX(), me.getY());
		app.getLevelView().getBoardView().repaint();
	}

	/**
	 * Tiles get covered when the Board is clicked
	 */
	@Override
	public void mouseClicked(MouseEvent me) 
	{
		Lightning lvl = (Lightning) app.getModel().getCurrentLevel();
		Piece p = lvl.getBullpen().getSelectedPiece();

		if(p != null)
		{
			lvl.getBullpen().removePiece(p);
			BoardView bv= app.getLevelView().getBoardView();
			Board b = lvl.getBoard();
			int col = bv.getColFromX(me.getX());
			int row = bv.getRowFromY(me.getY());
			Position rel = new Position(col, row);
			
			for(Position pos: p.getSquares())
			{
				if(b.isTileCovered(pos.derelatize(rel)) != null)
				{
					if(!b.getTileAtPosition(pos.derelatize(rel)).getIsCovered() && b.getTileAtPosition(pos.derelatize(rel)).isActive())
					{
						lvl.decreaseNumTilesLeft(1);
					}
					b.getTileAtPosition(pos.derelatize(rel)).setIsCovered(true);
				}
			}
			
			//Add random piece to Bullpen
			int index = (int) (34 * Math.random()) + 1;
			lvl.getBullpen().addPiece(KabasujiPieceFactory.makeNumberedPiece(index));
			app.getLevelView().getBullpenView().update();
			
			app.deselectSelectedPiece();
			bv.repaint();
			app.getLevelView().getAchievementView().repaint();
			if(lvl.hasWon())
			{
				app.getLevelView().getTimer().stop();
				new LevelCompletePopupView(app);
			}
		}
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	
	}
	
	
	@Override
	public void mouseExited(MouseEvent e) 
	{
	
		//Set the hover position to negative!
	
		app.getLevelView().getBoardView().setHoverPosition(-1000, -1000);
		
	}
	
	
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	
	}
	
	
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	
	}
	
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
	
	}
}
