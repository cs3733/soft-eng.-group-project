package diomedes.kabasuji.control;

import java.security.InvalidParameterException;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.levelbuilder.control.Move;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;
/**
 * Move Piece to Bullpen in Puzzle.
 * @author Aura
 * with a lot of @jordan 's code
 *
 */
public class MovePieceToBullpen extends Move {
	Position newBoardPos;
	PlacedPiece piece;
	Bullpen bullpen; 

	public MovePieceToBullpen(PlacedPiece piece, Bullpen bullpen) {
		//Can't do anything if the bullpen isn't set
		if(bullpen == null) {
			throw new InvalidParameterException("bullpen is not set");
		}
		//Can't do anything if the piece to move isn't set
		if(piece == null) {
			throw new InvalidParameterException("Piece to move not set");
		}

		this.bullpen = bullpen;
		this.piece = piece;
	}
	/**
	 * Cannot undo in Kabasuji
	 */
	@Override
	public boolean undoMove(IMovable lb) {
		return false;
	}

	@Override
	public boolean doMove(IMovable lb) {
		Level level = ((KabasujiModel) lb).getCurrentLevel();
		int achievement = level.getAchievement();
		Board board = ((KabasujiModel) lb).getCurrentLevel().getBoard();
		
		//Check if the move is valid
		if(!isValid(lb)) {
			return false;
		}
		//Add the piece to the bullpen
		bullpen.addPiece(piece.getPiece());
		board.clearSelectedPiece();
		board.removePiece(piece);
	

		//decreases the number of moves
		//This can ONLY be done in the puzzle mode
		((Puzzle) level).decreaseNumMovesLeft();
		
		//We took it out from the board so take that into account
		((Puzzle) level).increaseNumTilesLeft();
		
		if(achievement > 0){
			achievement = level.getAchievement(); //Refresh the getting achievement! We could have lost it.
			//Update and display appropriate things woo
		}

		return true;
	}
	/**
	 * Checks if any moves are allowed
	 * Do you have moves left? Have you select a piece?
	 */
	@Override
	public boolean isValid(IMovable lb) {
		Level level = ((KabasujiModel) lb).getCurrentLevel();
		//is the piece in the move a selected piece
		if(level.getBoard().getSelectedPiece() == null || !(level.getBoard().getSelectedPiece().equals(piece))) {
			return false;
		}
		//Does user have any moves left
		if(((Puzzle) level).getNumMovesLeft() <= 0){
			return false;
		}
		return true;
	}

}
