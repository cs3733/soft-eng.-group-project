package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.SelectVariationView;

/**
 * Controller to go to the Variation menu.
 *
 */
public class GoToSelectVariation implements ActionListener
{

	Application app;
	/**
	 * Constructor takes the current application.
	 * @param app current application
	 */
	public GoToSelectVariation(Application app){
		this.app = app;
	}
	/**
	 * Once an action is performed you go to the select variation menu.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		app.setContentPanel(SelectVariationView.NAME);
	}

}
