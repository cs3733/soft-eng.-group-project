package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JDialog;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.BoardView;
import diomedes.kabasuji.view.panel.MainMenuView;
import diomedes.shared.entity.Level;

/**
 * Controller to reset levels when a level is exited.
 * @author Aura
 *
 */
public class ExitButtonController implements ActionListener {
	
	Application app;
	JDialog w;
	KabasujiModel kabasuji;

	/**
	 * Constructor for the controller
	 * @param app currenet applicaction
	 * @param w the window that needs closing
	 */
	public ExitButtonController(Application app, JDialog w){
		this.app = app;
		this.w = w;
		this.kabasuji = app.getModel();
		
	}
	/**
	 * Resets the level and disposes the current level.
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		BoardView boardView = app.getLevelView().getBoardView();
		Level level = kabasuji.getCurrentLevel();
		//resets the level
		level.reset();
		level.recalculate();
		
		//Remove the current controller for the level.
		for(MouseListener ml: boardView.getMouseListeners()){
			boardView.removeMouseListener(ml);
		}
		
		for(MouseMotionListener mml: boardView.getMouseMotionListeners()){
			boardView.removeMouseMotionListener(mml);
		}
		
		//Resets timer for the Lightning mode
		if(level.getVariationName().equalsIgnoreCase("Lightning")){
			//Restore time left
			app.getLevelView().getTimer().stop();
			app.getLevelView().getTimer().resetCurrentTime();
		}
		
		
		boardView.repaint();
		app.setContentPanel(MainMenuView.NAME);
		app.deselectSelectedPiece();
		w.dispose();
		
	}

}
