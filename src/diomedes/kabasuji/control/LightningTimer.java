package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.popup.FailedLevelPopupView;
import diomedes.kabasuji.view.popup.LevelCompletePopupView;
import diomedes.shared.entity.Lightning;

/**
 * Timer for the Lightning Level.
 * @author Myles Spencer
 *
 */

public class LightningTimer implements ActionListener
{
	Application app;
	Timer timer;
	int maxTime;
	int currentTime;
	
	/**
	 * Constructor for Lightning timer.
	 * @param app	Current application.
	 * @param maxTime Maximum time for the timer.
	 */
	public LightningTimer(Application app, int maxTime)
	{
		this.app = app;
		this.maxTime = maxTime;
		int speed = 1000; //Every second
		timer = new Timer(speed, this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{		
		//Update Time display
		int timeLeft = maxTime - currentTime;
		int minutesLeft = timeLeft / 60;
		int secondsLeft = timeLeft % 60;
		String time = new String(minutesLeft + ":" + secondsLeft);
		app.getLevelView().getStatusView().setLightningTime(time);
		
		/**
		 * Checks if you have lost the level. Have you run out of time?
		 */
		if(currentTime >= maxTime)
		{
			timer.stop();
			//Trigger end of level
			Lightning lvl = (Lightning) app.getModel().getCurrentLevel();
			if(lvl.getAchievement() > 0)
			{
				new LevelCompletePopupView(app);
			}
			else
			{
				new FailedLevelPopupView(app);
			}
		}
		currentTime++;
	}
	
	/**
	 * Initiates the count down.
	 */
	public void start()
	{
		if(timer != null)
		{
			timer.start();
		}
	}
	
	/**
	 * Stops the timer.
	 */
	public void stop()
	{
		if(timer != null)
		{
			timer.stop();
		}
	}

	/**
	 * Resets the time for the timer. Useful when restarting a Lightning level.
	 */
	public void resetCurrentTime() {
		this.currentTime = 0;
		
	}
}
