package diomedes.kabasuji.control;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.BoardView;
import diomedes.kabasuji.view.popup.FailedLevelPopupView;
import diomedes.kabasuji.view.popup.LevelCompletePopupView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Release;

/**
 * Controller for Release level board.
 * @author Aura Velarde
 *
 */

public class BoardControllerRelease implements MouseMotionListener, MouseListener{

	Application app;
	KabasujiModel kabasuji;
	Graphics g;

	/**
	 * Controller for the board needs the application, the model, and the view.
	 * @param app Current application
	 */
	public BoardControllerRelease(Application app) {
		this.app = app;
		this.kabasuji = app.getModel();

		app.getLevelView().getStatusView().refreshReleaseLabel();

	}

	/**
	 * When the board is clicked; add a piece to the board if there's a piece and 
	 * cover the correct things. Check if the user has made any progress.
	 */
	@Override
	public void mouseClicked(MouseEvent me) {

		Piece p = null;
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null)
		{
			p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
		}

		if(p != null)
		{
			Release level =  (Release) app.getModel().getCurrentLevel();
			BoardView bv = app.getLevelView().getBoardView();
			Board b = app.getModel().getCurrentLevel().getBoard();
			int col = bv.getColFromX(me.getX());
			int row = bv.getRowFromY(me.getY());
			Position rel = new Position(col, row);
			PlacedPiece addedPiece = new PlacedPiece(rel, p.copy());

			if(b.isValid(addedPiece)){

				//Add the piece to the board!
				b.addPiece(addedPiece.copy());

				app.deselectSelectedPiece();

				//Remove it from the bullpen entity class
				app.getModel().getCurrentLevel().getBullpen().removePiece(p);
				app.getLevelView().getBullpenView().getPieceViews().updateSize();


				app.getLevelView().getBullpenView().updateUI();
				//Refreshes the number of tiles covered
				app.getLevelView().getStatusView().refreshReleaseLabel();
				app.getLevelView().refreshLabelBlue();
				app.getLevelView().refreshLabelGreen();
				app.getLevelView().refreshLabelRed();

				bv.repaint();
				app.getLevelView().getAchievementView().repaint();
				if(level.hasWon() || (level.getBullpen().getPieces().size() == 0 && level.getCurrentAchievement() > 0))
				{
					new LevelCompletePopupView(app);
				}
				else if(level.getBullpen().getPieces().size() == 0)
				{
					new FailedLevelPopupView(app);
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * Ensures the piece doesn't hover when not on the board.
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
		//Set the hover position to negative!

		app.getLevelView().getBoardView().setHoverPosition(-1000, -1000);
		app.getLevelView().getBoardView().repaint();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * Whenever the mouse is moved, if a piece is selected the Board draws it shadows
	 * to represent where it would be placed.
	 * @param MouseEvent me The mouse event of the mouse being moved.
	 */

	@Override
	public void mouseMoved(MouseEvent me) {

		app.getLevelView().getBoardView().setHoverPosition(me.getX(), me.getY());
		app.getLevelView().getBoardView().repaint();
	}

}
