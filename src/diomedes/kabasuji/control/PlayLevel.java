package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.LevelView;
import diomedes.shared.entity.Level;

/**
 * Controller to play a level.
 *
 */
public class PlayLevel implements ActionListener
{

	Application app;
	KabasujiModel model;
	JTable table;
	
	/**
	 * Constructor to play a level.
	 * @param app current application.
	 * @param model current model.
	 * @param table Table to display information.
	 */
	public PlayLevel(Application app, KabasujiModel model, JTable table){
		this.app = app;
		this.model = model;
		this.table = table;
	}
	
	/**
	 * Gets the level selected in the menu and selects it to play.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		LevelView lv = app.getLevelView();
		int selectedRow = table.getSelectedRow();
		if(selectedRow != -1) {
			Level lvl = (Level) table.getModel().getValueAt(selectedRow, 0);
			if(lvl.isUnlocked()) {
				app.setContentPanel(LevelView.NAME);
				//Sets the current level being played
				lvl.reset();
				lvl.recalculate();
				app.getModel().setCurrentLevel(lvl);
				lv.setLevel(lvl);
				lv.updateView();
			}
		}
	}

}
