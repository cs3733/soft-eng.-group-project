package diomedes.kabasuji.control;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import diomedes.kabasuji.view.Application;

/**
 * A scrollbar listener to help the rendering of pieces in the bullpen when scrolling through.
 * @author Kyle Carrero
 *
 */
public class BullpenScrollbarListener implements AdjustmentListener {

	/** Top-Level Application for Kabasuji **/
	Application app;
	
	/** Constructor
	 * @param app The current application **/
	public BullpenScrollbarListener(Application app) {
		this.app = app;
	}
	
	@Override
	/** Refreshes the bullpenview upon change event **/
	public void adjustmentValueChanged(AdjustmentEvent ae) {
		app.getLevelView().getBullpenView().update();
	}

}
