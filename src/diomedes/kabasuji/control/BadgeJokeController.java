package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.popup.BadgeJokePopupView;

/**
 * Controller that manages the badges joke.
 * @author Kyle Carrero
 *
 */

public class BadgeJokeController implements ActionListener {

	Application application;
	
	/**
	 * Takes the current application for the constructor
	 * @param application
	 */
	public BadgeJokeController(Application application) {
		this.application = application;
	}
	
	/**
	 * Pops the joke dialoge.
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		BadgeJokePopupView bjp = new BadgeJokePopupView(application);
	}

}
