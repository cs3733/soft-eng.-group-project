package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Piece;

/**
 * Controller to flip a piece vertically.
 */
public class FlipPieceVertical implements ActionListener {

	Application app;
	KabasujiModel km;

	/**
	 * Constructor for flipping a piece vertically.
	 * @param app the current application
	 * @param km the current model
	 */
	public FlipPieceVertical(Application app, KabasujiModel km) {
		this.app = app;
		this.km = km;
	}

	/**
	 * Flips the piece vertically once the button has been pressed.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
			Piece p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
			p.flipVertical();
			app.getLevelView().repaint();
		}
	}
	
}
