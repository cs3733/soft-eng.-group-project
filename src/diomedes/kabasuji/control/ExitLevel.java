package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.popup.FailedLevelPopupView;
import diomedes.kabasuji.view.popup.LevelCompletePopupView;

/**
 * Controller that handles returning to the LevelSelectView when the user
 * has decided that they are done playing a level.
 * @author Myles Spencer
 *
 */
public class ExitLevel implements ActionListener {

	Application app;

	public ExitLevel(Application app) {
		this.app = app;
	}
	
	/** Show the player the appropriate pop up if they got an Achievement or
	 * failed the level, and save the level back to memory only if they got
	 * an achievement
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.app.getModel().getCurrentLevel().getCurrentAchievement() > 0)
		{
			new LevelCompletePopupView(app);
		}
		else {
			new FailedLevelPopupView(app);
		}

	}

}
