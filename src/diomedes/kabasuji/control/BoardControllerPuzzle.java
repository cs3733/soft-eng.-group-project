package diomedes.kabasuji.control;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.BoardView;
import diomedes.kabasuji.view.popup.FailedLevelPopupView;
import diomedes.kabasuji.view.popup.LevelCompletePopupView;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;

/**
 * Controller for the Puzzle level Board.
 * @author Aura Velarde
 *
 */

public class BoardControllerPuzzle implements MouseMotionListener, MouseListener  {

	Application app;
	KabasujiModel kabasuji;
	Graphics g;

	/**
	 * Controller for the board needs the application, the model, and the view.
	 * @param app	the application
	 */
	public BoardControllerPuzzle(Application app) {
		this.app = app;
		this.kabasuji = app.getModel();

		/** TODO: Update when model.board in application is updated to work
		 * 
		 */
		app.getLevelView().getStatusView().refreshLabel();

	}


	/**
	 * Whenever the mouse is moved, if a piece is selected the Board draws it shadows
	 * to represent where it would be placed.
	 * @param MouseEvent me The mouse event of the mouse being moved.
	 */

	@Override
	public void mouseMoved(MouseEvent me) {

		app.getLevelView().getBoardView().setHoverPosition(me.getX(), me.getY());
		app.getLevelView().getBoardView().repaint();
	}

	/**
	 * A piece gets added when the Board is clicked, and a puzzle move is attempted!
	 */
	@Override
	public void mouseClicked(MouseEvent me) {

		Piece p = null;
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null)
		{
			p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();

		}

		if(p != null)
		{
			Puzzle level =  (Puzzle) app.getModel().getCurrentLevel();
			BoardView bv = app.getLevelView().getBoardView();
			Board b = app.getModel().getCurrentLevel().getBoard();
			int col = bv.getColFromX(me.getX());
			int row = bv.getRowFromY(me.getY());
			MovePieceToBoardInPuzzle m = new MovePieceToBoardInPuzzle(new Position(col, row), p);

			if(m.doMove(app.getModel()))
			{
				app.deselectSelectedPiece();

				//Remove it from the bullpen entity class
				app.getModel().getCurrentLevel().getBullpen().removePiece(p);
				app.getLevelView().getBullpenView().getPieceViews().updateSize();

				//Refresh the number of moves on the view as well
				app.getLevelView().getStatusView().refreshLabel();
				app.getLevelView().getBullpenView().updateUI();

				if(level.hasWon() || (level.getNumMovesLeft() == 0 && level.getCurrentAchievement() > 0))
				{
					new LevelCompletePopupView(app);
				}
				else if(level.getNumMovesLeft() == 0)
				{
					new FailedLevelPopupView(app);
				}
				
				bv.repaint();
				app.getLevelView().getAchievementView().repaint();
			}

		}
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseExited(MouseEvent e) {
		//Set the hover position to negative!
		app.getLevelView().getBoardView().setHoverPosition(-100, -100);
		app.getLevelView().getBoardView().repaint();
	}

	/**
	 * Moves a piece within the board when you press the Boar.
	 */
	@Override
	public void mousePressed(MouseEvent me) {
		Puzzle level =  (Puzzle) app.getModel().getCurrentLevel();
		BoardView bv = app.getLevelView().getBoardView();
		Board b = app.getModel().getCurrentLevel().getBoard();
		int col = bv.getColFromX(me.getX());
		int row = bv.getRowFromY(me.getY());

		if(b.getSelectedPiece() != null) {
			MovePieceInBoard m = new MovePieceInBoard(new Position(col, row), b.getSelectedPiece());
			if(m.doMove(app.getModel())){
				app.deselectSelectedPiece();
				app.getLevelView().getStatusView().refreshLabel();
				bv.repaint();
				if(level.hasWon() || (level.getNumMovesLeft() == 0 && level.getCurrentAchievement() > 0))
				{
					new LevelCompletePopupView(app);
				}
				else if(level.getNumMovesLeft() == 0)
				{
					new FailedLevelPopupView(app);
				}
			}
		}
		else {
			Position pos = new Position(col, row);
			b.selectPiece(pos);
		}

	}


	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
