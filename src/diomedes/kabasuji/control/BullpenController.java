package diomedes.kabasuji.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;

/**
 * Controller for the Bullpen.
 * @author Kyle Carrero
 * @author Myles Spencer
 */
public class BullpenController implements MouseMotionListener, MouseListener {
	
	Application app;
	/**
	 * Constructor for bullpen.
	 * @param app	Current application
	 */
	public BullpenController(Application app) {
		this.app = app;
	}

	/**
	 * When the Bullpen is clicked, you're either selecting a piece, or return a piece to the bullpen.
	 */
	@Override
	public void mouseClicked(MouseEvent me) {
		int index = (me.getX() / app.getLevelView().getBullpenView().getPieceViews().getHeight());
		
		if(index < app.getModel().getCurrentLevel().getBullpen().getPieces().size())
		{
			if(app.isPieceSelected()) {
				if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
					Piece s = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
					app.getModel().getCurrentLevel().getBullpen().clearSelectedPiece();
					Piece p = app.getModel().getCurrentLevel().getBullpen().getPieces().get(index);
					if(s != p)
					{
						app.getModel().getCurrentLevel().getBullpen().setSelectedPiece(p);
					}
				}
				else {
					PlacedPiece s = app.getModel().getCurrentLevel().getBoard().getSelectedPiece();
					MovePieceToBullpen mptb = new MovePieceToBullpen(s, app.getModel().getCurrentLevel().getBullpen());
					if(mptb.doMove(app.getModel())) {
						app.getLevelView().getBullpenView().update();
						app.getLevelView().getBoardView().repaint();
						app.getLevelView().getStatusView().refreshLabel();
						app.getLevelView().getAchievementView().repaint();
					}
					
					
				}
			}
			else {
				Piece p = app.getModel().getCurrentLevel().getBullpen().getPieces().get(index);
				app.getModel().getCurrentLevel().getBullpen().setSelectedPiece(p);
			}
			app.getLevelView().getBullpenView().update();
		}
	}

	@Override
	public void mouseEntered(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}
}
