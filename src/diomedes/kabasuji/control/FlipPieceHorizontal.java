package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Piece;

/**
 * Controller to flip pieces horizontally.
 *
 */
public class FlipPieceHorizontal implements ActionListener {

	Application app;
	KabasujiModel lb;

	/**
	 * Constructor for the controller.
	 * @param app current Application
	 * @param lb current model
	 */
	public FlipPieceHorizontal(Application app, KabasujiModel lb) {
		this.app = app;
		this.lb = lb;
	}

	/**
	 * Flips the selected piece horizontally .
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
			Piece p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
			p.flipHorizontal();
			app.getLevelView().repaint();
		}
	}
	
}
