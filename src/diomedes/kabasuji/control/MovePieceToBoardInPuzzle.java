package diomedes.kabasuji.control;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.Position;
import diomedes.shared.entity.Puzzle;

/**
 * Special move for the puzzle level.
 * @author Aura
 *
 */

public class MovePieceToBoardInPuzzle extends MovePieceToBoard {

	public MovePieceToBoardInPuzzle(Position position, Piece piece) {
		super(position, piece);
	}
	
	/**
	 * Moves the selected piece to the board, and deselects the piece.
	 * Decreases the number of moves. 
	 */
	@Override
	public boolean doMove(IMovable lb) {
		if(!isValid(lb)) {
			return false;
		}
		
		Level level = ((KabasujiModel) lb).getCurrentLevel();
		Board board = level.getBoard();
		Bullpen bullpen = level.getBullpen();
		
		//decreases the number of moves
		((Puzzle) level).decreaseNumMovesLeft();
		
		//Move successful also means less tiles
		((Puzzle) level).decreaseNumTilesLeft();
		
		boolean achievement = level.hasAchievement();
		if(achievement){
			int numStars = level.getAchievement();
			
			//TODO: Set the number of stars obtained
		}
		
		//Add the piece to the board, and deselect the piece
		board.addPiece(addedPiece.copy());
		board.clearSelectedPiece();
		bullpen.clearSelectedPiece();
		
		return true;
	}
	
	/**
	 * Checks if the move is valid by checking if there's moves left, and if the board can 
	 * add the piece.
	 */
	
	public boolean isValid(IMovable lb){
		Board board = ((KabasujiModel) lb).getCurrentLevel().getBoard();
		Puzzle level  = (Puzzle) ((KabasujiModel)lb).getCurrentLevel();
		int numMovesLeft = level.getNumMovesLeft();
		//Try to add the piece to the board. Make sure you have moves left.
		if(board.isValid(addedPiece) && numMovesLeft != 0) {
			//Can add the piece to the board.  Houston you are go for launch
			return true;
		}
		
		//All other logic has failed, so the move must not be valid
		return false;
	}

}
