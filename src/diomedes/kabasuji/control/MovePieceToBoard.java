package diomedes.kabasuji.control;


import java.security.InvalidParameterException;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.levelbuilder.control.Move;
import diomedes.shared.entity.Board;
import diomedes.shared.entity.Bullpen;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Piece;
import diomedes.shared.entity.PlacedPiece;
import diomedes.shared.entity.Position;

/**
 * Class that handles moving a piece from the toolbar to the board
 * Has methods to do the move, undo the move, and check if the move is valid.
 * @author Jordan Burklund
 *
 */
public class MovePieceToBoard extends Move {
	Position boardPosition;
	PlacedPiece addedPiece;

	
	/**
	 * Constructor specifying the piece and it's desired position on the board
	 * @param position Position that the piece should be placed at
	 * @param piece Piece to place on the board
	 * @throws InvalidParameterException if either parameters are null
	 */
	public MovePieceToBoard(Position position, Piece piece) {
		//Can't do anything if the position isn't set
		if(position == null) {
			throw new InvalidParameterException("Board position is not set");
		}
		//Can't do anything if the Piece isn't set
		if(piece == null) {
			throw new InvalidParameterException("Piece not set");
		}
		
		this.boardPosition = position;
		addedPiece = new PlacedPiece(boardPosition, piece.copy());
	}

	/**
	 * Returns false since the move cannot be undone
	 * @param lb Top level model
	 * @return false since the move cannot be undone
	 */
	@Override
	public boolean undoMove(IMovable lb) {
		return false;
	}

	/**
	 * Moves the selected piece to the board, and deselects the piece
	 */
	@Override
	public boolean doMove(IMovable lb) {
		if(!isValid(lb)) {
			return false;
		}
		
		Level level = ((KabasujiModel) lb).getCurrentLevel();
		Board board = level.getBoard();
		Bullpen bullpen = level.getBullpen();
		
		//Add the piece to the board, and deselect the piece
		board.addPiece(addedPiece.copy());
		board.clearSelectedPiece();
		bullpen.clearSelectedPiece();
		
		return true;
	}

	/**
	 * Checks if the move is valid
	 */
	@Override
	public boolean isValid(IMovable lb) {
		Board board = ((KabasujiModel) lb).getCurrentLevel().getBoard();
		//Try to add the piece to the board
		if(board.isValid(addedPiece)) {
			//Can add the piece to the board.  Houston you are go for launch
			return true;
		}
		
		//All other logic has failed, so the move must not be valid
		return false;
	}
}
