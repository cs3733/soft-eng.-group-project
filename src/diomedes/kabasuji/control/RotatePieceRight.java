package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.model.KabasujiModel;
import diomedes.kabasuji.view.Application;
import diomedes.shared.entity.Piece;

/**
 * Controller for rotating the selected Piece 90 degrees to the right.
 * @author Kyle Carrero
 *
 */
public class RotatePieceRight implements ActionListener {

	Application app;
	KabasujiModel km;

	/**
	 * Constructor for rotating a piece.
	 * @param app current application.
	 * @param km current model.
	 */
	public RotatePieceRight(Application app, KabasujiModel km) {
		this.app = app;
		this.km = km;
	}

	/**
	 * Once button is clicked; return 90 degrees to the left.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(app.getModel().getCurrentLevel().getBullpen().getSelectedPiece() != null) {
			Piece p = app.getModel().getCurrentLevel().getBullpen().getSelectedPiece();
			p.rotateRight();
			app.getLevelView().repaint();
		}
	}
}