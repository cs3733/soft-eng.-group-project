package diomedes.kabasuji.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import diomedes.kabasuji.view.Application;
import diomedes.kabasuji.view.panel.MainMenuView;

/**
 * Controller to return to the main Menu.
 */
public class ReturnToMainMenu implements ActionListener
{

	Application app;
	
	/**
	 * Constructor to return to main menu.
	 * @param app Current application
	 */
	public ReturnToMainMenu(Application app){
		this.app = app;
	}
	
	/**
	 * Once button is clicked, return to main menu.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		app.setContentPanel(MainMenuView.NAME);	

	}

}
