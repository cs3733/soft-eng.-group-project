package diomedes.kabasuji.model;

import java.util.ArrayList;

import diomedes.shared.entity.Board;
import diomedes.shared.entity.IMovable;
import diomedes.shared.entity.KabasujiPieceFactory;
import diomedes.shared.entity.Level;
import diomedes.shared.entity.Piece;

/**
 * High level model that contains Kabasuji information.
 *
 */
public class KabasujiModel implements IMovable {

	private ArrayList<Level> levels;
	ArrayList<Piece> p = new ArrayList<Piece>();
	Level currentLevel;
	
	public Board board;

	/**
	 * Constructor for the Kabasuji Model. Makes all of the pieces.
	 */
	public KabasujiModel() {
		levels = new ArrayList<Level>();

		for(int i = 1; i <= 35; i++) {
			p.add(KabasujiPieceFactory.makeNumberedPiece(i));
		}

	}

	/**
	 * Getter for the levels
	 * @return ArrayList<Level> levels all of the levels in the game
	 */
	public ArrayList<Level> getLevels() {
		return this.levels;
	}

	/**
	 * Getter for the pieces
	 * @return ArrayList<Pieces> p gets all of the pieces.
	 */
	public ArrayList<Piece> getPieces() {
		return p;
	}
	
	/**
	 * Returns the current level
	 * @return Level
	 */
	public Level getCurrentLevel(){
		return this.currentLevel;
	}
	
	public void setCurrentLevel(Level level){
		this.currentLevel = level;
	}
	
	/**
	 * Adds a level to the list
	 * TODO how to handle the level order number...
	 * @param level Level to add to the list
	 */
	public void addLevel(Level level) {
		levels.add(level);
	}
}
