Hello and welcome to Diomedes' Kabasuji!

In order to correctly run the splash screen, you must add the following VM Arguments in the Run Configurations:
(Run as... Run Configurations... VM Arguments)

For LevelBuilderMain.java:
-splash:src/diomedes/assets/LevelBuilderFadeIn.gif

for KabasujiMain.java
-splash:src/diomedes/assets/KabasujiFadeIn.gif

In order to run Kabasuji:
Run KabasujiMain.java

To run LevelBuilder:
Run LevelBuilderMain.java

both as java applications.

In our Level Builder class you will notice somethings
- When you pick a specific Variation only the things necessary for that level will show up
- You can enter the three variations to confirm this


In Kabasuji
- When you pick a specific Variation only the things necessary for that level will show up as well


Kabasuji FAQ 
How do I Play?
- Pick a Variation, and the first level should be unlocked for you! Once you receive an achievement the next one will be unlocked.
The way we decided to show achievements, is that if a player has won an achievement and they want to restart the game to get a better
achievement or just for fun; we keep the achievement they have displayed so they know what to aim for. 

What if I get stuck?
We added solutions in a folder under assets which contains the solutions for the levels.

How do I remove a piece from the board?
Click back in the bullpen! Make sure to not click too much to the right. The bullpen ends a bit prematurely, so it's easier if you 
click on the right.



Level Builder FAQ
How do I set the number of moves/timer?
Type the number of moves, or the time in seconds and be sure to press enter.

How do I save a level? 
Click File, Save and it will automatically be saved for you in a directory in your project called levels.

How do I load a level?
You won't have to worry about that; anything you've saved will be loaded for you :)

How can I add/Remove a hint?
Add a piece to the board, select the piece and then click add hint, it will immediately go to the bullpen for you.
If you want to remove the hint, click remove hint and then click the hint you want to remove.

How can I resize the Board?
You can resize the board by clicking the up and down arrows; if you have a piece added in a space where you're resizing to make it smaller
it will leave those tiles as valid for the board! that way it will be easy to create a separate board with the pieces that you started it.

How can I add/remove a number?
You can use our random generator and it will generate random numbers for the active tiles!
You can add them individually, pick a color and a number and click add numbers. If you regret a number, right click on the board
while mainting these settings.
If you hate eveyrything, you can just cleaer the board. 

How to undo Moves/Set Times
Double click the undo button if it doesn't work! 

When Testing:
We implemented mouse presses, which sadly randomly fail our tests.
We promise they all pass! You might just have to run it a couple of times. (Don't give up :))


Testing data
Graph and Charts are both available in assets.

Thank you!


